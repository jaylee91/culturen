package com.culturen.dao;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.culturen.dto.EventDTO;




public class EventDAO {
	
	private SqlSessionTemplate sessionTemplate;
	
	public void setSessionTemplate(SqlSessionTemplate sessionTemplate) throws Exception{	
		this.sessionTemplate = sessionTemplate;
	}
	//데이터갯수
	public int getDataCountIng(){
		
		int result =
				sessionTemplate
				.selectOne("com.event.eventMapper.getDataCountIng");
	
		return result;
		
	}
	

	//지난이벤트리스트
	public List<EventDTO> getListsIng(int start, int end,String evMode){
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("evMode", evMode);
		map.put("start", start);
		map.put("end", end);
		
		
		List<EventDTO> lists = sessionTemplate.selectList("com.event.eventMapper.getListsIng",map);
		return lists;
		
	}
	

	
}
