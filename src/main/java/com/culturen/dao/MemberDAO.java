package com.culturen.dao;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.mail.SimpleMailMessage;

import com.culturen.dto.MemberDTO;

public class MemberDAO {
	
	//DI(의존성 주입)
	private SqlSessionTemplate sessionTemplate;
	
	public void setSessionTemplate(SqlSessionTemplate sessionTemplate) throws Exception{	
		this.sessionTemplate = sessionTemplate;
		
	}
	
	// 1.num의 최대값
	public int getMaxNum(){
	
		int maxNum= 0;
		
		maxNum = sessionTemplate.selectOne("com.culturen.mybatis.memberMapper.maxNum");
		
		return maxNum;
		
	}
	
	// 2.입력(created.jsp->created_ok.jsp)
	public void insertData(MemberDTO dto){
		
		sessionTemplate.insert("com.culturen.mybatis.memberMapper.insertData", dto);
		
	}
	
	// 3.아이디 중복확인
	public String idCheck(String userId) {
		
		String id = sessionTemplate.selectOne("com.culturen.mybatis.memberMapper.idCheck", userId);
		
		return id;
	}
	
	// 4.로그인
	public boolean login(String userId, String userPwd) {
		
		Map<String, Object> hMap = new HashMap<String, Object>();
		
		hMap.put("userId", userId);
		hMap.put("userPwd", userPwd);
		
		String id = sessionTemplate.selectOne("com.culturen.mybatis.memberMapper.login", hMap);
		
		return (id == null) ? false : true;
	}
	
	// 5.하나의 데이터 읽어오기
	public MemberDTO getReadData(String userId) {

		MemberDTO dto = sessionTemplate.selectOne("com.culturen.mybatis.memberMapper.getReadData", userId);
		
		return dto;
	}
	
	// 6.데이터 수정하기
	
	public void updateData(MemberDTO dto) {
		
		sessionTemplate.update("com.culturen.mybatis.memberMapper.updateData", dto);
		
	}

	// 7.비밀번호 찾기
	public String findPw(String userId, String userName, String userEmail1, String userEmail2) {
		
		Map<String, Object> hMap = new HashMap<String, Object>();
		
		hMap.put("userId", userId);
		hMap.put("userName", userName);
		hMap.put("userEmail1", userEmail1);		
		hMap.put("userEmail2", userEmail2);
		
		String userPwd = sessionTemplate.selectOne("com.culturen.mybatis.memberMapper.findPw", hMap);
		
		return userPwd;
		
	}
	
}
