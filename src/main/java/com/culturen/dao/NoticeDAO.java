package com.culturen.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.culturen.dto.NoticeDTO;

public class NoticeDAO {
	
private SqlSessionTemplate sessionTemplate;
	
	public void setSessionTemplate(SqlSessionTemplate sessionTemplate) throws Exception{	
		this.sessionTemplate = sessionTemplate;
	}
	
	
	
	// 1.maxNum
		public int getMaxNum() {

			int maxNum = 0;

			maxNum = sessionTemplate.selectOne("com.notice.noticeMapper.maxNum");

			return maxNum;

		}

		// (created.jsp->created_ok.jsp)
		public void insertData(NoticeDTO dto) {

			sessionTemplate.insert("com.notice.noticeMapper.insertData", dto);

		}

		// list
		public List<NoticeDTO> getList(int start, int end, String searchKey, String searchValue) {

			HashMap<String, Object> map = new HashMap<String, Object>();

			map.put("start", start);
			map.put("end", end);
			map.put("searchKey", searchKey);
			map.put("searchValue", searchValue);

			List<NoticeDTO> lists = sessionTemplate.selectList("com.notice.noticeMapper.getLists", map);

			return lists;

		}

		// datacount
		public int getDataCount(String searchKey, String searchValue) {

			HashMap<String, Object> map = new HashMap<String, Object>();

			map.put("searchKey", searchKey);
			map.put("searchValue", searchValue);

			int result = sessionTemplate.selectOne("com.notice.noticeMapper.getDataCount", map);

			return result;

		}

		// 조회수
		public void updateHitCount(int nbNum) {

			sessionTemplate.update("com.notice.noticeMapper.updateHitCount", nbNum);

		}

		// article
		public NoticeDTO getReadData(int nbNum) {

			NoticeDTO dto = sessionTemplate.selectOne("com.notice.noticeMapper.getReadData", nbNum);

			return dto;

		}

		// 삭제
		public void deleteData(int nbNum) {

			sessionTemplate.delete("com.notice.noticeMapper.deleteData", nbNum);

		}

		// 수정
		public void updateData(NoticeDTO dto) {

			sessionTemplate.update("com.notice.noticeMapper.updateData", dto);

		}

	
}



