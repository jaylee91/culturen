package com.culturen.controller;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.culturen.dao.MemberDAO;
import com.culturen.dto.CustomInfo;
import com.culturen.dto.MemberDTO;
import com.culturen.email.Email;
import com.culturen.email.EmailSender;
import com.jdbc.util.MyUtil;

@Controller
public class MemberController {

	@Autowired
	@Qualifier("memberDAO")
	MemberDAO dao;
	
	@Autowired
	MyUtil myUtil;

	@Autowired
	EmailSender emailSender;
	@Autowired
	Email email;

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////// 회원가입(Member) 시작 ////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//join 회원가입 메인
	@RequestMapping(value="/join.action")
	public ModelAndView join() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/join");
		
		return mav;
	}
	
	//join_next 약관 동의
	@RequestMapping(value="/join_next.action")
	public ModelAndView join_next() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/join_next");
		
		return mav;
	}
	
	/*
	//join_child_confirm 
	@RequestMapping(value="/join_child_confirm.action")
	public ModelAndView join_child_confirm() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/join_child_confirm");
		
		return mav;
	}

	///join_next2 실명인증
	@RequestMapping(value="/join_next2.action")
	public ModelAndView join_next2() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/join_next2");
		
		return mav;
	}
	
	// join_next2  
	@RequestMapping(value="/join_cp_cert_number_confirm.action")
	public ModelAndView join_cp_cert_number_confirm() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/join_cp_cert_number_confirm");
		
		return mav;
	}
	
	*/
	// join_form 회원가입 폼
	@RequestMapping(value="/join_form.action" )
	public ModelAndView join_form() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/join_form");
		
		return mav;
	}
	
	// join_form 회원가입 처리
	@RequestMapping(value="/join_form_ok.action", method= {RequestMethod.GET, RequestMethod.POST})
	public String join_form_ok(MemberDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		int maxNum = dao.getMaxNum();
		dto.setUserNo(maxNum + 1);
		
		dao.insertData(dto);
		
		return "member/join_insert";
		
	}
	
	//아이디 중복확인 
	@RequestMapping(value="/checkID.action", method= {RequestMethod.GET, RequestMethod.POST})
	public String join_checkID(String userId, HttpServletRequest request, HttpServletResponse response) throws Exception {

		String id = dao.idCheck(userId);

		request.setAttribute("id", id);
		request.setAttribute("userId", userId);
		
		return "popup/checkID";
		
	}
	
	///join_insert 회원가입 완료
	@RequestMapping(value="/join_insert.action")
	public ModelAndView join_insert() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/join_insert");
		
		return mav;
	}
	
	///login 페이지
	@RequestMapping(value="/login.action")
	public ModelAndView login() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/login");
		
		return mav;
	}
	
	// login_ok 로그인 처리
	@RequestMapping(value="/login_ok.action", method= {RequestMethod.GET, RequestMethod.POST})
	public String login_ok(String userId, String userPwd, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		
		boolean id = (boolean)dao.login(userId, userPwd);
		String url= "";		

		if(id==true) {
			
			response.setContentType("text/html; charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.println("<script languague='javascript'>");
            out.println("alert('로그인이 완료되었습니다. 환영합니다.');");
            out.println("</script>");
            out.flush();	
			
            //로그인 성공시 세션 담기
            CustomInfo info = new CustomInfo();
            
            info.setUserId(userId);
		
			session.setAttribute("customInfo", info);
			
            url= "main/main";
            
		} else if(id==false) {

			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println("<script languague='javascript'>");
			out.println("alert('로그인 정보를 확인해주세요.');");
            out.println("</script>");			
			out.flush();
			
			url= "member/login";
		}
		
		return url;

	}
	
	
	// 로그아웃
	@RequestMapping(value="/logout.action", method= {RequestMethod.GET, RequestMethod.POST})
	public String logout(String userId, String userPwd, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		
		session.removeAttribute("customInfo");
		session.invalidate(); 						//세션의 모든정보 지우기

		return "main/main";
				
	}
	
	// 개인정보수정 화면불러오기
	@RequestMapping(value="/join_update.action", method= {RequestMethod.GET, RequestMethod.POST})
	public String joinUpdate(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	
		CustomInfo info = 
				(CustomInfo)session.getAttribute("customInfo");
		
		String userId = info.getUserId();
		
		MemberDTO dto = dao.getReadData(userId);
		
		request.setAttribute("dto", dto);
		
		return "member/join_update";
				
	}
	
	// 개인정보수정 완료
	@RequestMapping(value="/join_update_ok.action", method= {RequestMethod.GET, RequestMethod.POST})
	public String join_update_ok(MemberDTO dto, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	
		dao.updateData(dto);

		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<script languague='javascript'>");
		out.println("alert('회원수정이 완료되었습니다. 메인화면으로 이동합니다.');");
        out.println("</script>");			
		out.flush();
		
		
		return "main/main";
	}

	/*
	@RequestMapping(value="/sendpw.action")
    public ModelAndView sendEmailAction (@RequestParam Map<String, Object> paramMap, ModelMap model) throws Exception {
        
		ModelAndView mav;
        String id=(String) paramMap.get("id");
        String e_mail=(String) paramMap.get("email");
        String pw=mainService.getPw(paramMap);
        System.out.println(pw);
        if(pw!=null) {
            email.setContent("비밀번호는 "+pw+" 입니다.");
            email.setReceiver(e_mail);
            email.setSubject(id+"님 비밀번호 찾기 메일입니다.");
            emailSender.SendEmail(email);
            mav= new ModelAndView("redirect:/login.do");
            return mav;
        }else {
            mav=new ModelAndView("redirect:/logout.do");
            return mav;
        }
    }
	*/
	
	@RequestMapping(value="/findPw.action")
	public ModelAndView findPw() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/findPw");
		
		return mav;
	}
	
	
	// 비밀번호 찾기
	@RequestMapping(value="/findPw_ok.action", method= {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView findPw(MemberDTO dto, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ModelAndView mav;
		
		String id= (String) dto.getUserId();
		String userName = (String) dto.getUserName();
	    String userEmail1=(String) dto.getUserEmail1();
	    String userEmail2=(String) dto.getUserEmail2();
	    
	    String userEmail = userEmail1 + "@" + userEmail2;
	    
	    System.out.println(userEmail);
	    
		//1. 데이터 검사
		String userPwd = dao.findPw(id, userName, userEmail1, userEmail2);
				
		if(userPwd!=null) {
			
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println("<script languague='javascript'>");
			out.println("alert('비밀번호를 등록하신 메일로 발송했습니다. 메일을 확인해주세요.');");
	        out.println("</script>");			
			out.flush();
			
            email.setContent("비밀번호는 "+userPwd+" 입니다.");
            email.setReceiver(userEmail);
            email.setSubject(id+"님 비밀번호 찾기 메일입니다.");
            emailSender.SendEmail(email);
            mav = new ModelAndView("redirect:/login.action");
            return mav;

		}else {
			
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.println("<script languague='javascript'>");
			out.println("alert('회원정보가 일치하지 않습니다. 회원정보를 다시 입력해 주세요.');");
	        out.println("</script>");			
			out.flush();
			
            mav = new ModelAndView("redirect:/findPw.action");
            return mav;
        }

	}

}
