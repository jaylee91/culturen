package com.culturen.controller;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.culturen.dao.EventDAO;
import com.culturen.dao.NoticeDAO;
import com.culturen.dto.EventDTO;
import com.culturen.dto.NoticeDTO;
import com.jdbc.util.MyUtil;

@Controller
public class NqeController {
	
	@Autowired
	@Qualifier("NoticeDAO")
	NoticeDAO daoN;
	
	@Autowired
	@Qualifier("EventDAO")
	EventDAO dao;
	
	@Autowired
	MyUtil myUtil;
	
	//////////////////////////공지사항start////////////////////////////

	//인서트
	@RequestMapping(value="/noticeInsert.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})
	public String noticeInsert(HttpServletRequest request,HttpServletResponse response
			,NoticeDTO dto) throws Exception {
		
		return "useInfo/noticeInsert";
	}

	//insert_ok
	@RequestMapping(value="/noticeInsert_ok.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})	
	public String noticeInsert_ok(HttpServletRequest request, HttpServletResponse response
			,NoticeDTO dto) throws Exception {
		
		System.out.println(dto.getNbSubject() + "서브젝트 ㅅ");
		
		int maxNum=daoN.getMaxNum();
		dto.setNbNum(maxNum +1);
		
		daoN.insertData(dto);
		
		return "redirect:/notice.action";
	}
	
	//list
	@RequestMapping(value="/notice.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})	
	public String noticeList(HttpServletRequest request,HttpServletResponse response
			,NoticeDTO dto) throws Exception {
		
		String cp = request.getContextPath();
		
		String pageNum = request.getParameter("pageNum");
		int currentPage = 1;
		
		if(pageNum != null)
			currentPage = Integer.parseInt(pageNum);
		
		String searchKey = request.getParameter("searchKey");
		String searchValue = request.getParameter("searchValue");
		
		if(searchKey == null){
			
			searchKey = "nbSubject";
			searchValue = "";
			
		}else{
			
			if(request.getMethod().equalsIgnoreCase("GET"))
				searchValue =
					URLDecoder.decode(searchValue, "UTF-8");
			
		}
		
		int dataCount = daoN.getDataCount(searchKey, searchValue);
		
		int numPerPage = 5;
		int totalPage = myUtil.getPageCount(numPerPage, dataCount);
		
		if(currentPage> totalPage)
			currentPage = totalPage;
		
		int start =(currentPage-1)*numPerPage+1;
		int end = currentPage*numPerPage;
		
		List<NoticeDTO> lists=
				daoN.getList(start, end, searchKey, searchValue);
		
		int listNum,n=0;
		ListIterator<NoticeDTO> it  =lists.listIterator();
		while(it.hasNext()) {
			
			NoticeDTO data = (NoticeDTO)it.next();
			listNum = dataCount-(start+n-1);
			data.setLineNum(listNum);
			
			n++;
			
		}
		
		String param = "";
		if(!searchValue.equals("")){
			param = "searchKey=" + searchKey;
			param+= "&searchValue=" 
				+ URLEncoder.encode(searchValue, "UTF-8");
		}
		
		String listUrl = cp + "/notice.action";
		if(!param.equals("")){
			listUrl = listUrl + "?" + param;				
		}
		
		String pageIndexList =
				myUtil.pageIndexList(currentPage, totalPage, listUrl);
		
		String articleUrl =
				cp + "/noticeView.action?pageNum="+ currentPage;
		
		if(!param.equals(""))
			articleUrl = articleUrl + "&" + param;
		
		request.setAttribute("lists", lists);
		request.setAttribute("pageIndexList",pageIndexList);
		request.setAttribute("dataCount",dataCount);
		request.setAttribute("articleUrl",articleUrl);
		
		return "useInfo/notice";
	}

	//article
	@RequestMapping(value="/noticeView.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})	
	public String noticeView(HttpServletRequest request,HttpServletResponse response
			,NoticeDTO dto) throws Exception {
		
		String cp = request.getContextPath();
		
		int nbNum = Integer.parseInt(request.getParameter("nbNum"));
		String pageNum = request.getParameter("pageNum");
		
		String searchKey = request.getParameter("searchKey");
		String searchValue = request.getParameter("searchValue");
		
		if(searchKey != null)
			searchValue = URLDecoder.decode(searchValue, "UTF-8");
		
		daoN.updateHitCount(nbNum);
		dto = daoN.getReadData(nbNum);
		
		dto.setNbContent(dto.getNbContent().replaceAll("\n", "<br/>"));
		
		String param = "pageNum=" + pageNum;
		if(searchKey!=null){
			param += "&searchKey=" + searchKey;
			param += "&searchValue=" 
				+ URLEncoder.encode(searchValue, "UTF-8");
		}
		
		request.setAttribute("dto", dto);
		request.setAttribute("searchKey", searchKey);
		request.setAttribute("searchValue", searchValue);
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("params", param);
		
		
		return "useInfo/noticeView";
	}

	//update
	@RequestMapping(value="/noticeUpdated.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})	
	public String noticeUpdated(HttpServletRequest request,HttpServletResponse response
			,NoticeDTO dto) throws Exception {
		
		String cp = request.getContextPath();
		
		int nbNum = Integer.parseInt(request.getParameter("nbNum"));
		String pageNum = request.getParameter("pageNum");
		
		dto = daoN.getReadData(nbNum);
		
		if(dto == null) {
			return "redirect:/notice.action";
		}
		
		request.setAttribute("dto", dto);
		request.setAttribute("pageNum", pageNum);
		
		return "useInfo/noticeUpdated";
		
	}
	
	//update
	@RequestMapping(value="/noticeUpdated_ok.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})	
	public String noticeUpdated_ok(HttpServletRequest request,HttpServletResponse response
			,NoticeDTO dto) throws Exception {
		
		String pageNum = request.getParameter("pageNum");
		
		daoN.updateData(dto);
		
		return "redirect:/notice.action?pageNum="+pageNum;
	}
	
	@RequestMapping(value="/noticeDeleted.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})	
	public String noticeDeleted(HttpServletRequest request,HttpServletResponse response
			,NoticeDTO dto) throws Exception {
		
		int nbNum = Integer.parseInt(request.getParameter("nbNum"));
		String pageNum = request.getParameter("pageNum");
		
		daoN.deleteData(nbNum);
		
		return "redirect:/notice.action?pageNum="+pageNum;
		
		
	}

	//////////////////////////공지사항end////////////////////////////

	/////////////////////////티켓안내start////////////////////////////

	@RequestMapping(value="/ticketInfo.action" ,
			method= {RequestMethod.GET,RequestMethod.POST})	
	public String ticketInfo(HttpServletRequest request,HttpServletResponse response
			) throws Exception {
		
		return "userInfo/ticketInfo";
		
	}
	
	/////////////////////////티켓안내end////////////////////////////
	
	/////////////////////////이벤트 페이지///////////////////////////////

/*	// winList 당첨자 확인
	@RequestMapping(value="/winList.action", method= {RequestMethod.GET,RequestMethod.POST})
	public String winList(HttpServletRequest request,
			HttpServletRequest response) throws Exception {

		String cp = request.getContextPath();
		
		String evMode = (String) request.getAttribute("evMode");
		

		String pageNum = request.getParameter("pageNum");
		int currentPage = 1;
		
		if(pageNum !=null)
			currentPage = Integer.parseInt(pageNum);
		
		int dataCount = dao.getDataCount();
		
		int numPerPage = 6;
		int totalPage = myUtil.getPageCount(numPerPage, dataCount);
		
		if(currentPage > totalPage)
			currentPage = totalPage;
		
		int start = (currentPage-1)*numPerPage+1;
		int end = currentPage*numPerPage;
		
		List<EventDTO> lists =
				dao.getLists(start, end);
		
		String listUrl = cp + "/endList.action";
		
		String pageIndexList =
				myUtil.pageIndexList(currentPage, totalPage, listUrl);
		
		System.out.println(lists + "리스트해");
		request.setAttribute("lists", lists);
		request.setAttribute("pageIndexList",pageIndexList);
		request.setAttribute("dataCount",dataCount);
		
		//return "event/endList";	}
		*/
	
	/*// 지난이벤트 list
	@RequestMapping(value="/endList.action", method= {RequestMethod.GET,RequestMethod.POST})
	public String endList(HttpServletRequest request,
			HttpServletRequest response) throws Exception {
			
		String cp = request.getContextPath();
		
		String evMode = (String) request.getAttribute("evMode");
		
		
			
	
		
		String pageNum = request.getParameter("pageNum");
		int currentPage = 1;
		System.out.println("222EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE:::");
		if(pageNum !=null)
			currentPage = Integer.parseInt(pageNum);
		
		int dataCount = dao.getDataCount();
		
		int numPerPage = 6;
		int totalPage = myUtil.getPageCount(numPerPage, dataCount);
		
		if(currentPage > totalPage)
			currentPage = totalPage;
		
		int start = (currentPage-1)*numPerPage+1;
		int end = currentPage*numPerPage;
		
		List<EventDTO> lists =
				dao.getLists(start, end);
		
		String listUrl = cp + "/endList.action";
		
		String pageIndexList =
				myUtil.pageIndexList(currentPage, totalPage, listUrl);
		
		System.out.println(lists + "리스트해");
		request.setAttribute("lists", lists);
		request.setAttribute("pageIndexList",pageIndexList);
		request.setAttribute("dataCount",dataCount);
		
		//return "event/endList";
	
		
		return "event/endList";
			
	}*/
	
	@RequestMapping(value="/ingList.action", method= {RequestMethod.GET,RequestMethod.POST})
	public String ingList(HttpServletRequest request,
			HttpServletRequest response) throws Exception{
		
		String cp = request.getContextPath();
		
		String evMode = request.getParameter("evMode");
		
		if(evMode == null || !evMode.equalsIgnoreCase("")) {
			evMode = "A";
		}
		
		String pageNum = request.getParameter("pageNum");
		int currentPage = 1;
		if(pageNum !=null)
			currentPage = Integer.parseInt(pageNum);
		
		int dataCount = dao.getDataCountIng();
		
		int numPerPage = 6;
		int totalPage = myUtil.getPageCount(numPerPage, dataCount);
		
		if(currentPage > totalPage)
			currentPage = totalPage;
		
		int start = (currentPage-1)*numPerPage+1;
		int end = currentPage*numPerPage;
		
		List<EventDTO> lists =
				dao.getListsIng(start, end, evMode);
		
		int listNum,n=0;
		ListIterator<EventDTO> it  =lists.listIterator();
		while(it.hasNext()) {
			
			EventDTO data = (EventDTO)it.next();
			listNum = dataCount-(start+n-1);
			data.setListNum(listNum);
			
			n++;
			
		}
		
		String listUrl = cp + "/ingList.action";
		
		String pageIndexList =
				myUtil.pageIndexList(currentPage, totalPage, listUrl);
		
		request.setAttribute("evMode", evMode);
		request.setAttribute("lists", lists);
		request.setAttribute("pageIndexList",pageIndexList);
		request.setAttribute("dataCount",dataCount);
		
				
		return "event/ingList";
		
	}
	
	

}
