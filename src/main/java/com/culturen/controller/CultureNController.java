package com.culturen.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Document;

import com.culturen.dao.CultureNDAO;
import com.culturen.dto.ConcertListDTO;
import com.culturen.dto.CustomInfo;
import com.jdbc.util.ApiExplorer;
import com.jdbc.util.MyUtil;

@Controller
public class CultureNController {

	@Autowired
	@Qualifier("cultureNDAO")
	CultureNDAO dao;
	
	@Autowired
	ApiExplorer apiExplorer;
	
	@Autowired
	MyUtil myUtil;
	
	@RequestMapping(value="/", method= {RequestMethod.GET,RequestMethod.POST})
	public String home(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		
		//로그인 세션값 불러오기
		CustomInfo info = 
				(CustomInfo)session.getAttribute("customInfo");
		if(info!=null) {
			String userId = info.getUserId();
			System.out.println(userId);			
			request.setAttribute("userId", userId);
		}
		
		return "main/main";
	}
	
	/*
	@RequestMapping(value="/created.action", method= {RequestMethod.GET,RequestMethod.POST})
	public String created(HttpServletRequest request, HttpServletResponse response) {
		
		return "bbs/created";
	
	}
	*/
	
	///�׽�Ʈ
	@RequestMapping(value="/test.action")
	public ModelAndView test() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("layout/test");
		
		return mav;
	}
	
	
	///concert_list  공연
	@RequestMapping(value="/concert_list.action", method= {RequestMethod.GET,RequestMethod.POST})
	public String concert_list(HttpServletRequest request) throws Exception {	
		
		String genre = request.getParameter("genre");
		String str = "consert";
		
		Document doc = apiExplorer.doc(genre);
		int totalCount = apiExplorer.totalCount(doc);
		List<ConcertListDTO> lists = apiExplorer.concert(doc,str,genre);
			request.setAttribute("totalCount", totalCount);
		request.setAttribute("lists", lists);
		request.setAttribute("genre", genre);
			return "concert/concert_list";
		}

	///concert_view 상세 페이지
	@RequestMapping(value="/concert_view.action", method= {RequestMethod.GET,RequestMethod.POST})
	public String concert_view(HttpServletRequest request) throws Exception {
		
		int seq = Integer.parseInt(request.getParameter("seq"));
		
		ConcertListDTO dto = apiExplorer.concertView(seq);
		
		request.setAttribute("dto", dto);
		
		return "concert/concert_view";
	}
	
	///exhibit_list  전시
	@RequestMapping(value="/exhibit_list.action")
	public String exhibit_list(HttpServletRequest request) throws Exception {
		
		String genre = request.getParameter("genre");
		String str = "exhibit";
		
		Document doc = apiExplorer.doc(genre);
		int totalCount = apiExplorer.totalCount(doc);
		List<ConcertListDTO> lists = apiExplorer.concert(doc,str,genre);
		
		request.setAttribute("totalCount", totalCount);
		request.setAttribute("lists", lists);
		request.setAttribute("genre", genre);
		
		return "exhibit/exhibit_list";
	}
	
	///exhibit_view 상세 페이지
	@RequestMapping(value="/exhibit_view.action", method= {RequestMethod.GET,RequestMethod.POST})
	public String exhibit_view(HttpServletRequest request) throws Exception {
		
		int seq = Integer.parseInt(request.getParameter("seq"));
		
		ConcertListDTO dto = apiExplorer.concertView(seq);
		
		request.setAttribute("dto", dto);
		
		return "concert/concert_view";
	}
	
	///storyList  ��ȭ�̾߱�
	@RequestMapping(value="/storyList.action")
	public ModelAndView storyList() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("culture/storyList");
		
		return mav;
	}
	
	///actorList ��ȭ�ι� �̾߱�
	@RequestMapping(value="/actorList.action")
	public ModelAndView actorList() throws Exception {
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("member/actorList");
		
		return mav;
	}
	
}
