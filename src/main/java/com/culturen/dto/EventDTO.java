package com.culturen.dto;

public class EventDTO {
	
	private int evNum;
	private String evSubject;
	private String evRegdate;
	private String evPlace;
	private String evSdate;
	private String evEdate;
	private String evAnnouncement;
	private String evInvite;
	private String evMode;
	private String evImage;
	private String evContent;
	private String evCount;
	private int listNum;
	
	public int getListNum() {
		return listNum;
	}
	public void setListNum(int listNum) {
		this.listNum = listNum;
	}
	public String getEvMode() {
		return evMode;
	}
	public void setEvMode(String evMode) {
		this.evMode = evMode;
	}
	public String getEvSdate() {
		return evSdate;
	}
	public void setEvSdate(String evSdate) {
		this.evSdate = evSdate;
	}
	public String getEvEdate() {
		return evEdate;
	}
	public void setEvEdate(String evEdate) {
		this.evEdate = evEdate;
	}
	public String getEvAnnouncement() {
		return evAnnouncement;
	}
	public void setEvAnnouncement(String evAnnouncement) {
		this.evAnnouncement = evAnnouncement;
	}
	public String getEvInvite() {
		return evInvite;
	}
	public void setEvInvite(String evInvite) {
		this.evInvite = evInvite;
	}
	public String getEvPlace() {
		return evPlace;
	}
	public void setEvPlace(String evPlace) {
		this.evPlace = evPlace;
	}
	public int getEvNum() {
		return evNum;
	}
	public void setEvNum(int evNum) {
		this.evNum = evNum;
	}
	public String getEvSubject() {
		return evSubject;
	}
	public void setEvSubject(String evSubject) {
		this.evSubject = evSubject;
	}
	public String getEvRegdate() {
		return evRegdate;
	}
	public void setEvRegdate(String evRegdate) {
		this.evRegdate = evRegdate;
	}
	public String getEvImage() {
		return evImage;
	}
	public void setEvImage(String evImage) {
		this.evImage = evImage;
	}
	public String getEvContent() {
		return evContent;
	}
	public void setEvContent(String evContent) {
		this.evContent = evContent;
	}
	public String getEvCount() {
		return evCount;
	}
	public void setEvCount(String evCount) {
		this.evCount = evCount;
	}
	
	

}
