package com.culturen.dto;

public class NoticeDTO {
	
	private int nbNum;
	
	private String nbSubject;
	private String nbRedgate;
	private String nbContent; 
	private String nbCount;
	private int listNum;

	public int getListNum() {
		return listNum;
	}
	public void setLineNum(int listNum) {
		this.listNum = listNum;
	}
	public int getNbNum() {
		return nbNum;
	}
	public void setNbNum(int nbNum) {
		this.nbNum = nbNum;
	}
	public String getNbSubject() {
		return nbSubject;
	}
	public void setNbSubject(String nbSubject) {
		this.nbSubject = nbSubject;
	}
	public String getNbRedgate() {
		return nbRedgate;
	}
	public void setNbRedgate(String nbRedgate) {
		this.nbRedgate = nbRedgate;
	}
	public String getNbContent() {
		return nbContent;
	}
	public void setNbContent(String nbContent) {
		this.nbContent = nbContent;
	}
	public String getNbCount() {
		return nbCount;
	}
	public void setNbCount(String nbCount) {
		this.nbCount = nbCount;
	}
	
	

}
