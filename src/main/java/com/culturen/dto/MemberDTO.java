﻿package com.culturen.dto;

public class MemberDTO {
	
	private String userId;
	private int userNo;
	private String userName;
	private String userPost;
	private String userPwd;
	private String userAddr;
	private String userTel1;
	private String userTel2;
	private String userTel3;
	private String userBirth;
	private String userEmail1;
	private String userEmail2;
	private String userEmail3;
	private String userGender;
	private String userPwdQ;
	private String userPwdA;
	private String userNewsReceive;
	private String userSelect;
	private String userJoin;
	private String userMgFunc;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPost() {
		return userPost;
	}
	public void setUserPost(String userPost) {
		this.userPost = userPost;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getUserAddr() {
		return userAddr;
	}
	public void setUserAddr(String userAddr) {
		this.userAddr = userAddr;
	}

	public String getUserTel1() {
		return userTel1;
	}
	public void setUserTel1(String userTel1) {
		this.userTel1 = userTel1;
	}
	public String getUserTel2() {
		return userTel2;
	}
	public void setUserTel2(String userTel2) {
		this.userTel2 = userTel2;
	}
	public String getUserTel3() {
		return userTel3;
	}
	public void setUserTel3(String userTel3) {
		this.userTel3 = userTel3;
	}
	public String getUserBirth() {
		return userBirth;
	}
	public void setUserBirth(String userBirth) {
		this.userBirth = userBirth;
	}
	public String getUserEmail1() {
		return userEmail1;
	}
	public void setUserEmail1(String userEmail1) {
		this.userEmail1 = userEmail1;
	}
	public String getUserEmail2() {
		return userEmail2;
	}
	public void setUserEmail2(String userEmail2) {
		this.userEmail2 = userEmail2;
	}
	public String getUserEmail3() {
		return userEmail3;
	}
	public void setUserEmail3(String userEmail3) {
		this.userEmail3 = userEmail3;
	}
	public String getUserGender() {
		return userGender;
	}
	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}
	public String getUserPwdQ() {
		return userPwdQ;
	}
	public void setUserPwdQ(String userPwdQ) {
		this.userPwdQ = userPwdQ;
	}
	public String getUserPwdA() {
		return userPwdA;
	}
	public void setUserPwdA(String userPwdA) {
		this.userPwdA = userPwdA;
	}
	public String getUserNewsReceive() {
		return userNewsReceive;
	}
	public void setUserNewsReceive(String userNewsReceive) {
		this.userNewsReceive = userNewsReceive;
	}
	public String getUserSelect() {
		return userSelect;
	}
	public void setUserSelect(String userSelect) {
		this.userSelect = userSelect;
	}
	public String getUserJoin() {
		return userJoin;
	}
	public void setUserJoin(String userJoin) {
		this.userJoin = userJoin;
	}
	public String getUserMgFunc() {
		return userMgFunc;
	}
	public void setUserMgFunc(String userMgFunc) {
		this.userMgFunc = userMgFunc;
	}

}
