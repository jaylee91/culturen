package com.jdbc.util;

import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.culturen.dto.ConcertListDTO;

public class ApiExplorer {
	
	public Document doc(String genre) throws Exception {

		StringBuilder urlBuilder;
		
		if(genre==null) {
			urlBuilder = new StringBuilder("http://www.culture.go.kr/openapi/rest/publicperformancedisplays/period");
	        urlBuilder.append("?serviceKey=IA4sckeaDu%2FIZCrTYPCeyWCzXfGBt4FDQuO4FYgUqgazYsE0K8hpd%2BS2j6q93kcnnyp3A%2F2TewBzen9MWgceMA%3D%3D");
	        urlBuilder.append("&rows=9999");
		}else {
			urlBuilder = new StringBuilder("http://www.culture.go.kr/openapi/rest/publicperformancedisplays/realm");
			urlBuilder.append("?serviceKey=IA4sckeaDu%2FIZCrTYPCeyWCzXfGBt4FDQuO4FYgUqgazYsE0K8hpd%2BS2j6q93kcnnyp3A%2F2TewBzen9MWgceMA%3D%3D");
		
			if(genre.equals("10")) {
				urlBuilder.append("&realmCode=A000");// 연극
			}else if(genre.equals("20")) {
				urlBuilder.append("&realmCode=A000");// 뮤지컬
			}else if(genre.equals("30")) {
				urlBuilder.append("&realmCode=B000");// 국악/음악/콘서트
			}else if(genre.equals("40")) {
				urlBuilder.append("&realmCode=C000");// 클래식/무용
			}else if(genre.equals("50")) {
				urlBuilder.append("&realmCode=");// 아동/가족
			}else if(genre.equals("60")||genre.equals("70")||genre.equals("80")) {
				urlBuilder.append("&realmCode=");// 미술
			}
			urlBuilder.append("&rows=999");
		}
	
		URL url = new URL(urlBuilder.toString());
		URLConnection conn = (URLConnection) url.openConnection();

		conn.setRequestProperty("CONTENT-TYPE", "text/xml");

		DocumentBuilderFactory objDocumentBuilderFactory = null;
		DocumentBuilder objDocumentBuilder = null;
		Document doc = null;

		objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

		doc = objDocumentBuilder.parse(conn.getInputStream());
		
		return doc;
	}
	
	public int totalCount(Document doc) throws Exception{
	
		Node total = doc.getElementsByTagName("totalCount").item(0);
		int totalCount = Integer.parseInt(total.getTextContent());
		
		return totalCount;
	}
	
	public List<ConcertListDTO> concert(Document doc, String str, String genre) throws Exception{

		NodeList nList = doc.getElementsByTagName("perforList");

		List<ConcertListDTO> lists = new ArrayList<ConcertListDTO>();

		for (int i = 0; i < nList.getLength(); i++) {

			ConcertListDTO concertList = new ConcertListDTO();
			for (Node node = nList.item(i).getFirstChild(); node != null; node = node.getNextSibling()) {

				if (node.getNodeName().equals("seq")) {
					concertList.setSeq(Integer.parseInt(node.getTextContent()));
				} else if (node.getNodeName().equals("title")) {
					concertList.setTitle(node.getTextContent());
				} else if (node.getNodeName().equals("place")) {
					concertList.setPlace(node.getTextContent());
				} else if (node.getNodeName().equals("realmName")) {
					concertList.setRealmName(node.getTextContent());
				} else if (node.getNodeName().equals("area")) {
					concertList.setArea(node.getTextContent());
				} else if (node.getNodeName().equals("thumbnail")) {
					concertList.setThumbnail(node.getTextContent());
				}

			}
			
			if((genre==null||genre.equals(""))&&str.equals("consert")) {
				if(!concertList.getRealmName().equals("미술")) {
					lists.add(concertList);
				}
			}else if((genre==null||genre.equals(""))&&!str.equals("consert")) {
				if(concertList.getRealmName().equals("미술")) {
					lists.add(concertList);
				}
			}else if(genre.equals("10")&&!concertList.getTitle().contains("뮤지컬")) {
				lists.add(concertList);
			}else if(genre.equals("20")&&concertList.getTitle().contains("뮤지컬")&&!(concertList.getTitle().contains("가족")||concertList.getTitle().contains("어린이"))) {
				lists.add(concertList);
			}else if(genre.equals("30")) {
				lists.add(concertList);
			}else if(genre.equals("40")) {
				lists.add(concertList);
			}else if(genre.equals("50")&&(concertList.getTitle().contains("어린이")||concertList.getTitle().contains("가족"))) {
				lists.add(concertList);
			}else if(genre.equals("60")&&(concertList.getPlace().contains("미술관")||concertList.getPlace().contains("박물관")||concertList.getPlace().contains("갤러리")||concertList.getPlace().contains("뮤지엄"))) {
				lists.add(concertList);
			}else if(genre.equals("70")&&concertList.getRealmName().equals("미술")&&!(concertList.getPlace().contains("미술관")||concertList.getPlace().contains("박물관")||concertList.getPlace().contains("갤러리")||concertList.getPlace().contains("뮤지엄")||concertList.getPlace().contains("아트홀"))) {
				lists.add(concertList);
			}else if(genre.equals("80")&&concertList.getRealmName().equals("미술")&&concertList.getPlace().contains("아트홀")) {
				lists.add(concertList);
			}
			
		}

		return lists;

	}
	
	public String transFormDate(String date){
        SimpleDateFormat beforeFormat = new SimpleDateFormat("yyyymmdd");
        
        // Date로 변경하기 위해서는 날짜 형식을 yyyy-mm-dd로 변경해야 한다.
        SimpleDateFormat afterFormat = new SimpleDateFormat("yyyy-mm-dd");
        
        java.util.Date tempDate = null;
        
        try {
            // 현재 yyyymmdd로된 날짜 형식으로 java.util.Date객체를 만든다.
            tempDate = beforeFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        // java.util.Date를 yyyy-mm-dd 형식으로 변경하여 String로 반환한다.
        String transDate = afterFormat.format(tempDate);
        
        return transDate;
    }
	
	public ConcertListDTO concertView(int seq) throws Exception{
	
		StringBuilder urlBuilder = new StringBuilder("http://www.culture.go.kr/openapi/rest/publicperformancedisplays/d/");
		urlBuilder.append("?seq="+seq);
        urlBuilder.append("&serviceKey=IA4sckeaDu%2FIZCrTYPCeyWCzXfGBt4FDQuO4FYgUqgazYsE0K8hpd%2BS2j6q93kcnnyp3A%2F2TewBzen9MWgceMA%3D%3D");
        
		URL url = new URL(urlBuilder.toString());
		URLConnection conn = (URLConnection) url.openConnection();

		conn.setRequestProperty("CONTENT-TYPE", "text/xml");

		DocumentBuilderFactory objDocumentBuilderFactory = null;
		DocumentBuilder objDocumentBuilder = null;
		Document doc = null;

		objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
		objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

		doc = objDocumentBuilder.parse(conn.getInputStream());

		NodeList nList = doc.getElementsByTagName("perforInfo");

		ConcertListDTO dto = null;
		if (nList!=null && !nList.equals("")) {

			dto = new ConcertListDTO();
			for (Node node = nList.item(0).getFirstChild(); node != null; node = node.getNextSibling()) {
				
				if (node.getNodeName().equals("seq")) {
                    dto.setSeq(Integer.parseInt(node.getTextContent()));
                }else if(node.getNodeName().equals("title")){
                	dto.setTitle(node.getTextContent());
                }else if(node.getNodeName().equals("startDate")){
                	dto.setStartDate(transFormDate(node.getTextContent()));
                }else if(node.getNodeName().equals("endDate")){
                	dto.setEndDate(transFormDate(node.getTextContent()));
                }else if(node.getNodeName().equals("place")){
                	dto.setPlace(node.getTextContent());
                }else if(node.getNodeName().equals("realmName")){
                	dto.setRealmName(node.getTextContent());
                }else if(node.getNodeName().equals("area")){
                	dto.setArea(node.getTextContent());
                }else if(node.getNodeName().equals("thumbnail")){
                	dto.setThumbnail(node.getTextContent());
                }else if(node.getNodeName().equals("subTitle")){
                	dto.setSubTitle(node.getTextContent());
                }else if(node.getNodeName().equals("price")){
                	dto.setPrice(node.getTextContent());
                }else if(node.getNodeName().equals("contents1")){
                	dto.setContents1(node.getTextContent());
                }else if(node.getNodeName().equals("contents2")){
                	dto.setContents2(node.getTextContent());
                }else if(node.getNodeName().equals("url")){
                	dto.setUrl(node.getTextContent());
                }else if(node.getNodeName().equals("phone")){
                	dto.setPhone(node.getTextContent());
                }else if(node.getNodeName().equals("imgUrl")){
                	dto.setImgUrl(node.getTextContent());
                }else if(node.getNodeName().equals("gpsX")){
                	dto.setGpsX(node.getTextContent());
                }else if(node.getNodeName().equals("gpsY")){
                	dto.setGpsY(node.getTextContent());
                }else if(node.getNodeName().equals("placeUrl")){
                	dto.setPlaceUrl(node.getTextContent());
                }else if(node.getNodeName().equals("placeAddr")){
                	dto.setPlaceAddr(node.getTextContent());
                }else if(node.getNodeName().equals("placeSeq")){
                	dto.setPlaceSeq(node.getTextContent());
                }
 
        	}		
 
        }
		
		return dto;
		
	}
	
}
