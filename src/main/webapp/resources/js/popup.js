function popUp(obj){
	var $id = obj || $(this).attr('data-id');
	var $url = '../popup/';
	if($id == 'ticketing'){
		$url = '../ticket/';
		$id = 'step01';
	}
	var popWrap = $('<div class="popup_outer">').load($url + $id +'.do', function(){
		$('body').append(popWrap).find('.popup_outer').find('.popup').popUp();
	});

	$(this).addClass('pop_open');

}


$(function(){

	var win_h = $(window).height();
	var win_w = $(window).width();
	var $galleryIdx;
	var $imgSize;
	// 팝업
	$(document).on('click' ,'.lay_pop', function(e){
		var $id = $(this).attr('data-id');
		var $idx = $(this).parent().index();
		$galleryIdx = $(this).parent().index();
		var $url = '../popup/';
		var $objURL = $(this).attr('href');		

		if($id == 'ticketing'){
			$url = '../ticket/';
			$id = 'step01';
		}
//		console.log($galleryIdx);
//		console.log($(this).parent().hasClass("popup_outer"));

		if($(this).attr('data-id') == 'ticketing'){
			var popWrap = $('<div class="popup_outer">').load($url + $id +'.do', function(){
				if ($(this).parent().hasClass("popup_outer") == false) {
					var obj = popWrap.appendTo('body').find('.popup');
					var objH = obj.height();
						objH = obj.height();
						obj.popUp(objH);
				}
			});
		}else if($(this).attr('data-id').indexOf("login_pop") > -1){
			var $goUrl = $url + 'login_pop.do';
			if($id.replace('login_pop', '').length > 0){
				$goUrl += '?loginType=' + $id.replace('login_pop', '');
			}
			var popWrap = $('<div class="popup_outer">').load($goUrl, function(){
				var obj = popWrap.appendTo('body').find('.popup');
				var objH = obj.height();
				objH = obj.height();
				obj.popUp(objH);
			});
		}else {
			var popWrap = $('<div class="popup_outer">').load($url + $id +'.do', function(){
				var $playerSize = popWrap.find('#player').size();
				var $viewerSize = popWrap.find('#imgViewer').size();
				var obj = popWrap.appendTo('body').find('.popup');
				var objH = obj.height();

				//var $imgIndex = $idx;
				if($objURL != undefined && $playerSize != 0){ //동영상 일때
					popWrap.find('#player').attr('src',$objURL);
					objH = obj.height();
					obj.popUp(objH);
				}else if($objURL != undefined && $viewerSize != 0){ // 이미지 일떄
					var $img =$('<img src=' + $objURL + ' alt="" data-index=' + ($idx + 1) + ' />');
					$imgSize = $('.popGallery li').size();
					$('#imgViewer').append('<button class="prev">이전</button><button class="next">다음</button>');
					imgResize(obj, $img, $imgSize, objH, $idx);
				}else{
					objH = obj.height();
					obj.popUp(objH);
				}
			});
		}

		$(this).addClass('pop_open');

		e.preventDefault();
	});

	$(document).on('click','#imgViewer button', function(){
		var $class = $(this).attr('class');
		var $imgSize = $('.popGallery li').size();
		if($class == "next"){
			++$galleryIdx;
			if($galleryIdx >= $imgSize - 1){
				//$galleryIdx = $imgSize - 1;
				$('#imgViewer .next').fadeOut();
			}else{
				$('#imgViewer .prev').fadeIn();
			}

		}else{
			--$galleryIdx;
			if($galleryIdx <= 0){
				//$galleryIdx = $imgSize - 1;
				$('#imgViewer .prev').fadeOut();
			}else{
				$('#imgViewer .next').fadeIn();
			}
		}
		console.log($galleryIdx, $imgSize - 1);
		var $objURL = $('.popGallery li:eq(' + (parseInt($galleryIdx)) + ') a').attr('href');

		var $img =$('<img src=' + $objURL + ' alt="" data-index=' + (parseInt($galleryIdx)) + ' />');
		var $index = $galleryIdx;
		imgResize($('.popup_outer .popup'), $img, $imgSize);

	});

	function imgResize(obj, imgObj, imgSize, objH, $imgIdx){
		imgObj.load(function(){
			$('#imgViewer img').remove();
			obj.find('#imgViewer').append(imgObj);
			objH = obj.height();
			obj.popUp(objH);
			obj.addClass('imgViewPop');
			//console.log($viewerSize);
		});
	}


	$.fn.popUp = function(elemH){
		var $this = $(this);
		var objW = $this.outerWidth();
		var objH = elemH;
		var scroll = $(window).scrollTop();
		if($('.pop_msk').css('display') != 'block'){
			$this.parent().append('<div class="pop_msk" style="height:' +win_h+ 'px"></div>');
		}
		$this.css({top:win_h/2 - objH/2 + scroll, left:win_w/2 - objW/2});
		$(window).trigger('resize');
		$this.find('.fisrt_focus').focus();
		if($this.find('.last_btn').size() == 0){
			$this.append('<button class="last_btn btn_ico"></button>');
		}
		$this.parent().addClass('pop_open');
		//console.log($this.find('.last_btn').size());
	};


	$(document).on({
		click : function(e){
			var $id = $(this).closest('.popup').attr('id');
			var $this = $('#' + $id);
			console.log($this.parent());
			$this.parent().fadeOut(200, function(){
				$(this).closest('.popup_outer').remove();
				$('body').find('.pop_open').focus();
				if($('.popup_outer').size() == 0){
					$('body').removeClass('overhidden');
					$('.popup_outer').remove();
				}
			});
			$this.parent().removeClass('pop_open');
		},
		focusout :function(e){
			var $id = $(this).closest('.popup').attr('id');
			var $this = $('#' + $id);
			if($(this).hasClass('pop_close')){
				$this.find('.fisrt_focus').focus();
			}
		}
	},'.pop_close, .popup .btn_close');

	$(window).on('resize', function(){
		win_h = $(window).height();
		win_w = $(window).width();
		var scroll = $(window).scrollTop();
		$.each($('.popup'), function(){
			if($(this).parent().css('display') == 'block'){
				var $thisHeight = $(this).outerHeight();
				if($thisHeight > win_h || $(this).attr('id') == 'ticketing'){
					$(this).css({top:'0px', left:win_w/2 - $(this).outerWidth()/2});
					$(this).parent().css({top:scroll, width:'100%', height:win_h, overflow:'auto'});
					$('body').addClass('overhidden');
				}else{
					$(this).css({top:(win_h/2 - $thisHeight/2) + scroll, left:win_w/2 - $(this).outerWidth()/2});
				}
			}
		});
		$('.pop_msk').css('height',win_h);
	});
});


function popupTicket( btnEl, $url, $params ){
	var $this = btnEl;
	var $id = $this.attr('id');
//	console.log($url + $id +'.do?' + $params);
	$('#ticketing').load($url + $id +'.do?' + $params, function(){
		$(window).trigger('resize');
	});
};

function popupTicket2( btnEl, $url, $params ){
	var $id = btnEl;
	var layer = "#ticketing";
	if($id == "step01"){
		$('.popup_outer').load($url + $id +'.do?' + $params, function(){
			$(window).trigger('resize');
			if(!$(this).hasClass('pop_msk')){
				var win_h = $(window).height();
				$('.popup_outer').append('<div class="pop_msk" style="height:' +win_h+ 'px"></div>');
			}
		});
	}else{
		$('#ticketing').load($url + $id +'.do?' + $params, function(){
			$(window).trigger('resize');
		});
	}
};

function popupTicket3( $url, $params ){
	var $id = 'poll';
//	console.log($url + $id +'.do?' + $params);
	var popWrap = $('<div class="popup_outer">').load($url + $id +'.do?'+$params, function(){
		$('body').append(popWrap).find('.popup_outer').find('.popup').popUp();
	});
};

function popupTicket4( btnEl, $url, $params ){
	var $this = btnEl;
	var $id = $this.attr('id');
//	console.log($url + $id +'.do?' + $params);
	var popWrap = $('<div class="popup_outer">').load($url + $id +'.do?'+$params, function(){
		var obj = popWrap.appendTo('body').find('.popup');
		var objH = obj.height();
		objH = obj.height();
		obj.popUp(objH);
	});
};
