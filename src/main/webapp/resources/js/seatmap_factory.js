function createSelectorService() {
    // const selectorApi = new SelectorApiMock();
    // const selectorApi = new SelectorApiImpl('http://218.144.134.103:3000');
    const selectorApi = new SelectorApiImpl('http://175.125.91.99:3000');

    // const viewerpApi = new ViewerApiMock();
    // const viewerpApi = new ViewerApiImpl('http://218.144.134.103:3000');
    const viewerpApi = new ViewerApiImpl('http://175.125.91.99:3000');
    
    return new SelectorService(selectorApi, viewerpApi);
}

function createSelectorView(service,  glCanvas, ctxCanvas) {
    const mainView = new MainmapView(service, glCanvas, ctxCanvas);

    service.addView(mainView);

    const seatmapController = new SelectorController(service, ctxCanvas, mainView);

    return mainView;
}

function createViewerView(service, glCanvas, ctxCanvas) {
    const mainView = new MainmapView(service, glCanvas, ctxCanvas);

    service.addView(mainView);

    const seatmapController = new ViewerController(ctxCanvas, mainView);

    return mainView;
}

function createMinimap(service, mainView, glMinimapCanvas, ctxMinimapCanvas) {
    const minimapView = new MinimapView(service, mainView, glMinimapCanvas, ctxMinimapCanvas);

    service.addView(minimapView);

    const minimapController = new MinimapController(ctxMinimapCanvas, minimapView);

    return minimapView;
}

function isSeatmapAvailable(canvas) {
    try {
        let gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");

        return gl != null;
    }
    catch (error) { }

    return false;
}