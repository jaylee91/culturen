$(function(){

	var win_h = $(window).height();
	var win_w = $(window).width();
	var menuArr = [];
	var gnbliHeight = $('#gnb > ul > li > ul > li').outerHeight();

	// gnb
	function chk(){
		var mh = menuArr[0];
		if(cc == 1){
			$('.gnb_bg').css('height',gnbliHeight * mh + 40);
			$("#gnb ul li ul").css({height:gnbliHeight * mh + 10,paddingTop:'15px'});
		}else{
			$('.gnb_bg').css('height','0px');
			$("#gnb ul li ul").css({height:'0',paddingTop:'0'});
		}
	}


	$.each($('#gnb > ul > li > ul'),function(i,v){
		menuArr.push($(v).find('li').size());
		menuArr.sort(function(a,b){
			return b-a;
		});
	});



	$(document).on('focusin mouseover','#gnb ul li a', function(){
		setTimeout(chk, 200);
		cc = 1;
		$(this).parent().addClass('active');
	});

	$(document).on('focusout mouseout','#gnb ul li a', function(){
		setTimeout(chk, 200);
		cc = 0;
		$(this).parent().removeClass('active');
	});

	$(document).on('focusin mouseover','.gnb_bg, #gnb ul li ul, #gnb ul li ul li a', function(){
		setTimeout(chk, 200);
		cc = 1;
		$(this).closest('UL').parent().addClass('active');
	});

	$(document).on('focusout mouseout','.gnb_bg, #gnb ul li ul', function(){
		setTimeout(chk, 200);
		cc = 0;
		$(this).closest('UL').parent().removeClass('active');
	});


	// 전체메뉴
	$(document).on('click' ,'.allmenu .all', function(e){
		var $this = $(this);
		var $wh = $(window).outerHeight();
		$('body').append('<div class="pop_msk"></div>');
		$('.pop_msk').css({height:$wh,zIndex:'800'});
		if($this.parent().hasClass('on')){
			$this.parent().removeClass('on');
			$('body').find('.pop_msk').remove();
			$this.find('span').text('열기');
		}else{
			$this.parent().addClass('on');
			$this.find('span').text('닫기');
		}
		e.preventDefault();
	});

	$(document).on('focusout' ,'.all_list a', function(){
		var $this = $(this);
		var $target =  $('.all_list > ul > li');
		var $dep1_length = $target.size();
		var $idx = $this.closest('.dep01').index() + 1;
		if($dep1_length == $idx){
			var $li_length = $this.closest('.dep01').find('ul li').size();
			var $li_idx = $this.parent().index() + 1;
			if($li_length == $li_idx){
				$('.all').focus();
			}
		}
	});

	//TOP버튼

	$(document).on('click' ,'.btn_top', function(e){
		var $this = $(this);
		$('html, body').animate({scrollTop:'0px'},{queue: false, duration: 200});
		e.preventDefault();
	});

	$(window).on('scroll', function(){
		var scrollT = $(window).scrollTop();
		if(scrollT > 200){
			$('.btn_top').css('display','block');
		}else{
			$('.btn_top').css('display','none');
		}
	});

	// 관람후기
	$(document).on('click' ,'.view_writen', function(e){
		var $this = $(this);
		if($this.hasClass('on')){
			$this.closest('TBODY').find('TR:nth-child(even)').css('display','none');
			$('.view_writen').removeClass('on');
		}else{
			$this.closest('TBODY').find('TR:nth-child(even)').css('display','none');
			$this.closest('TR').next().css('display','table-row');
			$('.view_writen').removeClass('on');
			$this.addClass('on');
		}
		e.preventDefault();
	});


	// 탭메뉴
	$.each($('.tab_wrap'), function(){
		var tab_btn = $(this).find('.tab_header .tab_btn');
		var tab_index = tab_btn.index();
		var tab_body = $(this).find('.tab_body');
		var tab_con_box = tab_body.find('.tab_con_box');
		tab_btn.on('click', function(){
			tab_btn.removeClass('selected');
			$(this).addClass('selected');
			tab_con_box.hide();
			tab_con_box.eq($(this).index()).show();
		});
	});

	//공연상세 좋아요
	$(document).on('click' ,'.btn_like', function(){
		var $this = $(this);
		var $count = $('#likeCnt').val();
		var $count = parseInt($count);

		if($this.hasClass('like_chk')){ // -좋아요
			$this.removeClass('like_chk');
			if($count == 1){
				$this.find('.txt').text('좋아요');
			}else{
				$this.find('.txt').text($count - 1);
			}
			if($count > 0)
				$('#likeCnt').val($count-1);

			likeBtnClick('m');
		}else{ //좋아요 버튼 클릭
			$this.addClass('like_chk');
			if($.isNumeric($count)){
				$this.find('.txt').text($count + 1);
			}else{
				$this.find('.txt').text(1);
			}
			$('#likeCnt').val($count+1);
			likeBtnClick('a');
		}
	});

	//버튼 select
	$(document).on('click' ,'.sel_list button', function(){
		var $this = $(this);
		$this.closest('UL').find('button').removeClass('selected');
		$this.addClass('selected');
	});

	//결제수단 선택
	$(document).on('click' ,'.pay_tb input[type="radio"]', function(){
		var $this = $(this);
		var $idx = $this.closest('.item').index();
		var $target = $('.pay_kind_body');
		$target.find('li').hide();
		$target.find('li:eq('+$idx+')').show();
		console.log($idx);
	});

	//할인정보 더보기
	$(document).on('click' ,'.btn_sail_info', function(){
		var $this = $(this);
		if($this.closest('.sail_info').hasClass('layer_on')){
			//$this.next().hide();
			$this.closest('.sail_info').removeClass('layer_on');
		}else{
			//$this.next().show();
			$this.closest('.sail_info').addClass('layer_on');
		}
	});

	//할인정보 닫기
	$(document).on('click' ,'.layer_pop .pop_close', function(){
		var $this = $(this);
		//$(this).closest('.layer_pop').hide();
		$('.sail_info').removeClass('layer_on');
	});

	//관련사이트
	$(document).on('click' ,'.family_btn', function(e){
		var $this = $(this);
		if($this.next().hasClass('on')){
			$this.next().removeClass('on');
			$this.removeClass('selected');
		}else{
			$this.next().addClass('on');
			$this.addClass('selected');
		}
		e.preventDefault();
	});

	$('body').on('click', function(e){
		var $target = e.target;
		if(!$($target).hasClass('family_btn') && !$($target).closest('.sail_info').hasClass('layer_on') || $($target).parent().hasClass('pop_close')){
			$('.family_btn').next().removeClass('on');
			$('.family_btn').removeClass('selected');
			$('.sail_info').removeClass('layer_on');
		}
	});

	//상단검색
	$(document).on('focusin' ,'.t_search_wrap .comm_input', function(){
		var $this = $(this);
		$this.parent().addClass('on');
	});

	$(document).on('focusout' ,'.t_search_wrap .comm_input', function(){
		var $this = $(this);
		$this.parent().removeClass('on');
	});

	//별점

	$(document).on({
		click : function(){
			var $this = $(this);
			var $obj_width = $this.parent().outerWidth();
			var $pos = $this.parent().position().left;
			$this.closest('.dafault_bg').find('.bar').css('width',$pos + $obj_width);
			$this.parent().parent().find('span').removeClass('on');
			$this.parent().addClass('on');
		},
		mouseenter : function(){
			var $this = $(this);
			var $obj_width = $this.parent().outerWidth();
			var $pos = $this.parent().position().left;
			$this.closest('.dafault_bg').find('.bar').css('width',$pos + $obj_width);
		},
		mouseleave : function(){
			var $this = $(this);
			var $obj_width = 0;
			var $pos = 0;
			if($this.parent().parent().find('span.on').size() != 0){
				$obj_width = $this.parent().outerWidth();
				$pos = $this.parent().parent().find('span.on').position().left;
			}

			$this.closest('.dafault_bg').find('.bar').css('width',$pos + $obj_width);
			console.log($this.parent().parent().find('span.on').index());
		}
	}, '.star_sel input[type="radio"]');

	// 이메일 선택
	$(document).on('change' ,'.mail_select select', function(){
		var $this = $(this);
		var $value = $this.val();
		var $idx = $this.find('option:selected').index();
		$this.prev().val($value);
		if($idx != 0){
			$this.prev().attr('disabled','disabled');
		}else{
			$this.prev().removeAttr('disabled','disabled');
		}
	});

	//현금영수증 발급 선택
	$(document).on('click' ,'.input_active_wrap input[type="radio"], .input_active_wrap input[type="checkbox"]', function(){
		var $this = $(this);
		var $type = $this.attr('type');
		if($type == 'radio'){
			$this.closest('.input_active_wrap').find('.radio_b_wrap').next().find('input').attr('disabled','disabled');
			$this.parent().next().find('input').removeAttr('disabled','disabled');
		}else{
			if($this.is(':checked')){
				$this.parent().next().find('input, select').removeAttr('disabled','disabled');
			}else{
				$this.parent().next().find('input, select').attr('disabled','disabled');
			}
		}
	});

	$(document).on('click' ,'.paper_sel_wrap input[type="checked"]', function(){
		var $this = $(this);
		$this.closest('.paper_sel_wrap').find('.check_b_wrap').next().find('input, select').attr('disabled','disabled');
		$this.parent().next().find('input, select').removeAttr('disabled','disabled');
		console.log(32323);
	});

	//좌석선택 하단 구분박스
	$(document).on('click' ,'.seat_gubun .seat_more', function(){
		var $this = $(this);
		if($this.parent().hasClass('seat_more_close')){
			$this.parent().removeClass('seat_more_close');
		}else{
			$this.parent().addClass('seat_more_close');
		}
	});

	//예매날짜선택시 회차, 잔여석 노출 //20180212 기능삭제
	//$(document).on('click', '.time_sel .sel_list button', function(){
	//	$('.seat_sel p').hide();
	//	$('.seat_sel ul').show();
    //
	//	if($("#entranceType").val() == "E"){
	//		var selectFullTime = $('span', this).text();
	//		var divideTime = selectFullTime.split(" ~ ");
	//		var startTime = divideTime[0];
	//		var endTime = divideTime[1];
	//		$('#selected_time').val(startTime + " ~ " + endTime );
	//		$('#entranceStartTime').val(startTime);
	//		$('#entranceEndTime').val(endTime);
	//		$('#ordNum').val(divideTime[2]);
	//	}
	//});
    //
	$(document).on({
		mouseenter:function(){
			var $this = $(this);
			$this.next().show();
		},
		mouseout:function(){
			var $this = $(this);
			$this.next().hide();
		}
	},'.tip_use_info');

	//문화인물 사진 위치조정
	function actorList(){
		var $picH = $('.actor_list .pic').outerHeight();
		var $img = $('.actor_list .pic img');
		var $imgH = $img.outerHeight();
		if($imgH > $picH){
			$img.css('margin-top',-($imgH - $picH)/2);
		}
	}
	actorList();

	$('#gnb > ul > li > a').each(function(){
		if($('title').text().substring($('title').text().indexOf('<')).indexOf($(this).text()) > -1){
			$(this).parent().addClass("selected");
		}
	});

	$('.abs_menu_body > ul > li > a').each(function(){
		if($('title').text().indexOf($(this).text()) > -1){
			$(this).parent().addClass("selected");
		}
	});

	$('img').error(function () {
		$(this).attr('src', '../assets/img/poster_noimg.gif');
	}).each(function() {
		$(this).attr("src", $(this).attr("src"));
	});

});

function inputActive(){
	$.each($('.input_active_wrap input[type="radio"], .input_active_wrap input[type="checkbox"]'), function(i,v){
		if(!$(v).is(':checked')){
			$(v).parent().next().find('input, select').attr('disabled','disabled');
		}
		console.log(212);
	});
}