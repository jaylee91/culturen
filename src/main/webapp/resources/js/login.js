// placeholder
var setPlaceholderAuto = function () {

	var msieVersion = 0;

	if (navigator.appName.toLowerCase() == 'microsoft internet explorer') {
		var ua = navigator.userAgent.toLowerCase();
		var re = new RegExp("msie ([0-9]{1,}[/.0-9]{0,})");

		if (re.exec(ua) != null) {
			msieVersion = parseFloat(RegExp.$1);
		}
	}

	if (msieVersion > 9 || msieVersion == 0) {
		return false;
	}

	// ie9 �댄븯�먯꽌留� �숈옉
	$('input[placeholder][type=text], textarea[placeholder]').each(function (i) {
		try {
			$(this).data({
				type			:	$(this).attr('type')
				,placeholder	:	$(this).attr('placeholder')
				,color			:	$(this).css('color')
			});

			$(this).off('focus blur').on({
				focus : function () {
					var data = $(this).data();

					$(this).css('color', data.color);

					if (data.type == 'text' || data.type == 'password') {
						// $(this).attr('type', data.type);
						this.type = data.type;
					}

					if ($(this).val() == data.placeholder) {
						$(this).val('');
					}
				},
				blur : function () {
					var data = $(this).data();

					if ($(this).val() == '' || $(this).val() == data.placeholder) {
						$(this).css('color', '#ccc');
						
						if (data.type == 'text' || data.type == 'password') {
							// $(this).attr('type', 'text');
							this.type = 'text';
						}

						$(this).val(data.placeholder);

					} else {
						$(this).css('color', data.color);
						
						if (data.type == 'text' || data.type == 'password') {
							// $(this).attr('type', data.type);
							this.type = data.type;
						}
					}
				}
			}).blur();			
		} catch (e) {
		}
	});
};

$(function () {
	
	setPlaceholderAuto();
	
});