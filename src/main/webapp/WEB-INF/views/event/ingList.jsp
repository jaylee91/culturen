<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../layout/inc_top.jsp"/>

<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm04">
						<li class="selected"><a href="/culturen/ingList.action">진행중 이벤트</a></li>
						<li><a href="/culturen/endList.action">지난 이벤트</a></li>
						<li><a href="/culturen/winList.action">당첨자 확인</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->				

			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>진행중 이벤트</h2>
						<p>다양한 선물과 공연 초대가 준비되어 있습니다.</p>
					</div>
					
					<form action="" method="post">
					<div class="ev_tab">
					<c:if test="${dto.evMode eq '' ||dto.evMode == null }">	
						<ul>
							<li><a href="/culturen/ingList.action?evMode=A" class="on">초대</a></li>
							<li><a href="/culturen/ingList.action?evMode=B" >경품</a></li>
							<li><a href="/culturen/ingList.action?evMode=C" >일반</a></li>
						</ul>
					</c:if>
					<c:if test="${dto.evMode eq 'A' }">	
						<ul>
							<li><a href="/culturen/ingList.action?evMode=A" class="on">초대</a></li>
							<li><a href="/culturen/ingList.action?evMode=B" >경품</a></li>
							<li><a href="/culturen/ingList.action?evMode=C" >일반</a></li>
						</ul>
					</c:if>
						<c:if test="${dto.evMode eq 'B' }">	
						<ul>
							<li><a href="/culturen/ingList.action?evMode=A" >초대</a></li>
							<li><a href="/culturen/ingList.action?evMode=B" class="on">경품</a></li>
							<li><a href="/culturen/ingList.action?evMode=C" >일반</a></li>
						</ul>
					</c:if>
						<c:if test="${dto.evMode eq 'C' }">	
						<ul>
							<li><a href="/culturen/ingList.action?evMode=A" >초대</a></li>
							<li><a href="/culturen/ingList.action?evMode=B" >경품</a></li>
							<li><a href="/culturen/ingList.action?evMode=C" class="on">일반</a></li>
						</ul>
					</c:if>
					
					<input type="hidden" name="evMode" value="${evMode}">
					</form>
					
					</div>
					
					<div class="event_list">
												
						<ul>
							
						</ul>
					</div>
					<div class="pageing">
						
					</div>				

				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			
			<jsp:include page="../layout/inc_footer.jsp"/>	
			
