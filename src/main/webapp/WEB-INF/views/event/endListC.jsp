<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../layout/inc_top.jsp"/>

<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
	
</script>  

 
<!--E: Top -->

<!--S: container -->
<div class="container">
	

<!--S: lnb -->
<div class="lnb abs_menu">
	<div class="abs_menu_body">
		<ul class="sm04">
			<li><a href="/culturen/ingList.action">진행중 이벤트</a></li>
			<li class="selected"><a href="/culturen/endList.action">지난 이벤트</a></li>
			<li><a href="/culturen/winList.action">당첨자 확인</a></li>
		</ul>
	</div>
</div>
<!--E: lnb -->
	






<div class="container_body">
	<div class="content_wrap" id="contents">
		<div class="title_wrap">
			<h2>지난 이벤트</h2>
			<p>다양한 선물과 공연 초대가 준비되어 있습니다.</p>
		</div>

		<div class="ev_tab">
			<ul>
				<li><a href="/culturen/endList.action?eventtype=A" >초대</a></li>
				<li><a href="/culturen/endList.action?eventtype=B" >경품</a></li>
				<li><a href="/culturen/endList.action?eventtype=C" class="on">일반</a></li>
			</ul>
		</div>
		
		<div>
			<button>글쓰기</button>
		
		
		</div>
		
		<c:if test="${dto.evMode eq 'C' }">		
			<div class="event_list">
				<ul>
					<c:forEach var="dto" items="${lists}">
					<li>
						<div class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/fa396616e0a44279b6ca37fc0ea98214.jpg" style="width:148px;height:200px;" alt="문화N티켓 오픈기념 이벤트" /></div>
						<dl>
							<dt>${dto.evSubject }</dt>
							<dd class="label"><span class="btn btn_mini btn_lightGray">관람후기</span></dd>
							<dd class="first"><span class="tit">기간</span> : ${dto.evSdate } ~ ${dto.evEdate }</dd>
							<dd><span class="tit">발표</span> : ${dto.evAnnouncement }</dd>				
							<dd><span class="tit">초대</span> : ${dto.evInvite }</dd>
							<dd><span class="tit">장소</span> : ${dto.evPlace }</dd>					
						</dl>
					</li>
					</c:forEach>
				</ul>
			</div>
		</c:if>
		
		
		
		
		<div class="pageing">
			
				<a href="?pageNo=1&amp;eventtype=C" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1&amp;eventtype=A" class="last"><span>마지막 페이지 이동</span></a>
			
		</div>

		

	</div>
</div>
</div>
<!--E: container -->

<!--S: footer -->


<jsp:include page="../layout/inc_footer.jsp"/>	
