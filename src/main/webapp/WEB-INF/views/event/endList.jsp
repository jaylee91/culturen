<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="../layout/inc_top.jsp"/>

<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm04">
						<li><a href="/culturen/ingList.action">진행중 이벤트</a></li>
						<li class="selected"><a href="/culturen/endList.action">지난 이벤트</a></li>
						<li><a href="/culturen/winList.action">당첨자 확인</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				






			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>지난 이벤트</h2>
						<p>다양한 선물과 공연 초대가 준비되어 있습니다.</p>
					</div>

					<div class="ev_tab">
						<ul>
							<li><a href="/culturen/endList.action?eventtype=A" class="on">초대</a></li>
							<li><a href="/culturen/endList.action?eventtype=B" >경품</a></li>
							<li><a href="/culturen/endList.action?eventtype=C" >일반</a></li>
						</ul>
					</div>
					
					
					<div class="event_list">
						
						
						<ul>
							
							<li>
								
								<div class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/fa396616e0a44279b6ca37fc0ea98214.jpg" style="width:148px;height:200px;" alt="문화N티켓 오픈기념 이벤트" /></div>
								<dl>
									<dt>문화N티켓 오픈기념 이벤트</dt>
									
									<dd class="label"><span class="btn btn_mini btn_lightGray">관람후기</span></dd>
									
									<dd class="first"><span class="tit">기간</span> : 2018-01-06 ~ 2018-01-22</dd>
									<dd><span class="tit">발표</span> : 2018-01-25</dd>
									
									
									<dd><span class="tit">초대</span> : 2018-01-08 월12:00 263 쌍</dd>
									
									<dd><span class="tit">장소</span> : 강화군문예회관</dd>
									
									
									
									
								</dl>
							</li>
							
							<li>
								
								<div class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000014/product/aa856647ece24f8aa4f1ea761769dddf.jpg" style="width:148px;height:200px;" alt="[오픈이벤트] 롱디(LONG:D) 앨벌판매 콘서트 " /></div>
								<dl>
									<dt>[오픈이벤트] 롱디(LONG:D) 앨벌판매 콘서트 </dt>
									
									<dd class="label"><span class="btn btn_mini btn_lightGray">관람후기</span></dd>
									
									<dd class="first"><span class="tit">기간</span> : 2018-01-06 ~ 2018-01-22</dd>
									<dd><span class="tit">발표</span> : 2018-01-25</dd>
									
									
									<dd><span class="tit">초대</span> : 2018-01-08 월12:00 25 쌍</dd>
									
									<dd><span class="tit">장소</span> : KT&G 상상마당 라이브홀</dd>
									
									
									
									
								</dl>
							</li>
							
							<li>
								
								<div class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/e97f7462ef484e8f94eb2106c89a1595.jpg" style="width:148px;height:200px;" alt="[오픈이벤트] 산울림 고전극장" /></div>
								<dl>
									<dt>[오픈이벤트] 산울림 고전극장</dt>
									
									<dd class="label"><span class="btn btn_mini btn_lightGray">관람후기</span></dd>
									
									<dd class="first"><span class="tit">기간</span> : 2018-01-06 ~ 2018-01-22</dd>
									<dd><span class="tit">발표</span> : 2018-01-25</dd>
									
									
									<dd><span class="tit">초대</span> : 2018-01-08 월12:00 18 쌍</dd>
									
									<dd><span class="tit">장소</span> : 산울림 소극장</dd>
									
									
									
									
								</dl>
							</li>
							
							<li>
								
								<div class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/5e393c2ccd5c44688ca1ea79a5749baa.jpg" style="width:148px;height:200px;" alt="[오픈이벤트] 관객과의 전쟁" /></div>
								<dl>
									<dt>[오픈이벤트] 관객과의 전쟁</dt>
									
									<dd class="label"><span class="btn btn_mini btn_lightGray">관람후기</span></dd>
									
									<dd class="first"><span class="tit">기간</span> : 2018-01-06 ~ 2018-01-22</dd>
									<dd><span class="tit">발표</span> : 2018-01-25</dd>
									
									
									<dd><span class="tit">초대</span> : 2018-01-08 월12:00 25 쌍</dd>
									
									<dd><span class="tit">장소</span> : 홍대 윤형빈소극장</dd>
									
									
									
									
								</dl>
							</li>
							
							<li>
								
								<div class="pic"><img src="/ticket/uploads//TA000014/product/b8c9e67f74024988af478ce9ea02cc4c.gif" style="width:148px;height:200px;" alt="[오픈이벤트] 찰리와 초콜릿 공장 원화작가 퀀틴 블레이크 초대 이벤트" /></div>
								<dl>
									<dt>[오픈이벤트] 찰리와 초콜릿 공장 원화작가 퀀틴 블레이크 초대 이벤트</dt>
									
									<dd class="label"><span class="btn btn_mini btn_lightGray">관람후기</span></dd>
									
									<dd class="first"><span class="tit">기간</span> : 2018-01-06 ~ 2018-01-22</dd>
									<dd><span class="tit">발표</span> : 2018-01-25</dd>
									
									
									<dd><span class="tit">초대</span> : 2018-01-08 월12:00 100 쌍</dd>
									
									<dd><span class="tit">장소</span> : KT&G 상상마당 갤러리</dd>
									
								</dl>
							</li>
							
						</ul>
					</div>
					
					<div class="pageing">
						
							<a href="?pageNo=1&amp;eventtype=A" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1&amp;eventtype=A" class="last"><span>마지막 페이지 이동</span></a>
						
					</div>

					

				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			
			
			<jsp:include page="../layout/inc_footer.jsp"/>	
			