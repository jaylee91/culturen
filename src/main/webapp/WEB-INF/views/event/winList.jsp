<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="../layout/inc_top.jsp"/>


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm04">
						<li><a href="/culturen/ingList.action">진행중 이벤트</a></li>
						<li><a href="/culturen/endList.action">지난 이벤트</a></li>
						<li class="selected"><a href="/culturen/winList.action">당첨자 확인</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				





				
				<div class="container_body">
					<div class="content_wrap" id="contents">
						<div class="title_wrap">
							<h2>당첨자 확인</h2>
							<p>이벤트 당첨을 축하합니다.</p>
						</div>
				
						<div class="board_search_wrap pdb30">
							<div class="fr">
								<label for="FIELD" class="hidden">검색구분</label>
								<form method="get">
									<select name="category" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
										<option value="0" selected>전체</option>
										<option value="1" >제목</option>
										<option value="2" >내용</option>
									</select> 
									<label for="KEY" class="hidden">검색어</label> 
									<input type="text" name="keyword" placeholder="검색어를 입력하세요." title="검색어 입력" class="comm_input scr_elem" value="" />
									<button class="btn btn_search scr_elem">검색</button>
								</form>
							</div>
						</div>
				
						<table class="board_list">
							<caption>번호, 제목, 발표일로 구성된 이벤트 당첨자 확인 목록입니다.</caption>
							<colgroup>
								<col style="width: 10%" />
								<col style="width: *" />
								<col style="width: 10%" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">번호</th>
									<th scope="col">제목</th>
									<th scope="col">발표일</th>
								</tr>
							</thead>
							<tbody>
								
								
									<tr>
										<td>2</td>
										<td class="al subject">
											<a href="/culturen/winList.action?seq=2">
												<span class="f_red">[일반]</span> 문화N티켓 오픈 이벤트 '제35회 라이브클럽데이' 당첨자 대상 추가 안내 
											</a>
										</td>
										<td>2018-01-29</td>
									</tr>
								
									<tr>
										<td>1</td>
										<td class="al subject">
											<a href="/culturen/winList.action?seq=1">
												<span class="f_red">[일반]</span> 문화N티켓 오픈기념 이벤트 당첨자 발표
											</a>
										</td>
										<td>2018-01-25</td>
									</tr>
								
							</tbody>
						</table>
				
						<div class="pageing">
							
								<a href="?pageNo=1" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1" class="last"><span>마지막 페이지 이동</span></a>
							
						</div>
				
					</div>
				</div>
				</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			
			<jsp:include page="../layout/inc_footer.jsp"/>	