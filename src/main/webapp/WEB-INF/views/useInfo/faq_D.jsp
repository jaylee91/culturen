<%@ page contentType="text/html; charset=UTF-8"%>
<jsp:include page="../layout/inc_top.jsp"/>


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li class="selected"><a href="faq.action">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				






			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>FAQ</h2>
						<p>가장 많이 물어보신 질문입니다.</p>
					</div>

					<div class="board_search_wrap sort_wrap mgb30">
						<div class="fl">
							<a href="faq.action" class="btn  w100"><span class="txt">예매/결제</span></a>
							<a href="faq_B.action" class="btn  w100"><span class="txt">취소/환불</span></a>
							<a href="faq_C.action" class="btn  w100"><span class="txt">회원</span></a>
							<a href="faq_D.action" class="btn btn_red w100"><span class="txt">기타</span></a>
						</div>
						<form method="get">
						<div class="fr">
							<label for="FIELD" class="hidden">검색구분</label>
							<select name="category" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
								<option value="0" selected="">전체</option>
								<option value="1">제목</option>
								<option value="2">내용</option>
							</select>
							<label for="KEY" class="hidden">검색어</label>
							<input type="text" name="keyword" placeholder="검색어를 입력하세요." title="검색어 입력" class="comm_input scr_elem" value="">
							<button class="btn btn_search scr_elem">검색</button>
							<input type="hidden" name="faqtype" value="D">
						</div>
						</form>
					</div>
					
					<table class="board_list writen_list faq_list">
						<caption>분류, 제목 구성된 FAQ 목록이며 질문 클릭시 상세내용이 펼쳐지며 보여집니다.</caption>
						<colgroup>
							<col style="width:10%">
							<col style="width:*">
						</colgroup>
						 <thead>
							  <tr>
								<th scope="col">분류</th>
								<th scope="col">제목</th>
							  </tr>
						 </thead>
						 <tbody>
						 	  
							  <tr>
								<td>기타</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>이벤트 티켓 양도가 가능한가요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">네</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">. </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>기타</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>이벤트에 당첨되었습니다. 티켓은 어떻게 받나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">당첨되신 일자에 공연시작 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">30</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">분 전까지
매표소로 가셔서 문화</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">N</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">티켓
초대자임을 밝힌 후 신분증 확인 후에 티켓을 받으실 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span><br></p>
									</div>
								</td>
							  </tr>
							  
						 </tbody>
					</table>
					
					<div class="pageing">
						
							<a href="?pageNo=1" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1" class="last"><span>마지막 페이지 이동</span></a>
						
					</div>
				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>