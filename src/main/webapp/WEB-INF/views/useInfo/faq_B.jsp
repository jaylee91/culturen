<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="../layout/inc_top.jsp"/>


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li class="selected"><a href="faq.action">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				






			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>FAQ</h2>
						<p>가장 많이 물어보신 질문입니다.</p>
					</div>

					<div class="board_search_wrap sort_wrap mgb30">
						<div class="fl">
							<a href="faq.action" class="btn  w100"><span class="txt">예매/결제</span></a>
							<a href="faq_B.action" class="btn btn_red w100"><span class="txt">취소/환불</span></a>
							<a href="faq_C.action" class="btn  w100"><span class="txt">회원</span></a>
							<a href="faq_D.action" class="btn  w100"><span class="txt">기타</span></a>
						</div>
						<form method="get">
						<div class="fr">
							<label for="FIELD" class="hidden">검색구분</label>
							<select name="category" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
								<option value="0" selected="">전체</option>
								<option value="1">제목</option>
								<option value="2">내용</option>
							</select>
							<label for="KEY" class="hidden">검색어</label>
							<input type="text" name="keyword" placeholder="검색어를 입력하세요." title="검색어 입력" class="comm_input scr_elem" value="">
							<button class="btn btn_search scr_elem">검색</button>
							<input type="hidden" name="faqtype" value="B">
						</div>
						</form>
					</div>
					
					<table class="board_list writen_list faq_list">
						<caption>분류, 제목 구성된 FAQ 목록이며 질문 클릭시 상세내용이 펼쳐지며 보여집니다.</caption>
						<colgroup>
							<col style="width:10%">
							<col style="width:*">
						</colgroup>
						 <thead>
							  <tr>
								<th scope="col">분류</th>
								<th scope="col">제목</th>
							  </tr>
						 </thead>
						 <tbody>
						 	  
							  <tr>
								<td>취소/환불</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>예매 후 날짜, 시간, 좌석 등을 변경할 수 있나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매
후에는 날짜</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">시간</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">좌석을 변경할 수는 없지만</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">,</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 부분취소는 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">날짜</span><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:en-US;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">, </span><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">시간</span><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:en-US;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">, </span><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">좌석 변경을 원하시면 전체 취소 후 다시 예매를 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">시도하셔야</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">
합니다</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">.
</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">(</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">취소하는 경우 취소수수료가 발생할 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.)</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">좌석
일부 취소를 원하시면 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">마이페이지</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">&gt;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">예매</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">/</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">취소
내역</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 메뉴에서 부분 취소가 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>취소/환불</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>예매 후 부분취소가 가능한가요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">네</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">. </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">부분취소는 예매 결제 시 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">신용카드</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">,
</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">실시간계좌이체로
결제한 것에 한하여 부분취소가 가능</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.&nbsp;</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">마이페이지</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">&gt;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">예매</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">/</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: rgb(0, 112, 192); font-weight: bold;">취소
내역</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 메뉴에서 예매 내역을 확인하시고 취소를 원하시는 좌석을 선택
후 취소해주세요</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>취소/환불</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>예매 취소는 어떻게 하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-font-kerning:
12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
#0070C0;mso-style-textfill-fill-alpha:100.0%">마이페이지</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:#0070C0;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;
mso-style-textfill-type:solid;mso-style-textfill-fill-color:#0070C0;mso-style-textfill-fill-alpha:
100.0%">&gt; </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;
mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:
+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:minor-fareast;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
#0070C0;mso-style-textfill-fill-alpha:100.0%">예매</span><span style="font-size:
10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:
&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;
mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;color:#0070C0;
mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:#0070C0;mso-style-textfill-fill-alpha:100.0%">/</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:#0070C0;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:#0070C0;mso-style-textfill-fill-alpha:100.0%">취소
내역</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 메뉴에서 취소 가능 시간 이내 취소 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">취소 가능 시간은 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">관람일
하루 전 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">17:00
</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">까지</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">이며</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">그 이후에는 취소가 불가합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">또한 취소 시점에 따라 취소 수수료가 발생할 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>취소/환불</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>취소수수료는 얼마인가요 ?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">취소
수수료는 관람일 기준으로 적용됩니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매 당일에 취소하는 경우는 수수료가 부과되지 않습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매 후 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">10</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일
이내라도 취소시점이 관람일로부터 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">9</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 이내라면 그에 해당하는 취소수수료가 부과됩니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">. </span></p><p><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">상품의 특성에 따라 취소수수료 정책이 달라질 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.(</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">각 상품 예매 시 취소수수료 확인</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">)&nbsp;</span></p><p><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"><br></span></p><ul><li style="margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매
후 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">~ </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">관람일 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">10</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 전 :&nbsp;</span><span style="color: red; font-family: &quot;맑은 고딕&quot;; font-size: 10pt;">없음</span></li><li style="margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">관람일
</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">9</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 전</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">~7</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 전&nbsp; &nbsp; &nbsp;:&nbsp;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red;">티켓 금액의 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">10%</span></li><li style="margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">관람일
</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">6</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 전</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">~3</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 전&nbsp; &nbsp; &nbsp;:&nbsp;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red;">티켓 금액의 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">20%</span></li><li style="margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">관람일
</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">2</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 전</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">~1</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">일 전&nbsp; &nbsp; &nbsp;:&nbsp;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red;">티켓 금액의 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">30%</span></li></ul>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>취소/환불</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>티켓 환불은 어떻게 받나요</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">결제
방법에 따라 환불 방법이 다릅니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"><br></span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">신용카드로
결제하신 경우</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">&nbsp;: 취소수수료를 제외한 금액이 취소처리 됩니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"><br></span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">실시간계좌이체로
결제하신 경우</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">&nbsp;: 입금하신
계좌정보로 취소 수수료를 제외한 금액을 환불 받습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">&nbsp; &nbsp;단</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매 후 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">1</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">개월이
경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">500</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">원을
제외한 금액이 입금됩니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"><br></span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">가상계좌로
결제하신 경우</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">: </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">환불
계좌번호를 남기고</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">그 계좌를
통해 취소 수수료를 제외한 금액을 환불 받습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">&nbsp;단</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">후 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">1</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">개월이
경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">500</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">원을
제외한 금액이 입금됩니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"><br></span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">휴대폰으로
결제하신 경우</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">: </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">취소 신청
후 바로 취소 처리가 되며 취소 수수료를 제외한 티켓 금액 이용료가 취소 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">&nbsp; 단</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">당월
결제에 대한 취소가 아닌 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">1</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">개월이
경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">500</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">원을
제외한 금액이 입금됩니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>취소/환불</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>내일 공연을 보려고 오늘 예매했습니다. 오후 5시가 지났는데 취소 되나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">취소는 공연 관람일 하루 전 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">17:00</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">까지
이므로 취소가 불가합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span><br></p>
									</div>
								</td>
							  </tr>
							  
						 </tbody>
					</table>
					
					<div class="pageing">
						
							<a href="?pageNo=1" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1" class="last"><span>마지막 페이지 이동</span></a>
						
					</div>
				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>