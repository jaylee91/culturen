<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	request.setCharacterEncoding("UTF-8");
	String cp = request.getContextPath();
%>
<jsp:include page="../layout/inc_top.jsp"/>

<script>
	$(function( ){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
	
</script>
                   
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="guide.action">예매 가이드</a></li>
						<li><a href="faq.action">FAQ</a></li>
						<li><a href="ticketInfo.action">티켓판매 안내</a></li>
						<li class="selected"><a href="notice.action">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				
<script type="text/javascript">

	function sendIt(){
		
		var f = document.searchForm;
		
		f.action = "<%=cp%>/notice.action";
		f.submit();
		
}

</script>





			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>공지사항</h2>
						<p>새로운 소식을 전합니다.</p>
					</div>
					<form name="searchForm" method="get">
					<div class="board_search_wrap pdb30">
						<div class="fr">
							<label for="FIELD" class="hidden">검색구분</label>
							<select name="searchKey" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
								<option value="0" selected="">전체</option>
								<option value="nbSubject">제목</option>
								<option value="nbContent">내용</option>
							</select>
							<label for="KEY" class="hidden">검색어</label>
							<input type="text" name="searchValue" placeholder="검색어를 입력하세요." title="검색어 입력" class="comm_input scr_elem" value="">
							<button class="btn btn_search scr_elem" onclick="sendIt();">검색</button>
						</div>
					</div>
					</form>
					
					<table class="board_list">
						<caption>번호, 제목, 등록일, 조회수로 구성된 공지사항 목록입니다.</caption>
						<colgroup>
							<col style="width:10%">
							<col style="width:*">
							<col style="width:10%">
							<col style="width:10%">
						</colgroup>
						 <thead>
							  <tr>
								<th scope="col">번호</th>
								<th scope="col">제목</th>
								<th scope="col">등록일</th>
								<th scope="col">조회수</th>
							  </tr>
						 </thead>
						 <tbody>
						 	  
							<c:forEach var="dto" items="${lists}">			 	  
							  <tr>
								<td>${dto.listNum}</td>
								<td class="al subject">
								<a href="${articleUrl}&nbNum=${dto.nbNum}">${dto.nbSubject }</a></td>
								<td>${dto.nbRedgate }</td>
								<td>${dto.nbCount }</td>
							  </tr>
							 </c:forEach>
							  
						 </tbody>
					</table>
					
					<div class="pageing">
						
							<c:if test="${dataCount!=0 }">
								${pageIndexList }
							</c:if>
							<c:if test="${dataCount==0 }">
								등록된게시물이 없습니다.
							</c:if>
						
					</div>
					
					<div class="board_btn_wrap">
						<div class="lt_r">
							<a href="noticeInsert.action" class="btn btn_middle btn_dark">글쓰기</a>
						</div>
					</div>
				

					

				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			


<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>