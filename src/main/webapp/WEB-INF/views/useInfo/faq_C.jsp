<%@ page contentType="text/html; charset=UTF-8"%>
<jsp:include page="../layout/inc_top.jsp"/>


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li class="selected"><a href="faq.action">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				






			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>FAQ</h2>
						<p>가장 많이 물어보신 질문입니다.</p>
					</div>

					<div class="board_search_wrap sort_wrap mgb30">
						<div class="fl">
							<a href="faq.action" class="btn  w100"><span class="txt">예매/결제</span></a>
							<a href="faq_B.action" class="btn  w100"><span class="txt">취소/환불</span></a>
							<a href="faq_C.action" class="btn btn_red w100"><span class="txt">회원</span></a>
							<a href="faq_D.action" class="btn  w100"><span class="txt">기타</span></a>
						</div>
						<form method="get">
						<div class="fr">
							<label for="FIELD" class="hidden">검색구분</label>
							<select name="category" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
								<option value="0" selected="">전체</option>
								<option value="1">제목</option>
								<option value="2">내용</option>
							</select>
							<label for="KEY" class="hidden">검색어</label>
							<input type="text" name="keyword" placeholder="검색어를 입력하세요." title="검색어 입력" class="comm_input scr_elem" value="">
							<button class="btn btn_search scr_elem">검색</button>
							<input type="hidden" name="faqtype" value="C">
						</div>
						</form>
					</div>
					
					<table class="board_list writen_list faq_list">
						<caption>분류, 제목 구성된 FAQ 목록이며 질문 클릭시 상세내용이 펼쳐지며 보여집니다.</caption>
						<colgroup>
							<col style="width:10%">
							<col style="width:*">
						</colgroup>
						 <thead>
							  <tr>
								<th scope="col">분류</th>
								<th scope="col">제목</th>
							  </tr>
						 </thead>
						 <tbody>
						 	  
							  <tr>
								<td>회원</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>회원가입은 어떻게 하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; font-weight: bold;">[</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; font-weight: bold;">문화</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; font-weight: bold;">N</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; font-weight: bold;">티켓
신규회원</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; font-weight: bold;">]</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"><br></span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">사이트
상단의 회원가입 메뉴를 클릭하시면 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">문화포털</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">
통합회원 회원가입 팝업창이 보입니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">본인인증</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">회원정보입력</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">약관동의
후 가입이 완료됩니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>회원</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>아이디, 비밀번호를 잊어버렸어요.</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">사이트
상단의 로그인 메뉴를 클릭하시면 아이디</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">/</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">비밀번호
찾기 버튼이 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p>

</p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">본인확인절차로
아이디</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">이름</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">이메일을
입력하고 호가인 버튼을 클릭하면 아이디와 비밀번호를 확인하실 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>회원</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>회원탈퇴는 어떻게 하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">회원의
의사에 따라 자유롭게 탈퇴할 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">로그인 후
마이페이지</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">&gt;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">회원탈퇴
메뉴를 클릭하시면 문화포털 사이트의 회원탈퇴 화면으로 이동합니다.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">문화포털 사이트의 회원탈퇴 화면에서 탈퇴하하실 수 있습니다.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>회원</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>로그인이 안되요</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">로그인이
안될 때는 먼저 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">CapsLock</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">키가
활성화되어 있지 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">않은지</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">
확인하여 아이디와 비밀번호를 정확하게 입력합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">다음으로
아이디와 비밀번호가 틀리지&nbsp; 않았는지 로그인 메뉴를 클릭하시고 로그인
페이지가 나타나면 하단에 아이디</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">/</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">비밀번호
찾기 버튼을 클릭하고 정보를 입력하여 확인합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">위와 같이
한 후에도 로그인이 되지 않으면 고객센터</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">(02-3153-2898)</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">로
연락 주시기 바랍니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
						 </tbody>
					</table>
					
					<div class="pageing">
						
							<a href="?pageNo=1" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1" class="last"><span>마지막 페이지 이동</span></a>
						
					</div>
				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>