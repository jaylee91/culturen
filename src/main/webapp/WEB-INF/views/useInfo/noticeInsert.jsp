<%@ page contentType="text/html; charset=UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	String cp = request.getContextPath();
%>
<jsp:include page="../layout/inc_top.jsp"/>




<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>



                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
			
			
				<script src="https://code.jquery.com/jquery-latest.js"></script>
				<script type="text/javascript" src="./resources/editor/js/HuskyEZCreator.js" charset="utf-8"></script>
				<script type="text/javascript">
				
			

			function sendIt(){

					f = document.myForm;
					
					str = f.nbSubject.value;
					str = str.trim();
					if(!str){
						alert("\n제목을 입력하세요.");
						f.nbSubject.focus();
						return;
					}
					f.nbSubject.value = str;
					
					str = f.nbContent.value;
					str = str.trim();
					if(!str){
						alert("\n내용을 입력하세요.");
						f.nbContent.focus();
						return;
					}
					f.nbContent.value = str;
					
					f.action = "<%=cp%>/noticeInsert_ok.action";
					f.submit();
			 	} 
				
				</script>
			
			

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li><a href="/ticket/useInfo/faq.do">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li class="selected"><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				




			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2 class="f_ligth">문화N티켓 공지사항</h2>
						
					</div>


	<div class="board_view_tp01 mgt30">
		<div class="cont">
		
		<div class="reviewForm">
		
		<form id="insertBoardFrm" name="myForm" method="post" onsubmit="return false;"  >
							
		<table >
		<colgroup>
			<col width="15%">
			<col width="85%">
		</colgroup>
		<tbody>
			<tr>
			<th scope="row">제목</th>
			<td>
				<input type="text" name="nbSubject" maxlength="40" value="" title="제목 입력">
			</td>
			</tr>
		
		
			<tr>
				<th scope="row" style="vertical-align:top">내용</th>
			<td>	
				<textarea  name="nbContent" rows="40" cols="100"></textarea>
			</td>
			</tr>
		</tbody>
		</table>
		
		<div class="board_btn_wrap">
			<div class="lt_r">
			 	<a onclick="sendIt();" class="btn btn_middle btn_dark" id="inserBoard">등록하기</a>
				<a onclick="document.myForm.subject.focus();" class="btn btn_middle btn_dark">다시입력</a>
				<a href="notice.action" class="btn btn_middle btn_dark">돌아가기</a>
			</div>
		</div>
		</form>
		</div>
		</div>
	</div>
<!-- 
					<div class="board_btn_wrap">
						<div class="lt_r">
						 <input type="button" id="insertBoard" value="등록" />						
							<a onclick="sendIt();" class="btn btn_middle btn_dark" id="inserBoard">등록하기</a>
							<a onclick="document.myForm.subject.focus();" class="btn btn_middle btn_dark">다시입력</a>
							<a href="notice.action" class="btn btn_middle btn_dark">돌아가기</a>
						</div>
					</div>
					
 -->			
				</div>
			</div>
			
			
			
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>