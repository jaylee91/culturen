<%@ page contentType="text/html; charset=UTF-8"%>


<jsp:include page="../layout/inc_top.jsp"/>

<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="guide.action">예매 가이드</a></li>
						<li class="selected"><a href="faq.action">FAQ</a></li>
						<li><a href="ticketInfo.action">티켓판매 안내</a></li>
						<li><a href="notice.action">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				






			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>FAQ</h2>
						<p>가장 많이 물어보신 질문입니다.</p>
					</div>

					<div class="board_search_wrap sort_wrap mgb30">
						<div class="fl">
							<a href="faq.action" class="btn btn_red w100"><span class="txt">예매/결제</span></a>
							<a href="faq_B.action" class="btn  w100"><span class="txt">취소/환불</span></a>
							<a href="faq_C.action" class="btn  w100"><span class="txt">회원</span></a>
							<a href="faq_D.action" class="btn  w100"><span class="txt">기타</span></a>
							<!-- <a href="faq.action?faqtype=A" class="btn btn_red w100"><span class="txt">예매/결제</span></a>
							<a href="faq.action?faqtype=B" class="btn  w100"><span class="txt">취소/환불</span></a>
							<a href="faq.action?faqtype=C" class="btn  w100"><span class="txt">회원</span></a>
							<a href="faq.action?faqtype=D" class="btn  w100"><span class="txt">기타</span></a> -->
						</div>
						<form method="get">
						<div class="fr">
							<label for="FIELD" class="hidden">검색구분</label>
							<select name="category" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
								<option value="0" selected="">전체</option>
								<option value="1">제목</option>
								<option value="2">내용</option>
							</select>
							<label for="KEY" class="hidden">검색어</label>
							<input type="text" name="keyword" placeholder="검색어를 입력하세요." title="검색어 입력" class="comm_input scr_elem" value="">
							<button class="btn btn_search scr_elem">검색</button>
							<input type="hidden" name="faqtype" value="">
						</div>
						</form>
					</div>
					
					<table class="board_list writen_list faq_list">
						<caption>분류, 제목 구성된 FAQ 목록이며 질문 클릭시 상세내용이 펼쳐지며 보여집니다.</caption>
						<colgroup>
							<col style="width:10%">
							<col style="width:*">
						</colgroup>
						 <thead>
							  <tr>
								<th scope="col">분류</th>
								<th scope="col">제목</th>
							  </tr>
						 </thead>
						 <tbody>
						 	  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>예매 수수료는 얼마인가요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p>문화N 티켓에서는 차별 없는 국민의 문화생활을 위하여 예매 시 발생하는 <span style="color: rgb(255, 0, 0);">수수료가 없습니다.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>티켓 수령은 어떻게 하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">티켓은
현장 수령만 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">당일
공연 시작 시각 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">1</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">시간</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">~30</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;; color: red; font-weight: bold;">분
전까지 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">행사장 매표소에서 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">받으셔야</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매번호</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">,&nbsp;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">신분증 등을 지참하시면 편리하게 티켓을 받으실 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"><br></span></p><p>



</p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><br></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>티켓 예매 내역은 어디서 확인하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">문화</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">N</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">티켓 메인 화면의 상단 예매</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">/</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">취소 내역 또는 마이페이지</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">&gt;</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">/</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">취소 내역 메뉴에서 확인이 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>무통장 입금 시 예매자명과 입금자명이 다르면 예매가 안될까요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매
시 받으신 가상계좌는 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">소멸성</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 가상계좌로 예매자에게만 부여되는 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">고유번호입니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">입금계좌번호와
입금금액이 정확하게 일치하면 예매자명과 입금자명이 달라도 입금 확인이 가능합니다</span><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:en-US;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">그러므로 입금 계좌번호와 입금금액을 꼭 확인하여 입금
부탁드립니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>예매 시 결제 수단에는 어떤 것이 있나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">문화</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">N</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">티켓에서는 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">신용카드</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">,
</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">실시간
계좌이체</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">,
</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">가상계좌
휴대폰</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">의 결제수단을 지원합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>비회원으로 예매 할 수 있나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">네</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">. </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">문화</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">N</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">티켓에서는 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">비회원도
예매가 가능합니다</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">.</span></p><p>

</p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매하기 버튼을 클릭하시면 회원 또는 비회원 예매를 확인하므로 비회원예매를
선택하시면 예매자명</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">휴대폰번호</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">임시비밀번호 입력만으로 예매가 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>예매는 언제까지 할 수 있나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">마감시간은 취소 마감시간과 동일합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">예매 또는 취소는 평일</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">,
</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">주말</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">,
</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">공휴일
오후 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">17:00</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">까지</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 가능하며</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매는 당일 예매가 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">불가하오니</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 유의하여 주시기 바랍니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span><br></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>가상계좌로 결제했습니다. 언제까지 입금해야 하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<div style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0.25in; text-indent: -0.25in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">예매일
다음날 </span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:
&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">23:59</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">까지
</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">입금하셔야</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">. </span></div><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">가상계좌번호는 입금 마감시간 이후에는 부여된 가상계좌가 자동
소멸하므로 입금 시간을 연장할 수 없습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">입금 마감 시간까지 </span><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">입금 되지 않으면 자동으로 예매가 취소</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">되므로 다시 예매해 주셔야 합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">다시 예매할 경우 실시간 예매 서비스이므로 이전에 선택하신 좌석 그대로 예매가 되지
않을 수도 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>공연을 예매 했는데 갈 수 없어서 친구가 대신 가려고 합니다. 예매자와 관람자가 달라도 공연을 볼 수 있나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">예매자와 관람자가 달라도 관람이 가능합니다</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">.
</span></p><p>

</p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">티켓 수령 시 예매자명과 휴대폰 번호</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매번호를 확인하므로 미리 준비하시면 좀 더 편하게 입장하실 수
있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>비회원으로 예매 했는데 비밀번호를 잊어버렸습니다. 비밀번호를 알 수 있는 방법이 있나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">예매가
완료되면 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">SMS</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">로 예매 내용을 전송하며</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">비밀번호도 포함되어 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p>

</p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">비밀번호를 분실하신 경우는 </span><span style="font-size:
10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:
&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;
mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;color:red;
mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">고객센터</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">(02-3153-2898)</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">로
연락</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">하시면 비밀번호를 확인하실 수 있습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>단체 관람을 하려고 합니다. 온라인에서는 10명 까지만 예매가 가능하던데 그 이상 예매하려면 어떻게 해야하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">온라인
예매는 최대 </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">10</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">명 까지만 예매가 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p>

</p><p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">11</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">명
이상 예매를 원하시면 고객센터</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;
mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:
+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:minor-fareast;
mso-bidi-theme-font:minor-bidi;color:red;mso-font-kerning:12.0pt;language:en-US;
font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
red;mso-style-textfill-fill-alpha:100.0%">(02-3153-2898)</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:red;mso-font-kerning:12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:
solid;mso-style-textfill-fill-color:red;mso-style-textfill-fill-alpha:100.0%">로</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;"> 연락주세요</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">. </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">상담원이 예매 진행을 도와드리겠습니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p>
									</div>
								</td>
							  </tr>
							  
							  <tr>
								<td>예매/결제</td>
								<td class="al subject"><a href="#" class="view_writen"><span class="ico">질문</span>현금영수증을 받고 싶습니다. 어떻게 해야하나요?</a></td>
							  </tr>
							  <tr>
								<td colspan="4" class="txt_view">
									<div class="answer f_black">
										<span class="ico">답변</span>
										<p style="line-height: 150%; margin-top: 0pt; margin-bottom: 0pt; margin-left: 0in; direction: ltr; unicode-bidi: embed; word-break: keep-all;"><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">현금영수증
발행은 가상계좌</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">실시간계좌이체 결제시에 발급이 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span></p><p><span style="font-size:10.0pt;
font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;
mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:
minor-fareast;mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-font-kerning:
12.0pt;language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
#0070C0;mso-style-textfill-fill-alpha:100.0%">마이페이지</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:#0070C0;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;
mso-style-textfill-type:solid;mso-style-textfill-fill-color:#0070C0;mso-style-textfill-fill-alpha:
100.0%">&gt;</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;
mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:
+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:minor-fareast;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
#0070C0;mso-style-textfill-fill-alpha:100.0%">증빙서류</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;mso-ascii-font-family:&quot;맑은 고딕&quot;;
mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:+mn-cs;mso-ascii-theme-font:
minor-latin;mso-fareast-theme-font:minor-fareast;mso-bidi-theme-font:minor-bidi;
color:#0070C0;mso-font-kerning:12.0pt;language:en-US;font-weight:bold;
mso-style-textfill-type:solid;mso-style-textfill-fill-color:#0070C0;mso-style-textfill-fill-alpha:
100.0%">&gt;</span><span style="font-size:10.0pt;font-family:&quot;맑은 고딕&quot;;
mso-ascii-font-family:&quot;맑은 고딕&quot;;mso-fareast-font-family:&quot;맑은 고딕&quot;;mso-bidi-font-family:
+mn-cs;mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:minor-fareast;
mso-bidi-theme-font:minor-bidi;color:#0070C0;mso-font-kerning:12.0pt;
language:ko;font-weight:bold;mso-style-textfill-type:solid;mso-style-textfill-fill-color:
#0070C0;mso-style-textfill-fill-alpha:100.0%">현금영수증 메뉴</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">에서 발급이 가능하며</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">, </span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">발급한 후 다른 번호로 재발행도 가능합니다</span><span style="font-size: 10pt; font-family: &quot;맑은 고딕&quot;;">.</span><br></p>
									</div>
								</td>
							  </tr>
							  
						 </tbody>
					</table>
					
					<div class="pageing">
						
							<a href="?pageNo=1" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1" class="last"><span>마지막 페이지 이동</span></a>
						
					</div>
				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>