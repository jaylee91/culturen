<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link href="/ticket/assets/css/import.css" rel="stylesheet">
<link href="/ticket/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="/ticket/assets/js/jquery.min.js"></script>
<script src="/ticket/assets/js/jquery-ui.js"></script>
<script src="/ticket/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/ticket/assets/js/popup.js"></script>
<script src="/ticket/assets/js/common.js"></script>
		
<!--[if lt IE 9]>
	<script src="/ticket/assets/js/html5shiv.js"></script>
	<script src="/ticket/assets/js/respond.min.js"></script>
<![endif]-->
	</head>
	<body>
		<div id="skipNavi">
			<ul>
				<li><a href="#contents">본문 바로가기</a></li>
				<li><a href="#gnb">주메뉴 바로가기</a></li>
			</ul>
		</div>

		<div id="wrap">
			<!--S: Top -->
			


<div class="header_wrap">
	<header>
		<h1><a href="/ticket/"><img src="/ticket/assets/img/logo.png" alt="문화N티켓" /></a></h1>
		<div class="t_group">
			<ul>
			
				<li><a href="#login_pop" class="lay_pop" data-id="login_pop2" id="login-pop-link2">로그인</a></li>
				<li><a href="https://www.culture.go.kr/member/join.do" target="_blank" title="새창">회원가입</a></li>
			
				<li><a href="/ticket/mypage/cancle_list.do">예매/취소내역</a></li>
				<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
				<li><a href="/ticket/about/introduction.do">문화N티켓 소개</a></li>
				<li><a href="http://www.culture.go.kr/ticketadmin" target="_blank">참여단체 로그인</a></li>
			</ul>
		</div>
		<div id="gnb">
			<ul>
				<li><a href="/ticket/concert/concert_list.do">공연</a></li>
				<li><a href="/ticket/exhibit/exhibit_list.do">전시</a></li>
				<li><a href="/ticket/culture/storyList.do">문화이야기</a>
					<ul>
						<li><a href="/ticket/culture/storyList.do">공연제작 이야기</a></li>
						<li><a href="/ticket/culture/actorList.do">문화인물 이야기</a></li>
						<li><a href="/ticket/culture/societyList.do">문화단체 이야기</a></li>
						<li><a href="/ticket/culture/tourList.do">문화관광 이야기</a></li>
					</ul>
				</li>
				<li><a href="/ticket/event/ingList.do">이벤트</a>
					<ul>
						<li><a href="/ticket/event/ingList.do">진행중 이벤트</a></li>
						<li><a href="/ticket/event/endList.do">지난 이벤트</a></li>
						<li><a href="/ticket/event/winList.do">당첨자 확인</a></li>
					</ul>
				</li>
				<li><a href="/ticket/useInfo/guide.do">이용안내</a>
					<ul>
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li><a href="/ticket/useInfo/faq.do">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="allmenu">
			<a href="#" class="all">전체메뉴<span>열기</span></a>
			<div class="all_list">
				<ul>
					<li class="dep01"><a href="/ticket/concert/concert_list.do">공연</a></li>
					<li class="dep01"><a href="/ticket/exhibit/exhibit_list.do">전시</a></li>
					<li class="dep01"><a href="/ticket/culture/storyList.do">문화이야기</a>
						<ul>
							<li><a href="/ticket/culture/storyList.do">공연제작 이야기</a></li>
							<li><a href="/ticket/culture/actorList.do">문화인물 이야기</a></li>
							<li><a href="/ticket/culture/societyList.do">문화단체 이야기</a></li>
							<li><a href="/ticket/culture/tourList.do">문화관광 이야기</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/event/ingList.do">이벤트</a>
						<ul>
							<li><a href="/ticket/event/ingList.do">진행중 이벤트</a></li>
							<li><a href="/ticket/event/endList.do">지난 이벤트</a></li>
							<li><a href="/ticket/event/winList.do">당첨자 확인</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/useInfo/guide.do">이용안내</a>
						<ul>
							<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
							<li><a href="/ticket/useInfo/faq.do">FAQ</a></li>
							<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
							<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
						</ul>
					</li>
					
					<li class="dep01"><a href="/ticket/about/introduction.do">문화N티켓 소개</a>
						<ul>
							
							<li><a href="#" class="f_red lay_pop" data-id="login_pop2">로그인</a></li>
							<li><a href="https://www.culture.go.kr/member/join.do" class="f_red" target="_blank" title="새창">회원가입</a></li>
							
							<li><a href="/ticket/polc/policy.do">개인정보처리방침</a></li>
							<li><a href="/ticket/polc/term.do">이용약관</a></li>
							<li><a href="/ticket/polc/copy.do">저작권정책</a></li>
							<li class="end"><a href="/ticket/polc/collect.do">검색결과수집정책</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<form action="/ticket/search/search.do">
		<div class="t_search_wrap">
			<label for="keyword" class="hidden">통합검색</label>
			<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력하세요." class="comm_input" title="검색어 입력" />
			<input type="image" class="btn_search" src="/ticket/assets/img/ico_search.png" alt="검색">
		</div>
		</form>
	</header>
	<div class="gnb_bg"></div>
	<a href="#" data-id="login_pop" class="lay_pop" id="login-pop-link" style="display:none">로그인</a>
	<a href="#" data-id="login_pop3" class="lay_pop" id="login-pop-link3" style="display:none">로그인</a>
</div>


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li><a href="/ticket/useInfo/faq.do">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				

			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>예매 가이드</h2>
						<p>모든 궁금증을 해결해드립니다.</p>
					</div>

					<div class="ev_tab">
						<ul>
							<li><a href="#" class="on" id="6">예매방법</a></li>
							<li><a href="#" id="0">결제방법</a></li>
							<li><a href="#" id="1">수수료</a></li>
							<li><a href="#" id="2">취소/환불</a></li>
							<li><a href="#" id="3">티켓수령</a></li>
							<li><a href="#" id="4">전화예매</a></li>
						</ul>
					</div>

					<!-- 예매방법 -->
					<div class="guide" id="guideInfo">
						<ul>
							<li><img src="/ticket/assets/img/guide_img01.jpg" alt="공연선택 : 원하시는 공연을 선택휴 예매하기 버튼을 클릭하세요."></li>
							<li><img src="/ticket/assets/img/guide_img02.jpg" alt="관람일/회차/인원 선택 : 원하시는 날짜와 시간을 선택 후 인원수를 선택하고 다음 단계 버튼을 클릭하세요."></li>
							<li><img src="/ticket/assets/img/guide_img03.jpg" alt="좌석선택 : 원하시는 좌석을 선택 후 다음단계 버튼을 클릭하세요."></li>
							<li><img src="/ticket/assets/img/guide_img04.jpg" alt="할인선택 : 선택 가능한 할인 종류와 매수를 선택합니다.처음 선택하신 인원수에 한하여 매수를 선택할 수 있습니다."></li>
							<li><img src="/ticket/assets/img/guide_img05.jpg" alt="예매자 확인 및 결제 : 결제하기를 클릭하시면 결제 팝업이 보여집니다.결제수단을 선택하시어 예매를 완료해 주세요."></li>
							<li><img src="/ticket/assets/img/guide_img06.jpg" alt="예매완료"></li>
						</ul>
					</div>

					<!-- 결제방법 -->
					<div class="guide_info" id="guideInfo">
						<p class="f_16 f_black mgt10">문화N티켓에서는 신용카드, 휴대폰, 실시간 계좌이체, 무통장입금의 결제수단을 지원하고 있습니다.</p>
						<h2>신용카드</h2>
						<p>문화N티켓에서는 BC, KB국민, 롯데, 삼성, 신한, 외환, 씨티 등 대부분의 신용카드 사용이 가능합니다.</p>
						<div class="txt_b">
							<h3>안전결제</h3>
							<p>
								안전결제 방식은 카드사에서 제공하는 화면에서 직접 인터넷안전결제 비밀번호 입력하여 상품 대금을 결제하는 방식입니다.<br />
								단, 거래금액이 30만원 이상일 때는 공인인증을 거쳐야 합니다.
							</p>

							<h3>안심클릭 결제</h3>
							<p>
								안심클릭서비스는 온라인 전자상거래용 지불인증 서비스로 온라인 쇼핑 시 고객님의 카드번호, 유효기간, 주민번호, 비밀번호 등의 주요정보를 입력하지 않고 고객님이 사전에 정한 안심클릭서비스 암호(패스워드)를 통해 거래하는 인터넷 결제서비스입니다.<br />
								단, 거래금액이 30만원 이상일 때는 공인인증을 거쳐야 합니다.
							</p>
						</div>

						<h2>가상계좌</h2>
						<div class="txt_b">
							<p>
								결제 시 가상계좌를 선택하시면 입금일시까지 부여받은 은행 계좌로 입금하시는 결제수단입니다.<br />
								(가상계좌는 예매 시 고객님께 은행계좌가 일시적으로 부여됐다가 입금 마감 시간이 지나면 은행계좌가 자동 소멸되는 시스템입니다.)<br />
								예매 건당 하나의 계좌번호가 생성되고, 당일 여러 건의 예매를 동일한 예매자가 했더라도 각각의 예매마다 계좌번호가 변경되어 달리 부과되오니 주의하여 입금하시기 바랍니다. <br />
								예매 금액과 입금 금액이 일치하지 않을 경우 입금 오류가 발생하여 입금 처리되지 않습니다. 또한 수표는 정상적으로 입금되지 않습니다.<br />
								<span class="f_red">입금 마감은 예매일 다음날 23:59까지</span> 이며, 미입금된 경우는 자동 취소됩니다.(<span class="f_red">은행에 따라 23:30에 마감되는 경우가 있으니 확인하시기 바랍니다.</span>)<br />
								입금계좌를 분실하셨으면 <a href="/ticket/mypage/cancle_list.do" class="f_blue un_l">예매/취소 내역</a>에서 확인하시기 바랍니다.
							</p>
						</div>

						<h2>실시간 계좌이체</h2>
						<div class="txt_b">
							<p>
								출금이체 방식에 의한 인터넷상의 계좌이체 대금결제 서비스이며, 결제 및 결제 후 잔액이 실시간으로 업데이트됩니다.<br />
								회원님의 공인인증서를 통해 결제가 이루어집니다.<br />
								당일 결제/취소/환불이 가능하며, 당월 취소시에는 출금하신 계좌로 환불됩니다. (<span class="f_red">익월 취소시에는 고객님의 환불 계좌로 입금됩니다.</span>)
							</p>
						</div>

						<h2>휴대폰</h2>
						<div class="txt_b">
							<p>
								SK텔레콤, KT, LG U+ 등 모든 통신사별 결제가 가능합니다.<br />
								통신사를 선택 후 휴대폰번호, 주민등록번호를 입력하면 인증번호 6자리가 문자로 전송되며, 전송받은 인증번호를 인증번호 란에 입력하면 결제되는 방식입니다.<br />
								예매 시 각 통신사별 멤버쉽카드 할인혜택은 적용되지 않습니다.<br />
								통신사 정산방식에 따라 <span class="f_red">익월 취소인 경우, 소액결제 승인 취소가 안되므로 고객님의 환불 계좌로 입금</span>됩니다.<br />
								결제하려는 금액이 최고결제금액 한도 이하이나 결제가 불가한 경우, 각 통신사 고객센터를 통해 본인의 한도금액을 확인하시기 바랍니다.
							</p>
						</div>
					</div>

					<!-- 수수료 -->
					<div class="guide_info" id="guideInfo">					
						<p class="f_16 f_black mgt10">문화N티켓에서는 상품에 따라 예매수수료가 다를 수 있습니다.</p>
						<h2>취소 마감 시간</h2>
						<p class="f_red f_bold">관람일 전일 (평일/주말/공휴일/토요일) 17:00시</p>

						<h2>취소 수수료 안내</h2>
						<table class="board_view_tp02 mgb20">
							<caption>취소 수수료에 대한 정보를 받으실 수 있습니다..</caption>
							<colgroup>
								<col style="width:20%">
								<col style="width:*">
							</colgroup>
							 <tbody>
								  <tr>
									<th scope="row">예매 후 ~ 관람일 10일 전</th>
									<td>없음</td>
								  </tr>
								  <tr>
									<th scope="row">관람일 9일 전~7일 전</th>
									<td>티켓 금액의 10%</td>
								  </tr>
								  <tr>
									<th scope="row">관람일 6일 전~3일 전</th>
									<td>티켓 금액의 20%</td>
								  </tr>
								  <tr>
									<th scope="row">관람일 2일 전~1일 전</th>
									<td>티켓 금액의 30%</td>
								  </tr>
							 </tbody>
						</table>
						<ul class="line_h24">
							<li>- 예매 당일 밤 12시 이전 취소 시 취소수수료가 없습니다.</li>
							<li>- 예매 후 9일 이내라도 취소시점이 관람일로부터 9일 이내라면 그에 해당하는 취소수수료가 부과됩니다.</li>
							<li>- 상품의 특성에 따라 취소수수료 정책이 달라질 수 있습니다.(각 상품 예매 시 취소수수료 확인) </li>
						</ul>
					</div>

					<!-- 취소/환불 -->
					<div class="guide_info" id="guideInfo">
						
						<p class="f_16 f_black mgt10">문화N티켓의 <span class="f_red">취소 마감시간은 관람일 전일 (평일/주말/공휴일/토요일) 17:00</span>시 입니다. </p>
						<table class="board_view_tp02 mgt20 mgb20">
							<caption>취소 수수료에 대한 정보를 받으실 수 있습니다..</caption>
							<colgroup>
								<col style="width:20%">
								<col style="width:*">
							</colgroup>
							 <tbody>
								  <tr>
									<th scope="row">예매 후 ~ 관람일 10일 전</th>
									<td>없음</td>
								  </tr>
								  <tr>
									<th scope="row">관람일 9일 전~7일 전</th>
									<td>티켓 금액의 10%</td>
								  </tr>
								  <tr>
									<th scope="row">관람일 6일 전~3일 전</th>
									<td>티켓 금액의 20%</td>
								  </tr>
								  <tr>
									<th scope="row">관람일 2일 전~1일 전</th>
									<td>티켓 금액의 30%</td>
								  </tr>
							 </tbody>
						</table>
						<ul class="line_h24">
							<li>상품의 특성에 따라 취소수수료 정책이 달라질 수 있습니다.(각 상품 예매 시 취소수수료 확인) </li>
							<li>부분취소는 신용카드, 실시간계좌이체로 결제하신 건에 한하여 부분취소가 가능합니다.</li>
						</ul>
						
						<h2>신용카드로 결제하신 경우</h2>
						<div class="txt_b">
							<p>
								일반적으로 당사의 취소 처리가 완료되고 3~5일 후 카드사의 취소가 확인됩니다. (체크카드 동일)<br />
								예매 취소 시점과 해당 카드사의 환불 처리기준에 따라 취소금액의 환급방법과 환급일은 다소 차이가 있을 수 있으며, 예매 취소 시점에 따라 취소 수수료를 제외한 금액을 취소합니다. 
							</p>
						</div>

						<h2>가상계좌로 결제하신 경우</h2>
						<div class="txt_b">
							<p>
								예매 취소 시에 환불 계좌번호를 남기고, 그 계좌를 통해 취소 수수료를 제외한 금액을 환불 받습니다.<br />
								단, 예매 후 1개월이 경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 500원을 제외한 금액이 입금됩니다. <br />
								취소 후 고객님의 계좌로 입금까지 대략 5~7일 정도가 소요됩니다. (주말 제외)
							</p>
						</div>

						<h2>실시간 계좌이체로 결제하신 경우</h2>
						<div class="txt_b">
							<p>
								예매 취소 시 입금하신 계좌정보로 취소 수수료를 제외한 금액을 환불 받습니다.<br />
								단, 예매 후 1개월이 경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 500원을 제외한 금액이 입금됩니다.<br />
								취소 후 고객님의 계좌로 입금까지 대략 5~7일 정도가 소요됩니다. (주말 제외)
							</p>
						</div>

						<h2>휴대폰으로 결제하신 경우</h2>
						<div class="txt_b">
							<p>
								취소 신청 후 바로 취소 처리가 되며 취소 수수료를 제외한 티켓 금액 이용료가 취소 가능합니다.
								단, 당월 결제에 대한 취소가 아닌 1개월이 경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 500원을 제외한 금액이 입금됩니다.<br />
							</p>
						</div>

					</div>

					<!-- 티켓수령 -->
					<div class="guide_info" id="guideInfo">
						
						<p class="f_16 f_black mgt10">문화N티켓은 <span class="f_bold">현장수령</span>만 가능합니다.</p>
						<p class="mgt30 line_h24">
							현장수령은 관람 당일 공연장 티켓박스에서 티켓을 수령하시면 됩니다.<br />
							티켓 수령시 예매번호, 예매자의 신분증을 지참하셔야 합니다.<br />
							현장수령시 할인 받은 증빙서류(복지카드, 국가유가족/유족증) 등을 미지참하셨을 경우 차액 지불 후에만 수령이 가능합니다.<br />
							<span class="f_red">1시간~30분 전</span>에 현장에 도착하셔서 티켓을 수령하시면 여유롭습니다.
						</p>

					</div>

					<!-- 전화예매 -->
					<div class="guide_info" id="guideInfo">
						
						<p class="f_16 f_black mgt10">문화N티켓은 <span class="f_bold">전화예매가 가능</span>합니다.</p>
						<p class="mgt30">인터넷 예매가 어려우시거나 <span class="f_red">11명 이상 예매를 원하시는 경우</span> 전화로 예매하실 수 있습니다.</p>
						
						<div class="custom_info mgt30">
							<ul>
								<li>
									<span>고객센터</span>
									02.3153.2898
								</li>
								<li>
									<span>운영시간</span>
									평일 오전 10시 ~ 오후 6시
								</li>
							</ul>
						</div>
						
						<p class="mgt30 line_h24">
							고객센터 상담원에게 공연명/관람일/시간/인원/좌석/결제수단을 말씀하시면 예매를 대신 해드립니다.<br />
							전화예매는 신용카드, 가상계좌 결제만 가능하며, 전화부분취소는 신용카드 결제에 한해서만 가능합니다.<br />
							예매가 완료되면 SMS로 예매내역이 전송되어 예매내역을 확인하실 수 있습니다.
						</p>

					</div>

				</div>
			</div>
			
		<script type="text/javascript">
			$(function(){
				$(".guide_info").each(function(){
					$(this).hide();
				});
				
				$('.ev_tab ul li a').click(function(e){
					e.preventDefault();
					removeOnClass();
					allHideDiv();
					$(this).addClass("on");
					showDiv($(this).attr("id"));
				});
				
				
				
			});
			
			function removeOnClass(){
				$('.ev_tab ul li a').each(function(){
					$(this).removeClass("on");
				});
			}
			
			function allHideDiv(){
				$('.guide').hide();
				$(".guide_info").each(function(){
					$(this).hide();
				});
			}
			
			function showDiv(e){
				
				if(e == 6){
					$('.guide').show();
				}else{
					$('.guide_info').eq(e).show();
				}
			}
		</script>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<div class="footer">
	<div class="footer_body">
		<div class="btm_t_wrap">
			<ul class="link">
				<li><a href="/ticket/polc/policy.do">개인정보처리방침</a></li>
				<li><a href="/ticket/polc/term.do">이용약관</a></li>
				<li><a href="/ticket/polc/copy.do">저작권정책</a></li>
				<li><a href="/ticket/polc/collect.do">검색결과수집정책</a></li>
			</ul>
			<ul class="sns">
					<!-- <li class="snsT"><a title="새창열림" href="https://www.facebook.com/culture.N.ticket/" target="_blank"><span>문화N티켓 페이스북 바로가기</span></a></li> -->
					<li class="snsF"><a title="새창열림" href="https://www.instagram.com/culture_n_ticket/" target="_blank"><span>문화N티켓 인스타그램 바로가기</span></a></li>
					<li class="snsB"><a title="새창열림" href="https://blog.naver.com/PostThumbnailList.nhn?blogId=kcis_&from=postList&categoryNo=71&parentCategoryNo=71" target="_blank"><span>문화N티켓 블로그 바로가기</span></a></li>
			</ul>
			<div class="family_site">
				<a href="#none" class="family_btn">관련사이트</a>
				<ul>
					<li><a href="http://www.culture.go.kr/" target="_blank" title="새창">문화포털</a></li>
					<li><a href="http://www.culture.go.kr/data/" target="_blank" title="새창">문화데이터광장</a></li>
					<li><a href="http://www.culture.go.kr/wday/" target="_blank" title="새창">매달 마지막 수요일은 문화가 있는 날</a></li>
				</ul>
			</div>
		</div>

		<div class="copy_wrap">
			<div class="logo_g">
				<a href="http://www.mcst.go.kr/" target="_blank" title="새창"><img src="/ticket/assets/img/btm_logo01.gif" alt="문화체육관광부" /></a>
				<a href="http://www.kcisa.kr/" target="_blank" title="새창"><img src="/ticket/assets/img/btm_logo02.gif" alt="한국문화정보원" /></a>
			</div>

			<address>
				우)03925 서울시 마포구 월드컵북로 400 <br />
				대표전화:02-3153-2898, 이메일:ticket@kcisa.kr, Copyright 한국문화정보원<br />
				사업자 등록번호 101-82-10054 대표자 : 이현웅
			</address>

			<div class="certify_wrap">

				<a href="http://www.culture.go.kr/" target="_blank" title="새창"><img src="/ticket/assets/img/btm_r_02.gif" alt="문화포털" /></a>
			</div>
		</div>

	</div>
</div>

<button class="btn_top"><span class="txt">TOP</span></button>