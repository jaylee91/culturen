<%@ page contentType="text/html; charset=UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	String cp = request.getContextPath();
%>
<jsp:include page="../layout/inc_top.jsp"/>



<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm05">
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li><a href="/ticket/useInfo/faq.do">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li class="selected"><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				




			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2 class="f_ligth">${dto.nbSubject }</h2>
						<p><span class="mgr5">등록일 : ${dto.nbRedgate }</span>   <span class="mgl5">작성자 : 문화N티켓</span></p>
					</div>

					<div class="board_view_tp01 mgt30">
						<div class="cont">
							${dto.nbContent }
						</div>
					</div>

					<div class="board_btn_wrap">
						<div class="lt_r">
							<a onclick="location.href='<%=cp %>/noticeUpdated.action?nbNum=${dto.nbNum }&pageNum=${pageNum }'" class="btn btn_middle btn_dark">수정</a>
							<a onclick="location.href='<%=cp %>/noticeDeleted.action?nbNum=${dto.nbNum }&pageNum=${pageNum }'" class="btn btn_middle btn_dark">삭제</a>
							<a href="notice.action" class="btn btn_middle btn_dark">목록</a>
						</div>
					</div>
					

				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>