<%@ page contentType="text/html; charset=UTF-8"%>
		
<jsp:include page="../layout/inc_top.jsp"/>



<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>	
	

<script src="/culturen/resources/js/login.js" type="text/javascript"></script>
	
	
<script type="text/javascript">
	$(function(){

		$("div input").each(function(){
			type =  $(this).attr('type');
			
			if(type == 'text' || type == 'password') {
				$(this).focusin(function() {
					$(this).parent().addClass('focus');
					//$(this).attr('placeholder' , '');
				}) 
							
				$(this).focusout(function() {
					$(this).parent().removeClass('focus');
					//$(this).attr('placeholder' , $(this).attr('title'));
				}) 
			}
		})

		$("#ubirth").keypress(function(event){			
			if (event.which && (event.which  > 47 && event.which  < 58 || event.which == 8)) {
		    } else {
		    	event.preventDefault();
		  	}
		});
		
		$('#confirmBtn').click(function(){
			$('form[name=frm]').submit();
		})
		
	});
	
	var count = 0 ;
	
	function doSubmit(frm){
		// 두번 호출 되네 ㅡ.ㅡㅋ 
		count++;
		if(count>1) {count = 0 ; return;}
		
		
		if ( $("input[name='name']").val() == "" ){
			alert("이름을 입력하세요.");
			$("input[name='name']").focus();
			return false;
			
		}
		
		if ( $("input[name='birth']").val() == "" ){
			alert("생일을 입력하세요.");
			$("input[name='birth']").focus();
			return false;
		}
		
		if ( $("input[name='birth']").val().replace(/^\s*|\s*$/g,"").length  != 8 ){
			alert("생년월일은 8자리 숫자를 입력세요.");
			$("input[name='birth']").focus();
			return false;
		}
		
		var url = frm.action;		
		var option = 'width=500,height=250,toolbar=no,menubar=no,status=no,scrollbars=yes,resizable=no';
		window.open('', 'findId', option);
		
		frm.target = "findId";
		frm.submit();
	}
</script>
</head>
<body>
<div id="wrap" class="login">
	<div id="headerLogin">
		<h1><a href="/index.do"><span>문화포털</span></a></h1>
	</div>
	<p class="title01">아이디 찾기</p>
	<div id="contentLogin">
		<div class="loginForm idSearch">
		<span class="before"></span>
		<form name="frm" action="/popup/findIdResult.do" method="post" onsubmit="doSubmit(this);return false;">
			<fieldset>
				<legend>로그인</legend>
				<!-- focus될경우 class="focus" -->
				<div class="inputS focus">
					<!-- class="placeholder" 추가 -->
					<input id="name" name="name" type="text" class="placeholder" title="이름 입력" placeholder="이름 입력">
					<p class="txt01">회원가입 시 등록하신 실명을 입력해 주시기 바랍니다.</p>
				</div>
				<!-- focus될경우 class="focus" -->
				<div class="inputS">
					<input id="birth" name="birth" type="text" class="placeholder" title="생년월일 입력" placeholder="생년월일 입력">
					<p class="txt01">생년월일이 1978년 3월 3일인 경우 19780303으로 입력해주시기 바랍니다.</p>
				</div>
				<!-- 로그인 버튼 : S -->
				<div class="btnLogin"><input id="confirmBtn" type="image" src="/culturen/resources/img/content/btnConfirm01.jpg" alt="확인"></div>
				<!-- 로그인 버튼 : E -->
			</fieldset>
		</form>
		<span class="after"></span>
		</div>
	</div>
</div>



<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>