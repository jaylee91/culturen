<%@ page contentType="text/html; charset=UTF-8"%>





<jsp:include page="../layout/inc_top.jsp"/>





<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>


<script src="/js/jquery.jqtransform.js" type="text/javascript"></script>
<script src="/js/portal/view/login/login.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/people.js"></script>





<script type="text/javascript">
	$().ready(function(){
		
		
		
		
		
		$("input[type='checkbox'],input[type='radio']").ionCheckRadio();
		$('.selectCom').jqTransform();

		$("input[type=text]").each(function(){
			$(this).focusin(function() {
			    $(this).parent().addClass('focus');
			    //$(this).attr('placeholder' , '');
			});

			$(this).focusout(function() {
			    $(this).parent().removeClass('focus');
				//$(this).attr('placeholder' , $(this).attr('title'));

			});
		});

		$('#icr-7').click(function() {
			checkboxAll();
		});
		
		//키보드로 선택 했을 때
		$("input:checkbox[id='sAgree05']").on("change", function(){
			checkboxAll();
		});
		
		$('#ipinCertificationArea').click(function () { 
		    $('#mobileCertificationArea').removeClass('on');
		    $('#ipinCertificationArea').addClass('on');
		})

		$('#mobileCertificationArea').click(function () { 
		    $('#ipinCertificationArea').removeClass('on');
		    $('#mobileCertificationArea').addClass('on');
		})
		 
		$("button[name='gpin']").click(function(){
			
			document.name = "main";
			var wWidth = 360;
			var wHight = 120;

			var wX = (window.screen.width - wWidth) / 2;
			var wY = (window.screen.height - wHight) / 2;
		    var gPinLoginWin = window.open("/member/join_ipin.do", "gPinLoginWin", "directories=no,toolbar=no,left="+wX+",top="+wY+",width="+wWidth+",height="+wHight);
		    //gPinLoginWin.opener = "/member/join_ipin.do";
		    gPinLoginWin.focus();
		});

	});
	
	function checkboxAll(){
		var checkedAll = $('#sAgree05').is(":checked");
		  
		 for(var index = 1 ; index < 6 ; index++)
		 	$('#sAgree0' + index).attr("checked" , checkedAll);
		  
		 
		 for(var index = 3 ; index < 7 ; index++) { 
		    if(!checkedAll)
		        $('#icr-' + index).removeClass("checked");
			else 
				$('#icr-' + index).addClass("checked");
		}
	}
	
	function checkForm() {
		if($('input[name=name]').val()  == '') { 
			alert('이름을 입력해 주세요');
			$('input[name=name]').focus(); 
		    return false;     
		}

		if ($('input[name=birthday]').val()  == '') { 
			alert('생년월일을 입력해 주세요');
			$('input[name=birthday]').focus(); 
			return false;
		}
		
		if ($('input[name=birthday]').val().length  != 8) { 
			alert('생년월일을 년월일 형태로 입력해 주세요.\r\n(예 : 19800101)');
			$('input[name=birthday]').focus(); 
			return false;
		}

		if ($("input[name='gender']:checked").size() == 0) {
		    alert('성별을 선택해 주세요');
		    return false;
		}

		if($('input[name=mbphnNo]').val()  == '') { 
			alert('휴대폰 번호를 입력해 주세요');
			$('input[name=mbphnNo]').focus(); 
			return false;
		} else if ($('input[name=mbphnNo]').val().length > 11){
		    alert('휴대폰 번호는 11자를 넘을수가 없습니다.');
		    return false;
		}  
		
		for (var index = 1 ; index < 5 ; index++ ) { 
	  		if(!$('#sAgree0' + index).is(":checked")){
	  			switch(index) {
	  				case 1:
	  					alert("개인정보 이용 및 제공에 동의해주세요");
               			break;
	  				case 2:
	  					alert("통신사 이용약관에 동의해주세요");
               			break;
		            case 3:
		              	alert("고유식별 정보처리에 동의해주세요.");
                     	break;
           			case 4:
             			alert("휴대폰 본인 확인 이용약관에 동의해주세요");
                     	break;
	  			}
	    		
	    		return false;
			}
		}
		return true;
	}
	
	function alertErrorMessageByForm( msg, el ) {
		alert(msg);
		if(typeof el != 'undefined'){
			el.focus();
		}
		return false;
	}
	
	preambleView = function(seq) { 
		document.name = "main";
		var wWidth = 430;
		var wHight = 590;

		var wX = (window.screen.width - wWidth) / 2;
		var wY = (window.screen.height - wHight) / 2;
		var url = "/member/cp/pop_cp_agree" + seq + ".do";
	    var popTems = window.open(url, "cpLoginWin", "directories=no,toolbar=no,left="+wX+",top="+wY+",width="+wWidth+",height="+wHight);
	    popTems.focus();
	}
	
	
	
	
</script>

<div id="wrap" class="member">
	<div id="headerLogin">
		<h1><a href="/index.do"><span>문화포털</span></a></h1>
	</div>
	<div id="contentLogin">
		<!-- 회원 가입 : S -->
		<div class="memberWrap">
			<!-- 회원유형 : E -->
			<!-- 가입절차 : S -->
			<!-- 가입절차 아닐경우 class="hide" 추가 -->
			<div class="stepWrap">
			<span class="before"></span>
				
				<ol class="stepTit">
					<li class="step01"><em>약관동의</em></li>
					<li class="step02 on"><em>실명인증</em></li>
					<li class="step03"><em>회원정보입력</em></li>
					<li class="step03_01"><em>그래픽인증</em></li>
					<li class="step04"><em>가입완료</em></li>
				</ol>
				 
				<!-- 약관동의 : E -->
				<!-- 실명인증 : S -->
				<!-- 실명인증 아닐경우 class="hide" 추가 -->
				<div class="step02Wrap">
					
					<p class="text01">
					문화포털은 개인정보에 대한 유출 및 도용을 방지하기 위해 실명 인증을 시행하고 있습니다.<br>정보통신망 이용 촉진 및 정보보호 등에 관한 법률 제23조 2(주민등록번호의 사용 제한)에 의거하여 웹사이트 내 <br>주민등록번호 수집ᆞ이용이 금지되어 주민번호 입력 기반의 인증서비스 사용이 제한됩니다.
					</p>
<form id="cpCertRequestForm" action="./join_cp_cert_number.do" method="post" onsubmit="return checkForm(this);">
					<ul class="tabSt01">
						<!-- 휴대폰인증시 class="on" 추가 -->
						<li id="mobileCertificationArea" class="on">
							<a href="#url">휴대폰인증</a>
							<!-- sms인증 일 경우 class="smsConfirm" -->
							<div id="mobileCertification" class="confirm">
								<fieldset>
									<legend>휴대폰인증</legend>
									<div class="confirm">
										<div class="inputBox">
											<!-- focus될경우 class="focus" 추가 -->
											<div class="inputB">
												<!-- placeholder  class="placeholder" -->
												<input type="text" id="name" name="name" class="placeholder" title="이름" placeholder="이름" maxlength="20">
											</div>
											<div class="inputB">
												<!-- placeholder  class="placeholder" -->
												<input type="text" id="birthday" name="birthday" class="placeholder" title="생년월일 (예 : 19700301)" placeholder="생년월일 (예 : 19700301)" maxlength="8">
											</div>
											<div class="radioBox">
												
												<div class="icr__hidden" id="icr-container__1"><input type="radio" id="man" name="gender" value="1"><label for="man">남자</label></div><span class="icr enabled" id="icr-1"><span class="icr__radio"></span><span class="icr__text">남자</span></span>
												
												<div class="icr__hidden" id="icr-container__2"><input type="radio" id="woman" name="gender" value="0"><label for="woman">여자</label></div><span class="icr enabled" id="icr-2"><span class="icr__radio"></span><span class="icr__text">여자</span></span>
											</div>
											<div class="inputB st1">
												<div class="selectCom jqtransformdone" style="width: 129px;">
													<div class="jq_sel" style="z-index: 20;"><div><span>SKT</span><a href="#" class="jqTransformSelectOpen">카테고리 선택</a></div><ul style="display: none; width: 125px; visibility: visible;"><li><a href="#" index="0" class="selected">SKT</a></li><li><a href="#" index="1">KT</a></li><li><a href="#" index="2">LGU+</a></li><li><a href="#" index="3">알뜰폰 SKT</a></li><li><a href="#" index="4">알뜰폰 KT</a></li><li><a href="#" index="5">알뜰폰 LGU+</a></li></ul><select id="telCmmCd" name="telCmmCd" title="통신사 선택" class="jq_sel_hide" style="">
														<option value="01" selected="selected">SKT</option>
														<option value="02">KT</option>
														<option value="03">LGU+</option>
														<option value="04">알뜰폰 SKT</option>
														<option value="05">알뜰폰 KT</option>
														<option value="06">알뜰폰 LGU+</option>
													</select></div>
												</div>
												<!-- placeholder  class="placeholder" -->
												<input type="text" id="mbphnNo" name="mbphnNo" class="placeholder" title="휴대폰번호(‘-’ 없이 입력)" placeholder="휴대폰번호(‘-’ 없이 입력)">
											</div>
										</div>
										<ul class="agreeBox">
											<li>
												
												<div class="icr__hidden" id="icr-container__3"><input type="checkbox" id="sAgree01" name="sAgree01"><label for="sAgree01">개인정보 이용 및 제공에 동의합니다.</label></div><span class="icr enabled" id="icr-3"><span class="icr__checkbox"></span><span class="icr__text">개인정보 이용 및 제공에 동의합니다.</span></span>
												<button onclick="javascript:preambleView('01')" type="button" title="새창열림">전문보기</button>
											</li>
											<li>
												
												<div class="icr__hidden" id="icr-container__4"><input type="checkbox" id="sAgree02" name="sAgree02"><label for="sAgree02">통신사 이용약관에 동의합니다. </label></div><span class="icr enabled" id="icr-4"><span class="icr__checkbox"></span><span class="icr__text">통신사 이용약관에 동의합니다.</span></span>
												<button onclick="javascript:preambleView('03')" type="button" title="새창열림">전문보기</button>
											</li>
											<li>
												
												<div class="icr__hidden" id="icr-container__5"><input type="checkbox" id="sAgree03" name="sAgree03"><label for="sAgree03">고유식별 정보 처리에 동의합니다.</label></div><span class="icr enabled" id="icr-5"><span class="icr__checkbox"></span><span class="icr__text">고유식별 정보 처리에 동의합니다.</span></span>
												<button onclick="javascript:preambleView('02')" type="button" title="새창열림">전문보기</button>
											</li>
											<li>
												
												<div class="icr__hidden" id="icr-container__6"><input type="checkbox" id="sAgree04" name="sAgree04"><label for="sAgree04">휴대폰 본인 확인 이용 약관에 동의합니다.</label></div><span class="icr enabled" id="icr-6"><span class="icr__checkbox"></span><span class="icr__text">휴대폰 본인 확인 이용 약관에 동의합니다.</span></span>
												<button onclick="javascript:preambleView('04')" type="button" title="새창열림">전문보기</button>
											</li>
											<li>
												
												<div class="icr__hidden" id="icr-container__7"><input type="checkbox" id="sAgree05" name="sAgree05"><label for="sAgree05">전체항목에 동의합니다.</label></div><span class="icr enabled" id="icr-7"><span class="icr__checkbox"></span><span class="icr__text">전체항목에 동의합니다.</span></span>
											</li>
										</ul>
									</div>
									<!-- SMS 인증 : S -->
									<div class="smsC">
										<!-- focus될경우 class="focus" 추가 -->
										<div class="inputB focus">
											<!-- placeholder  class="placeholder" -->
											<input type="text" class="placeholder" title="SMS 인증번호 입력" placeholder="SMS 인증번호">
										</div>
										<button type="button" class="btnGray">재전송</button>
									</div>
									<!-- SMS 인증 : E -->
								</fieldset>
								<div class="btnBox">
									<button type="submit"><img src="/culturen/resources/img/content/btnConfirm.jpg" alt="확인"></button>
								</div>
							</div>
						</li>
						
						<!-- 아이핀 인증 시 class="on" 추가 -->
						<li id="ipinCertificationArea">
							<a href="#url">아이핀인증</a>
							<div>
								<p class="txt">공공 아이핀(I-PIN, Internet Personal Identification Number)은 <br>행정자치부에서 제공하는 주민등록번호 대체수단으로 <br>인터넷상 개인 식별번호를 의미합니다.</p>
								<div class="btnBox">
									<button type="button" name="gpin" title="새창 열림"><img src="/images/portal/content/member/btnIpin.jpg" alt="아이핀 인증"></button>
								</div>
							</div>
						</li>
					</ul>
</form>
					
					<form action="/member/join_certification.do" name="join" id="join" method="post">
						<input type="hidden" name="year">
						<input type="hidden" name="month">
						<input type="hidden" name="day">
						<input type="hidden" name="sex">
						<input type="hidden" name="name2">
						<input type="hidden" name="member_key">
						<input type="hidden" name="gubun">
					</form>

				</div>
				<span class="after"></span>
				<!-- 가입완료 : E -->
			</div>
			<!-- stepWrap : E -->
		</div>
		<!-- 회원 가입 : E -->
	</div>








<br/><br/><br/><br/>

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>