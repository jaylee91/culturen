<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../layout/inc_top.jsp"/>


<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>

<script src="/js/portal/view/login/login.js" type="text/javascript"></script>


<title>일반회원 회원가입 - 회원정보수정 | 문화포털</title>
</head>
<body>

<script type="text/javascript">
//<![CDATA[
	$().ready(function(){
		$("input[type='checkbox'],input[type='radio']").ionCheckRadio();
		
		$("select[name=userEmail3]").change(function(){
		  	$('#email2').val($("select[name=userEmail3]").val());
		});
		
		/* 
		$('#email3').change(function(){
			   $("#email3 option:selected").each(function () {
					
					if($(this).val()== '1'){ //직접입력일 경우
						 $("#email2").val('');                //값 초기화
						 $("#email2").attr("disabled",false); //활성화
					}else{ 					//직접입력이 아닐경우
						 $("#email2").val($(this).text());    //선택값 입력
						 $("#email2").attr("disabled",true);  //비활성화
					}
			   });
		});
		 */
	});
	
	function chckNumKey(obj) {
		val = obj.value;
		re = /[^0-9]/;
		obj.value = val.replace(re, "");
	}
	
	function openPopupId(obj, oWidth, oHeight){
		
		var userId = $("#userId").val();			
 		
		if ( !idCheck() ){
			return;
		}

		var url = userId == "" ? obj : obj +"?userId="+userId;
		
		var pWidth = oWidth;
		var pHeight = oHeight;
		
		var option = 'width=' + pWidth + 'px,height=' + pHeight + 'px,toolbar=no,menubar=no,status=no,scrollbars=yes,resizable=no';
		window.open(url, '', option);
		
	}
	
	function openPopupZipCode(obj, oWidth, oHeight){
		
		var addr_category = $("input[name='addr_category']:checked").val();
		
		var url = addr_category == "" ? obj +"?addr_category=63" : obj +"?addr_category="+addr_category;
		var pWidth = oWidth;
		var pHeight = oHeight;
		var option = 'width=' + pWidth + 'px,height=' + pHeight + 'px,toolbar=no,menubar=no,status=no,scrollbars=yes,resizable=no';
		window.open(url, '', option);
		
		return false;
		
	}
	
	function idCheck()
	{
	    var userid = $("input[name='userId']").val();
	    var seq = 0 , seq1=0;
	    var alpha = 'abcdefghijklmnopqrstuvwxyz';
	    var digit = '1234567890 ';
	    var alphaCheck = false;
	    var numberCheck = false;

	     if ( userid.length < 6 || userid.length > 12 ) {
	         alert("아이디는 6 ~ 12 자리까지 입니다!");
	         $("input[name='userId']").val("");
	         $("input[name='userId']").focus();
	         return false;
	     }else{
	    	 
	    	if (userid.indexOf(' ') > -1) {
		  	        alert("공백을 입력할 수 없습니다.");
		  	        $("input[name='userId']").val("");
			        $("input[name='userId']").focus();
		  	        return false;
			}
	    	 
	     	var count=0
	    	  	for (i=0;i<userid.length;i++){
	    	    	ls_one_char = userid.charAt(i);
	    	 
	    	    	if(ls_one_char.search(/[a-z|A-Z|0-9]/) == -1) {
	    	   			count++
	    	   		}
	    	  	}
	    	  	if(count!=0) {
	    	  		alert("특수문자는 사용하실 수 없습니다.");
	    	    	$("input[name='userId']").val("");
	         		$("input[name='userId']").focus();
	 	   	    return false;
	    	  	}
	    	  	
	 	   	 for(var i=0; i<userid.length; i++){
	 	 		   if(alpha.indexOf(userid.charAt(i)) != -1){
	 	 		    	alphaCheck = true;
	 	 		   }
	 	 		   if(digit.indexOf(userid.charAt(i)) != -1){
	 	 		    	numberCheck = true;
	 	 		   }
	 	  	}
	   		
	 	  	/*
	 		if(alphaCheck != true || numberCheck != true){
	 		    alert("영문,숫자 1자 이상으로 조합해주세요.");
	 		    $("input[name='user_id']").val("");
		        $("input[name='user_id']").focus();
	 		    return false;
	 		}//if
	  		 */
	  		
	  		
	  		splitedDigit1 = userid.split("") ; 
	     	var initStr = splitedDigit1[0] ;
	     	
	     	for(i=0; i< splitedDigit1.length-2; i++){ 
	     	  if( (splitedDigit1[i-1] == splitedDigit1[i] && splitedDigit1[i] == splitedDigit1[i+1] && splitedDigit1[i+1] == splitedDigit1[i+2] )){ 
	     		  alert ("4자이상 동일한 숫자나 문자를 입력하시면 안됩니다.");
	     		  $("input[name='userId']").val("");
		          $("input[name='userId']").focus();
	     		  return false;
	     	  }  
	         }// end for
	         
	     }

	     return true;
	}
	
	function pwdCheck()
	{

	     var pswd = $("input[name='pwd']").val();
	     var seq = 0 , seq1=0;

	     var alpha = 'abcdefghijklmnopqrstuvwxyz';
	     var digit = '1234567890 ';
	     var sChar = '!@$%^&*';
	     var sChar_Count = 0;
	     var alphaCheck = false;
	     var numberCheck = false;

	     if ( pswd.length < 10 || pswd.length > 16 ) {
	         alert("비밀번호는 10 ~ 16 자리까지 입니다!");
	         $("input[name='pwd']").val("");
	         $("input[name='pwd2']").val("");
	         $("input[name='pwd']").focus();
	         return false;
	     }else{
	     	for(var i=0; i<pswd.length; i++){
	    		   if(sChar.indexOf(pswd.charAt(i)) != -1){
	    		    	sChar_Count++;
	    		   }
	    		   if(alpha.indexOf(pswd.charAt(i)) != -1){
	    		    	alphaCheck = true;
	    		   }
	    		   if(digit.indexOf(pswd.charAt(i)) != -1){
	    		    	numberCheck = true;
	    		   }
	     	}//for
	     	/*
	  		if(sChar_Count < 1 || alphaCheck != true || numberCheck != true){
	  		    alert("영문,숫자 1자 이상,특수문자 1자 이상으로 조합해주세요.");
	  		  	document.getElementById("pswd").value="";
	          	document.getElementById("pswdCnfm").value="";
	 	        document.getElementById("pswd").focus();
	  		    return false;
	  		}//if
	  		*/
	  		
	  		if (pswd.indexOf(' ') > -1) {
	  	        alert("공백을 입력할 수 없습니다.");
	  	      	$("input[name='pwd']").val("");
		        $("input[name='pwd2']").val("");
		        $("input[name='pwd']").focus();
	  	        return false;
	  	    }
	  		
	  		if (pswd == $("input[name='user_id']").val() ) {

	  	        alert ("아이디와 비밀번호를 같게 입력하시면 안됩니다.");
	  	        $("input[name='pwd']").val("");
		        $("input[name='pwd2']").val("");
		        $("input[name='pwd']").focus();
	  	        return false;
	  	    }
	  		
	  		splitedDigit1 = pswd.split("") ; 
	     	var initStr = splitedDigit1[0] ;
	     	
	     	for(i=0; i< splitedDigit1.length-2; i++){ 
	     	  if( (splitedDigit1[i-1] == splitedDigit1[i] && splitedDigit1[i] == splitedDigit1[i+1] && splitedDigit1[i+1] == splitedDigit1[i+2] )){ 
	     		  alert ("4자이상 동일한 숫자나 문자를 입력하시면 안됩니다.");
	     		  $("input[name='pwd']").val("");
			      $("input[name='pwd_result']").val("");
			      $("input[name='pwd']").focus();
	     		  return false;
	     	  }  
	         }// end for
	         
	     	for(i=0; i< splitedDigit1.length-2; i++){ 
	       	  if( ( (splitedDigit1[i+2] - splitedDigit1[i+1] == 1) && (splitedDigit1[i+1] - splitedDigit1[i] == 1) && (splitedDigit1[i] - splitedDigit1[i-1] == 1) )){ 
	       		  alert ("4자이상 연속된 숫자를 입력하시면 안됩니다.");
	       		  $("input[name='pwd']").val("");
		          $("input[name='pwd2']").val("");
		          $("input[name='pwd']").focus();
	       		  return false;
	       	  }  
	         }// end for
	     }

	     return true;
	}
	
	function doSubmit(frm){

		if ( $("input[name='pwd']").val() == "" ){
			
			alert("비밀번호를 입력하세요.");
			$("input[name='pwd']").focus();
			return;
			
		}
		
		if ( $("input[name='pwd_result']").val() == "" ){
			
			alert("비밀번호 확인 답변을 입력하세요.");
			$("input[name='pwd_result']").focus();
			return;
			
		}
		
		if ( $("input[name='pwd']").val() != $("input[name='pwd2']").val() ){
			
			alert("비밀번호가 일치하지 않습니다.");
			$("input[name='pwd2']").focus();
			return;
			
		}
		
		if ( !pwdCheck() ){
			return;
		}
		
		if ( $("input[name='pwd_result']").val() == "" ){
			
			alert("비밀번호 확인 답변을 입력하세요.");
			$("input[name='pwd_result']").focus();
			return;
			
		}
		
		if ( $("input[name='hp2']").val() == "" || $("input[name='hp3']").val() == "" ){
			
			alert("휴대전화 번호를 입력하세요.");
			$("input[name='hp2']").focus();
			return;
			
		}
		
		if ( $("input[name='email1']").val() == "" || $("input[name='email2']").val() == ""){
			
			alert("이메일을 입력하세요.");
			$("input[name='pwd_result']").focus();
			return;
			
		}
		
		if ( $("select[name='addr_sido'] option:selected").val() == "" ){
			
			alert("거주지역을 선택해주세요.");
			$("select[name='addr_sido']").focus();
			return;
			
		}
		
		if( $("input[name='newsletter_yn']:radio:checked").length == 0 ) {
			alert("뉴스레터 수신 여부를 선택해주세요.");
			$("input[name='newsletter_yn']").focus();
			return;
	    }
		
		if( $("input[name='sec_cer_flag']:radio:checked").length == 0 ) {
			alert("2단계 인증 사용 여부를 선택해주세요.");
			$("input[name='sec_cer_flag']").focus();
			return;
	    }
		
		frm.submit();
	}
//]]>
</script>

<div id="wrap" class="member">
	<div id="headerLogin">
		<h1><a href="/index.do"><span>문화포털</span></a></h1>
	</div>
	<div id="contentLogin">
		<!-- 회원 가입 : S -->
		<div class="memberWrap">
			<!-- 회원유형 : E -->
			<!-- 가입절차 : S -->
			<!-- 가입절차 아닐경우 class="hide" 추가 -->
			<div class="stepWrap">

				<form id="result_join" name="join" action="/culturen/join_update_ok.action" method="post" onsubmit="doSubmit(this);return false;">
				<!-- 실명인증 : E -->
				<!-- 회원정보입력 : S -->
				<!-- 회원정보입력이 아닐경우 class="hide" 추가 -->
				<div class="step03Wrap">
					<h3 class="titBul01">회원정보수정</h3>
					<table class="boardInput02 hasTop" summary="아이디, 이름, 성별, 출생년도, 비밀번호, 비밀번호 확인, 비밀번호 확인 질문, 비밀번호 확인 답변, 전화번호, 휴대전화 번호, 이메일, 뉴스레터 수신여부, 주소가 포함된 기본 정보 입력">
						<caption>기본 정보 입력</caption>
						<colgroup>
							<col style="width:26%">
							<col style="width:74%">
						</colgroup>
			
						<tbody>

						<tr>
							<th scope="row"><span>아이디</span></th>
							<td>
								<input id="userId" name="userId" class="" title="아이디입력" readonly="readonly" value="${dto.userId }" maxlength="12">								
								<input type="hidden" name="hid_user_id" id="hid_user_id">
								<input type="hidden" name="idChk" id="idChk">
								<input type="hidden" name="member_category" value="1">
								<input id="join_category" name="join_category" type="hidden" value="">
							</td>
						</tr>
						<tr>
							<th scope="row"><span>이름</span></th>
							<td>
								<input type="text" id="name" name="userName" class="inputText uid" title="이름"  value="${dto.userName }">
							</td>
						</tr>
						<tr>
							<th scope="row"><span>성별</span></th>
							<td>
								
								<input type="text" id="sex" name="userGender" readonly="readonly" value="${dto.userGender }">
								
							</td>
						</tr>
						<tr>
							<th scope="row"><span>출생년도</span></th>
							<td>
								<input id="birth_year" name="userBirth" class="inputText uid" type="text" value="${dto.userBirth }">
								
							</td>
						</tr>
						<tr>
							<th scope="row"><span>비밀번호</span></th>
							<td>
								<div class="inputBox">
									<!-- class="placeholder" 추가 -->
									<input type="password" class="inputText placeholder" name="userPwd" title="비밀번호" value="${dto.userPwd }" maxlength="16" style="width:410px" placeholder="띄어쓰기 없이 영문 소문자와 숫자를 조합하여 10~16자로 설정해주시기 바랍니다.">
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><span>비밀번호 확인</span></th>
							<td>
								<div class="inputBox">
									<!-- class="placeholder" 추가 -->
									<input type="password" class="inputText placeholder" name="userPwd2" title="비밀번호" maxlength="16" style="width:410px" placeholder="비밀번호를 한번 더 입력해주시기 바랍니다.">
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><span>비밀번호 확인 질문</span></th>
							<td>
								<div class="selectCom">
									<select id="question" name="userPwdQ" title="비밀번호 확인 질문 선택">
									<option value="${dto.userPwdQ }">${dto.userPwdQ }</option>
									<option value="내가 졸업한 초등학교는?">내가 졸업한 초등학교는?</option>
									<option value="내가 태어난 도시는?">내가 태어난 도시는?</option>
									<option value="내가 여행하고 싶은 곳은?">내가 여행하고 싶은 곳은?</option>
									<option value="가낭 인상 깊게 읽은 책이름은?">가낭 인상 깊게 읽은 책이름은?</option>
									<option value="타인이 모르는 자신만의 신체 비밀이 있다면?">타인이 모르는 자신만의 신체 비밀이 있다면?</option>
									<option value="다시 태어나면 되고 싶은 것은?">다시 태어나면 되고 싶은 것은?</option>
									<option value="자신의 보물 제 1호는?">자신의 보물 제 1호는?</option>
									<option value="가장 존경하는 인물은?">가장 존경하는 인물은?</option>
									<option value="친구들에게 공개하지 않은 어릴 적 별명이 있다면?">친구들에게 공개하지 않은 어릴 적 별명이 있다면?</option>
									<option value="자신의 인행 좌우명은?">자신의 인행 좌우명은?</option>
									<option value="가장 친한 친구 이름은?">가장 친한 친구 이름은?</option>
									<option value="가장 종아하는 색깔은?">가장 종아하는 색깔은?</option>
									<option value="가장 기억에 남는 선생님 성함은?">가장 기억에 남는 선생님 성함은?</option>											
									<option value="내가 좋아하는 캐릭터는?">내가 좋아하는 캐릭터는?</option>
									<option value="나의 별명은?">나의 별명은?</option>
								</select>
								</div>
							</td>
						</tr>
						<tr>
							<th scope="row"><span>비밀번호 확인 답변</span></th>
							<td>
								<input type="text" class="inputText" name="userPwdA" title="비밀번호 확인 답변" value="${dto.userPwdA }">
							</td>
						</tr>
						<tr>
							<th scope="row"><span>휴대전화 번호</span></th>
							<td>
								<div class="selectCom">
									<select name="userTel1" title="전화번호 앞자리 선택" >
										<option value="${dto.userTel1 }">${dto.userTel1 }</option>
										<option value="010">010</option>
										<option value="011">011</option>
										<option value="016">016</option>
										<option value="017">017</option>
										<option value="018">018</option>
										<option value="019">019</option>
									</select>
								</div>
								<span>-</span>
								<input id="hp2" name="userTel2" class="inputText phone01" title="전화번호 가운데자리 입력" onkeydown="chckNumKey(this)" type="text" value="${dto.userTel2 }" maxlength="4">
								<span>-</span>
								<input id="hp3" name="userTel3" class="inputText phone01" title="전화번호 뒷자리 입력" onkeydown="chckNumKey(this)" type="text" value="${dto.userTel3 }" maxlength="4">
							</td>
						</tr>
						<tr>
							<th scope="row"><span>이메일</span></th>
							<td>
								<input id="email1" name="userEmail1" class="inputText email01" title="메일 ID 입력" type="text" value="${dto.userEmail1 }"> @
								<input id="email2" name="userEmail2" class="inputText email01" title="메일서비스 제공회사 입력" type="text" value="${dto.userEmail2 }">
								<div class="selectCom">
									<select id="email3" name="userEmail3" class="selectMail" title="메일서비스 제공회사 선택" >
										<option value="${dto.userEmail3 }">${dto.userEmail3 }</option>
										<option value="">직접입력</option>
										
										<option value="kebi.com">깨비메일</option>
									
										<option value="korea.com">코리아닷컴</option>
									
										<option value="naver.com">네이버</option>
									
										<option value="nate.com">네이트</option>
									
										<option value="daum.net">다음</option>
									
										<option value="dreamwiz.com">드림위즈</option>
									
										<option value="lycos.co.kr">라이코스</option>
									
										<option value="yahoo.co.kr">야후</option>
									
										<option value="empal.com">엠파스</option>
									
										<option value="chol.com">천리안</option>
									
										<option value="paran.com">파란</option>
									
										<option value="freechal.com">프리챌</option>
									
										<option value="hanafos.com">하나포스</option>
									
										<option value="hanmail.net">한메일</option>
									
										<option value="hotmail.com">핫메일</option>
									</select>	
								</div>
							</td>
						</tr>					
						<tr>
							<th scope="row"><span>거주지역</span></th>
							<td>
								<select id="addr_sido" name="userAddr" class="selectMail" >
									<option value="${dto.userAddr }">${dto.userAddr }</option>
									<option value="서울특별시">서울특별시</option>
									<option value="부산광역시">부산광역시</option>
									<option value="대구광역시">대구광역시</option>
									<option value="인천광역시">인천광역시</option>
									<option value="광주광역시">광주광역시</option>
									<option value="대전광역시">대전광역시</option>
									<option value="울산광역시">울산광역시</option>
									<option value="세종특별자치시">세종특별자치시</option>
									<option value="경기도">경기도</option>
									<option value="강원도">강원도</option>
									<option value="충청북도">충청북도</option>
									<option value="충청남도">충청남도</option>
									<option value="전라북도">전라북도</option>
									<option value="전라남도">전라남도</option>
									<option value="경상북도">경상북도</option>
									<option value="경상남도">경상남도</option>
									<option value="제주특별자치도">제주특별자치도</option>
								</select>
							</td>
						</tr>
						<tr>
							<th scope="row"><span>뉴스레터 수신 여부</span></th>
							<td>
								
								
								<c:if test="${dto.userNewsReceive eq 'Y'}" > 
									<div class="icr__hidden" id="icr-container__1">
										<input id="newsletter_yn1" name="userNewsReceive" title="수신" type="radio" value="${dto.userNewsReceive }" checked="checked"><label for="newsletter_yn1">수신</label>
									</div>
									<div class="icr__hidden" id="icr-container__2">
										<input id="newsletter_yn2" name="userNewsReceive" title="미수신" type="radio" value="N" ><label for="newsletter_yn2">미수신</label>
									</div>
								</c:if>

								<c:if test="${dto.userNewsReceive eq 'N'}" >
									<div class="icr__hidden" id="icr-container__1">
										<input id="newsletter_yn1" name="userNewsReceive" title="수신" type="radio" value="Y" ><label for="newsletter_yn1">수신</label>
									</div>
									<div class="icr__hidden" id="icr-container__2">
										<input id="newsletter_yn2" name="userNewsReceive" title="미수신" type="radio" value="${dto.userNewsReceive }" checked="checked"><label for="newsletter_yn2">미수신</label>
									</div>
								</c:if> 
								
								<span class="sTxt">매주 다양한 문화정보를 받아보실 수 있습니다. </span>
							</td>
						</tr>
						</tbody>

					</table>
					
					<!-- 선택동의 동의시 보여짐 -->
					<div class="btnBox">
						<button type="submit"><img src="/culturen/resources/img/content/btnConfirm.jpg" alt="수정하기"></button>
						<a href="http://192.168.16.11:8080/culturen/"><img src="/culturen/resources/img/content/btnCancel.jpg" alt="취소"></a>
					</div>
					
				</div>
				</form>
				<!-- 회원정보입력 : E -->
				<!-- 가입완료 : S -->
				<!-- 가입완료가 아닐경우 class="hide" 추가 -->
				<div class="step04Wrap hide">
					<p class="text01"> 가입완료</p>
					<p class="text02"> 회원 가입이 완료되었습니다.<br> 문화포털 회원이 되신 것을 환영합니다!</p>
					<div class="joinBox">
						<p class="text03">회원가입으로<strong>1,000P</strong> 가 적립되었습니다.</p>
						<p class="text04">포인트 적립 현황은 로그인 후 <strong>마이페이지 &gt; 나의 문화포인트</strong>에서 확인하실 수 있습니다.</p>
					</div>
					<div class="btnBox">
						<button type="submit"><img src="/culturen/resources/img/content/btnLogin01.jpg" alt="로그인"></button>
						<a href="#url"><img src="/culturen/resources/img/content/btnHome.jpg" alt="홈으로"></a>
					</div>
				</div>
				<!-- 가입완료 : E -->
			<span class="after"></span>
			</div>
			<!-- stepWrap : E -->
		</div>
		<!-- 회원 가입 : E -->
	</div>
	<div id="footerLogin">
		<ul>
			<li><a href="/info/termsOfUse.do">이용약관</a></li>
			<li><a href="/info/privacyPolicy.do">개인정보처리방침</a></li>
			<li><a href="/info/aboutA.do">문화포털이용안내</a></li>
		</ul>
		<p class="copyright">Copyrightⓒ  2014 <strong>Korea Culture Information Service Agency</strong> All Rights Reserved.</p>
	</div>
</div>

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>