<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="../layout/inc_top.jsp"/>





<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>


<script src="/js/jquery.jqtransform.js" type="text/javascript"></script>
<script src="/js/portal/view/login/login.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/people.js"></script>

<!-- <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="/css/portal/contentMember.css" rel="stylesheet" type="text/css">
<script src="/js/portal/lib/jquery/jquery.js" type="text/javascript"></script>
<script src="/js/portal/lib/jquery/external/jquery.customInput.js" type="text/javascript"></script>
<script src="/js/portal/view/login/login.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/people.js"></script>
<title>회원가입 - 실명인증 | 문화포털</title> -->


<script type="text/javascript">
	$().ready(function(){
		
		
		
		
		$("input[type='checkbox'],input[type='radio']").ionCheckRadio();

		$("input[type=text]").each(function(){
			$(this).focusin(function() {
			    $(this).parent().addClass('focus');
			    //$(this).attr('placeholder' , '');
			})

			$(this).focusout(function() {
			    $(this).parent().removeClass('focus');
				//$(this).attr('placeholder' , $(this).attr('title'));

			});
		})
	
		$('#icr-7').click(function() { 
			 checkedAll = $('#sAgree05').is(":checked");
			  
			 for(index = 1 ; index < 6 ; index++)
			 	$('#sAgree0' + index).attr("checked" , checkedAll);
			  
			 
			 for(index = 3 ; index < 7 ; index++) { 
			    if(!checkedAll)
			        $('#icr-' + index).removeClass("checked");
				else 
					$('#icr-' + index).addClass("checked");
			}
		});
		
		$('#ipinCertificationArea').click(function () { 
		    $('#mobileCertificationArea').removeClass('on');
		    $('#ipinCertificationArea').addClass('on');
		})

		$('#mobileCertificationArea').click(function () { 
		    $('#ipinCertificationArea').removeClass('on');
		    $('#mobileCertificationArea').addClass('on');
		})
		/* 
		if($('#mobileCertification').attr('class') == 'confirm') {
			$('#mobileCertification').removeClass('confirm');
			$('#mobileCertification').addClass('smsConfirm');
		} else {
			$('#mobileCertification').removeClass('smsConfirm');
			$('#mobileCertification').addClass('confirm');
		}
		 */
		 
		 
		 $('button[name=mobileSubmit]').click(function(){
			if($('input[name=smsCertNo]').val()  == '') { 
				alert('인증번호를 입력해 주세요');
				$('input[name=smsCertNo]').focus(); 
			    return false;     
			}

			$("form[name='join']").submit();
		});
		 
	 	$('#btn-cp-cert-retry').click(function(){
			var $form = $('#cpCertRequestForm');
			$form.find("input[name=smsReSndYn]").val('Y');
			$form.submit();
			
			return false;
		});
		$("button[name='gpin']").click(function(){
			
			document.name = "main";
			var wWidth = 360;
			var wHight = 120;

			var wX = (window.screen.width - wWidth) / 2;
			var wY = (window.screen.height - wHight) / 2;
		    var gPinLoginWin = window.open("/member/join_ipin.do", "gPinLoginWin", "directories=no,toolbar=no,left="+wX+",top="+wY+",width="+wWidth+",height="+wHight);
		    //gPinLoginWin.opener = "/member/join_ipin.do";
		    gPinLoginWin.focus();
		});
		
		var msg = '';
		if (msg != '') {
			alert( '인증번호가 재발송되었습니다.' );
		}
		
	});
	
	preambleView = function(seq) { 
		document.name = "main";
		var wWidth = 430;
		var wHight = 590;

		var wX = (window.screen.width - wWidth) / 2;
		var wY = (window.screen.height - wHight) / 2;
		var url = "/member/cp/pop_cp_agree" + seq + ".do";
	    var popTems = window.open(url, "cpLoginWin", "directories=no,toolbar=no,left="+wX+",top="+wY+",width="+wWidth+",height="+wHight);
	    popTems.focus();
	}
</script>
</head>
<body>
<div id="wrap" class="member">
	<div id="headerLogin">
		<h1><a href="/index.do"><span>문화포털</span></a></h1>
	</div>
	<div id="contentLogin">
		<!-- 회원 가입 : S -->
		<div class="memberWrap">
			
			<!-- 회원유형 : E -->
			<!-- 가입절차 : S -->
			<!-- 가입절차 아닐경우 class="hide" 추가 -->
			<div class="stepWrap">
				<ol class="stepTit">
					<!-- 선택시 class="on" 추가 -->
					<li class="step01"><em>약관동의</em></li>
					
						<li class="step02 on"><em>실명인증</em></li>
					
					
					<li class="step03"><em>회원정보입력</em></li>
					<li class="step03_01"><em>그래픽인증</em></li>
					<li class="step04"><em>가입완료</em></li>
				</ol>
				
				<!-- 약관동의 : E -->
				<!-- 실명인증 : S -->
				<!-- 실명인증 아닐경우 class="hide" 추가 -->
				<div class="step02Wrap">
					<form id="cpCertConfirmRequestForm" name="join" action="./join_cp_cert_number_confirm.do" method="post">
					<input id="svcTxSeqno" name="svcTxSeqno" type="hidden" value="20180402203901785">
					<input id="mbphnNo" name="mbphnNo" type="hidden" value="01057482004">
					<p class="text01">
					문화포털은 개인정보에 대한 유출 및 도용을 방지하기 위해 실명 인증을 시행하고 있습니다.<br>정보통신망 이용 촉진 및 정보보호 등에 관한 법률 제23조 2(주민등록번호의 사용 제한)에 의거하여 웹사이트 내 <br>주민등록번호 수집ᆞ이용이 금지되어 주민번호 입력 기반의 인증서비스 사용이 제한됩니다.
					</p>
					<ul class="tabSt01">
						<!-- 휴대폰인증시 class="on" 추가 -->
						<li id="mobileCertificationArea" class="on">
							<a href="#url">휴대폰인증</a>
							<!-- sms인증 일 경우 class="smsConfirm" -->
							<div id="mobileCertification" class="smsConfirm">
								<fieldset>
									<legend>휴대폰인증</legend>

									<!-- SMS 인증 : S -->
									<div class="smsC">
										<!-- focus될경우 class="focus" 추가 -->
										<div class="inputB">
											<!-- placeholder  class="placeholder" -->
											<input id="smsCertNo" name="smsCertNo" title="SMS인증번호 입력" placeholder="SMS인증번호 입력" class="inputMember" type="text" value="" maxlength="6">
											
										</div>
										<span class="button gray">
											<button id="btn-cp-cert-retry" type="button" class="btnGray">재전송</button>
										</span>
									</div>
									<!-- SMS 인증 : E -->
								</fieldset>
								<div class="btnBox">
									<button type="button" name="mobileSubmit"><img src="/images/portal/content/member/btnConfirm.jpg" alt="확인"></button>
								</div>
							</div>
						</li>
						<!-- 아이핀 인증 시 class="on" 추가 -->
						<li id="ipinCertificationArea">
							<a href="#url">아이핀인증</a>
							<div>
								<p class="txt">공공 아이핀(I-PIN, Internet Personal Identification Number)은 <br>행정자치부에서 제공하는 주민등록번호 대체수단으로 <br>인터넷상 개인 식별번호를 의미합니다.</p>
								<div class="btnBox">
									<button type="button" name="gpin"><img src="/images/portal/content/member/btnIpin.jpg" alt="아이핀 인증"></button>
								</div>
							</div>
						</li>
					</ul>
					</form>
				
					<form id="cpCertRequestForm" name="cpCertRequestForm" method="POST" action="./join_cp_cert_number_retry.do">
						<input type="hidden" name="svcTxSeqno" value="20180402203901785">
						<input type="hidden" name="mbphnNo" value="01057482004">
						<input type="hidden" name="smsReSndYn" value="">
					</form>	
					
				</div>
				
				<!-- 가입완료 : E -->
			</div>
			<!-- stepWrap : E -->
		</div>
		<!-- 회원 가입 : E -->
	</div>
	<!-- <div id="footerLogin">
		<ul>
			<li><a href="/info/termsOfUse.do">이용약관</a></li>
			<li><a href="/info/privacyPolicy.do">개인정보처리방침</a></li>
			<li><a href="/info/aboutA.do">문화포털이용안내</a></li>
		</ul>
		<p class="copyright">Copyrightⓒ  2014 <strong>Korea Culture Information Service Agency</strong> All Rights Reserved.</p>
	</div>
</div>

</body></html> -->


<br/><br/><br/><br/>

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>