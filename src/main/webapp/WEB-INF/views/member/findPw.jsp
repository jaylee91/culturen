<%@ page contentType="text/html; charset=UTF-8"%>
<jsp:include page="../layout/inc_top.jsp"/>



<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>	
<script src="/culturen/resources/js/jquery.jqtransform.js" type="text/javascript"></script>	

<script src="/culturen/resources/js/login.js" type="text/javascript"></script>	

	
</head>
<body>
<script type="text/javascript">
		$().ready(function(){
			
			$('.selectCom').jqTransform();
			
			$('ul li').each(function(){
				$(this).click(function(){
					if($(this).attr('class') != 'on') {
				      	$('ul li').removeClass('on');
				      	$(this).addClass('on');
					}
				});
			});

			$("div input").each(function(){
				type =  $(this).attr('type');
				
				if(type == 'text' || type == 'password') {
					$(this).focusin(function() {
						$(this).parent().addClass('focus');
						//$(this).attr('placeholder' , '');
					}) 
								
					$(this).focusout(function() {
						$(this).parent().removeClass('focus');
						//$(this).attr('placeholder' , $(this).attr('title'));
					}) 
				}
			})
			
			$("button[name='gpin']").click(function(){
				var wWidth = 360;
				var wHight = 120;
				var user_id = $("#fuid02").val();
				var wX = (window.screen.width - wWidth) / 2;
				var wY = (window.screen.height - wHight) / 2;
			    var gPinLoginWin = window.open("/member/join_ipin.do?user_id="+user_id, "gPinLoginWin", "directories=no,toolbar=no,left="+wX+",top="+wY+",width="+wWidth+",height="+wHight);
			    gPinLoginWin.focus();
			});
			
			$("#ussn1,#ussn2").keypress(function(event){			
				if (event.which && (event.which  > 47 && event.which  < 58 || event.which == 8)) {
			    } else {
			    	event.preventDefault();
			  	}
			});
			
		});
		
		doSubmit = function(frm) { 
			gubun = $(frm).find('input[name=gubun]').val();
			
			if($(frm).find('input[name=user_id]').val() == ""){
				alert('아이디를 입력해 주세요');
				$(frm).find('input[name=user_id]').focus();
				return false;
			}
			
			if(gubun == 1) { 
				 
				 if($(frm).find('input[name=name]').val() == ""){
					 alerT('이름을 입력해 주세요');
					 $(frm).find('input[name=name]').focus();
					 return false;
				 }
				 
				 if($(frm).find('input[name=email]').val() == ""){
						alert('이메일을 입력해 주세요');
						$(frm).find('input[name=email]').focus();
						return false;
				 }
			} else if(gubun == 3) { 
				if($('#fque > option:selected').val() == "" || !($('#fque > option:selected').val())) { 
					alert('비밀번호 확인 질문을 선택해 주세요');
					$('#fque').focus();
					return false;
				}
				
				if($(frm).find('input[name=pwd_result]').val() == ""){
					alert('비밀번호 확인 답변을 입력해 주세요');
					$(frm).find('input[name=pwd_result]').focus();
					return false;
				}
			} else {
				return false;
			}
			
			frm.action = "/culturen/findPw_ok.action";
			frm.submit();
			
		};
	</script>
		<div id="wrap" class="login">
	<div id="headerLogin">
		<h1><a href="/index.do"><span>문화포털</span></a></h1>
	</div>
	<p class="title01">비밀번호 찾기</p>
	<div id="contentLogin">
		<div class="loginForm PwSearch">
			<span class="before"></span>
			<ul class="tabSt01">
				<!-- 휴대폰인증시 class="on" 추가 -->
				<li class="on">
					<a href="#url">비밀번호 찾기</a>
					<div>
						<form action="#" method="post" onsubmit="doSubmit(this);return false;">
							<fieldset>
								<legend>임시 비밀번호 발급</legend>
								<p class="txt01">등록하신 정보로 아래 항목을 기입하시면 등록하신 이메일로 비밀번호를 전송해드리겠습니다.</p>
								<!-- focus될경우 class="focus" -->
								<div class="inputId">
									<!-- class="placeholder" 추가 -->
									<input name="userId" type="text" class="placeholder" title="아이디" placeholder="아이디">
								</div>
								<!-- focus될경우 class="focus" -->
								<div class="inputId">
									<!-- class="placeholder" 추가 -->
									<input name="userName" type="text" class="placeholder" title="이름" placeholder="이름">
								</div>
								<!-- focus될경우 class="focus" -->
								<div class="input">
									<input id="" name="userEmail1" type="text" class="placeholder" title="이메일주소" placeholder="이메일주소">@
									<input id="" name="userEmail2" type="text" class="placeholder" title="이메일주소" placeholder="이메일주소">
								</div>
								<br/>
								<input type="hidden" name="gubun" value="1">
								<!-- 로그인 버튼 : S -->
								<div class="btnLogin"><input type="image" src="/culturen/resources/img/content/btnConfirm01.jpg" alt="확인"></div>
								<!-- 로그인 버튼 : E -->
							</fieldset>
						</form>
					</div>
				</li>
				
				<!-- 
				<li>
					<a href="#url">비밀번호 확인 질문</a>
					<div>
						<form action="#" method="post" onsubmit="doSubmit(this);return false;">
							<fieldset>
								<legend>임시 비밀번호 발급</legend>
								<p class="txt01">회원가입 시 설정하신&nbsp;비밀번호 확인 질문 및 답변을 통해 임시비밀번호를 재발급 받으실 수 있습니다.</p>
								focus될경우 class="focus"
								<div class="inputId">
									class="placeholder" 추가
									<input name="user_id" type="text" class="placeholder" title="아이디 입력" placeholder="아이디 입력">
								</div>
								
								<div class="selectCom inputId jqtransformdone" style="width: 448px;">
									<div class="jq_sel" style="z-index: 20;"><div><span>비밀번호 확인 질문</span><a href="#" class="jqTransformSelectOpen">카테고리 선택</a></div><ul style="display: none; width: 444px; visibility: visible;"><li><a href="#" index="0" class="selected">비밀번호 확인 질문</a></li><li><a href="#" index="1">-내가 졸업한 초등학교는?</a></li><li><a href="#" index="2">-내가 태어난 도시는?</a></li><li><a href="#" index="3">-내가 여행하고 싶은 곳은?</a></li><li><a href="#" index="4">-가낭 인상 깊게 읽은 책이름은?</a></li><li><a href="#" index="5">-타인이 모르는 자신만의 신체 비밀이 있다면?</a></li><li><a href="#" index="6">-다시 태어나면 되고 싶은 것은?</a></li><li><a href="#" index="7">-자신의 보물 제 1호는?</a></li><li><a href="#" index="8">-가장 존경하는 인물은?</a></li><li><a href="#" index="9">-친구들에게 공개하지 않은 어릴 적 별명이 있다면?</a></li><li><a href="#" index="10">-자신의 인행 좌우명은?</a></li><li><a href="#" index="11">-가장 친한 친구 이름은?</a></li><li><a href="#" index="12">-가장 종아하는 색깔은?</a></li><li><a href="#" index="13">-가장 기억에 남는 선생님 성함은?</a></li><li><a href="#" index="14">-내가 좋아하는 캐릭터는?</a></li><li><a href="#" index="15">-나의 별명은?</a></li></ul><select id="fque" name="question" title="비밀번호 확인 질문 선택" class="jq_sel_hide" style="">
										<option value="">비밀번호 확인 질문</option>
										<option value="내가 졸업한 초등학교는?">-내가 졸업한 초등학교는?</option>
										<option value="내가 태어난 도시는?">-내가 태어난 도시는?</option>
										<option value="내가 여행하고 싶은 곳은?">-내가 여행하고 싶은 곳은?</option>
										<option value="가낭 인상 깊게 읽은 책이름은?">-가낭 인상 깊게 읽은 책이름은?</option>
										<option value="타인이 모르는 자신만의 신체 비밀이 있다면?">-타인이 모르는 자신만의 신체 비밀이 있다면?</option>
										<option value="다시 태어나면 되고 싶은 것은?">-다시 태어나면 되고 싶은 것은?</option>
										<option value="자신의 보물 제 1호는?">-자신의 보물 제 1호는?</option>
										<option value="가장 존경하는 인물은?">-가장 존경하는 인물은?</option>
										<option value="친구들에게 공개하지 않은 어릴 적 별명이 있다면?">-친구들에게 공개하지 않은 어릴 적 별명이 있다면?</option>
										<option value="자신의 인행 좌우명은?">-자신의 인행 좌우명은?</option>
										<option value="가장 친한 친구 이름은?">-가장 친한 친구 이름은?</option>
										<option value="가장 종아하는 색깔은?">-가장 종아하는 색깔은?</option>
										<option value="가장 기억에 남는 선생님 성함은?">-가장 기억에 남는 선생님 성함은?</option>											
										<option value="내가 좋아하는 캐릭터는?">-내가 좋아하는 캐릭터는?</option>
										<option value="나의 별명은?">-나의 별명은?</option>
									</select></div>
								</div>
								focus될경우 class="focus"
								<div class="inputId">
									<input id="fans" name="pwd_result" type="text" class="placeholder" title="비밀번호 확인 답변 입력" placeholder="비밀번호 확인 답변 입력">
								</div>
								<input type="hidden" name="gubun" value="3">
								로그인 버튼 : S
								<div class="btnLogin"><input type="image" src="/culturen/resources/img/content/btnConfirm01.jpg" alt="확인"></div>
								로그인 버튼 : E
							</fieldset>
						</form>
					</div>
				</li> 
				-->
				
			</ul>
			<span class="after"></span>
		</div>
	</div>
</div>

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>