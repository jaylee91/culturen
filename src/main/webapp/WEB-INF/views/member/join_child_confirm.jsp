
<%@ page contentType="text/html; charset=UTF-8"%>


<jsp:include page="../layout/inc_top.jsp"/>



<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>

<script src="/js/portal/view/login/login.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/people.js"></script>


<script type="text/javascript">
	$().ready(function(){
		
		$("input[type='checkbox'],input[type='radio']").ionCheckRadio();

		$("input[type=text]").each(function(){
			$(this).focusin(function() {
			    $(this).parent().addClass('focus');
			    //$(this).attr('placeholder' , '');
			})

			$(this).focusout(function() {
			    $(this).parent().removeClass('focus');
				//$(this).attr('placeholder' , $(this).attr('title'));

			});
		})

		$('#icr-7').click(function() { 
			 checkedAll = $('#sAgree05').is(":checked");
			  
			 for(index = 1 ; index < 6 ; index++)
			 	$('#sAgree0' + index).attr("checked" , checkedAll);
			  
			 
			 for(index = 3 ; index < 7 ; index++) { 
			    if(!checkedAll)
			        $('#icr-' + index).removeClass("checked");
				else 
					$('#icr-' + index).addClass("checked");
			}
		});
	});
	
	doSubmit = function() { 
		if($('input[name=name]').val()  == '') { 
			alert('이름을 입력해 주세요');
			$('input[name=name]').focus(); 
		    return false;     
		}
		if($('input[name=birth_year]').val()  == '') { 
			alert('출생년도를 입력해 주세요');
			$('input[name=birth_year]').focus(); 
			return false;
		}
		if ($('input[name=birth_year]').val().length != 4){
		    alert('출생년도를 4자리 숫자로 입력해 주세요.\r\n(예 : 2010)');
		    return false;
		}  
		if($("input[name='sex']:checked").size() == 0){
		    alert('성별을 선택해 주세요');
		    return false;
		}
		
		$('form').submit();
	}
	
	function alertErrorMessageByForm( msg, el ) {
		alert(msg);
		if(typeof el != 'undefined'){
			el.focus();
		}
		return false;
	}

</script>



<div id="wrap" class="member">
	<div id="headerLogin">
		<h1><a href="/index.do"><span>문화포털</span></a></h1>
	</div>
	<div id="contentLogin">
		<!-- 회원 가입 : S -->
		<div class="memberWrap">
			
			<!-- 회원유형 : E -->
			<!-- 가입절차 : S -->
			<!-- 가입절차 아닐경우 class="hide" 추가 -->
			<div class="stepWrap">
			<span class="before"></span>
				<ol class="stepTit">
					<!-- 선택시 class="on" 추가 -->
					<li class="step01"><em>약관동의</em></li>
					<li class="join14_01 on">
						<em class="join14">실명인증 &gt; 보호자 동의</em>
					</li>
					<li class="step03"><em>회원정보입력</em></li>
					<li class="step03_01"><em>그래픽인증</em></li>
					<li class="step04"><em>가입완료</em></li>
				</ol>
				
				<!-- 약관동의 : E -->
				<!-- 실명인증 : S -->
				<!-- 실명인증 아닐경우 class="hide" 추가 -->
				<div class="step02Wrap">
					<form action="/member/join_next2.do" name="join" method="post" onsubmit="javascript:doSubmit();return false;">
						<p class="text01">만 14세  미만 어린이는 보호자(법정대리인)와 함께 가입해 주시기 바랍니다.정보통신망이용촉진 및 정보보호 등에 관한 법률 제 21조 제 1항에서 만 14세 미만 아동의 개인정보 수집 시 부모의 동의를 얻도록 규정하고 있습니다.<br>만 14세 미만 어린이의 경우 회원가입 시 보호자(법정대리인)의 실명 인증을 통한 가입 동의가 필요합니다.
						</p>
						<h2>14세 미만 가입자 정보 입력</h2>
						<fieldset>
							<legend>휴대폰인증</legend>
							<div class="inputBox">
								<!-- focus될경우 class="focus" 추가 -->
								<div class="inputB focus">
								<!-- placeholder  class="placeholder" -->
									<input id="name" name="name" type="text" class="placeholder" title="이름 입력" placeholder="이름입력" maxlength="20">
								</div>
								<div class="inputB">
									<!-- placeholder  class="placeholder" -->
									<input id="birth_year" name="birth_year" type="text" class="placeholder" title="출생년도 입력" placeholder="출생년도 입력" maxlength="8">
								</div>
								<div class="radioBox">
									
									<div class="icr__hidden" id="icr-container__1"><input type="radio" id="man01" name="sex" value="1" checked="checked">
									<label for="man01">남자</label></div>
									<span class="icr enabled checked" id="icr-1">
									<!-- <span class="icr__radio"></span>
									<span class="icr__text">남자</span> -->
									</span>
									
									<div class="icr__hidden" id="icr-container__2"><input type="radio" id="woman01" name="sex" value="0">
									<label for="woman01">여자</label></div>
									<span class="icr enabled" id="icr-2">
									<!-- <span class="icr__radio"></span>
									<span class="icr__text">여자</span> -->
									</span>
								</div>
							</div>
						</fieldset>
						<div class="btnBox mt20">
							<button type="submit"><img src="/culturen/resources/img/content/btnConfirm.jpg" alt="확인"></button>
						</div>
						<!-- 14세미만 : E  -->
						<!-- 외국인 : S -->
						<ul class="list">
							<li>문화포털은 개인정보에 대한 유출 및 도용을 방지하기 위해 실명 인증을 시행하고 있습니다.</li>
							<li>정보통신망 이용 촉진 및 정보보호 등에 관한 법률 제23조 2(주민등록번호의 사용 제한)에 의거하여 <br>웹사이트 내 · 주민등록번호 수집·이용이 금지되어 주민번호 입력 기반의 인증서비스 사용이 제한됩니다.</li>
							<li>현재 문화포털에서는 본인을 확인할 수 있도록 휴대폰 본인확인 서비스와 공공아이핀서비스를<br> 제공하고 있습니다.</li>
						</ul>
					</form>
				</div>
				<!-- 가입완료 : E -->
			<span class="after"></span>
			</div>
			<!-- stepWrap : E -->
		</div>
		<!-- 회원 가입 : E -->
	</div>
<br/><br/><br/><br/><br/><br/>
<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>