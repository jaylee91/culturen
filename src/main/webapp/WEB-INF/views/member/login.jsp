<%@ page contentType="text/html; charset=UTF-8"%>


<jsp:include page="../layout/inc_top.jsp"/>
<!-- //inc_top -->


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->


<!-- 바디 부분  --> 			
			<!--S: container -->
			<!-- <div class="container"> -->
			<div class="">
				

  <script>
	$(document).ready(function(){

		// 프로모션
		var promotionSlide = $('.promotion_item').bxSlider({
			mode: 'fade',
			auto: true,
			autoControls: true,
			pause: 5000,
			speed:1000,
			nextText: '다음',
			prevText: '이전',
			autoControlsCombine:true,
			nextSelector: '.promo_next',
			prevSelector: '.promo_prev',
			autoControlsSelector:'.promo_stop',
			startText:'재생',
			stopText:'정지',
			onSliderLoad:function(){
				console.log('21212');
			}
		});

		$(document).on('click','.bx-next, .bx-prev, .bx-pager-link',function() {
			if(!$('.promo_stop').hasClass('on')){
				promotionSlide.stopAuto();
				promotionSlide.startAuto();
			}
		});

		$(document).on('click','.bx-stop',function() {
			var $this = $(this);
			if($('.promo_stop').hasClass('on')){
				$('.promo_stop').removeClass('on');
			}else{
				$('.promo_stop').addClass('on');
			}

			console.log("1111")
		});

		var concertSlide = $('.concert_list').bxSlider({
			slideWidth: 200,
			minSlides: 2,
			maxSlides: 5,
			moveSlides: 1,
			slideMargin: 20,
			pager:false,
			captions: true,
			nextText: '다음',
			prevText: '이전'
		});

		var concert01_html = $('.concert01').html();
		var concert02_html = $('.concert02').html();
		var concert03_html = $('.concert03').html();
		$('.concert_list').append(concert01_html);
		concertSlide.reloadSlider();


		//새로운공연, 후기많은공연 탭메뉴
		$(document).on('click' ,'.tab_tp01 > ul > li > button', function(e){
			var $this = $(this);
			var $idx = $(this).parent().index();
			$this.parent().siblings().removeClass('selected');
			$this.parent().addClass('selected');
			if($this.next().get(0).className != 'list'){
				$('.concert_list_outer').empty();
				var $firstConcert = $('.concert_list_wrap ul li:eq(' + $idx + ') .concert_list_outer').append('<ul class="concert_list">');
					switch ($idx) {
					case 0:
						$firstConcert.find('.concert_list').append(concert01_html);
						break;
					case 1:
						$firstConcert.find('.concert_list').append(concert02_html);
						break;
					case 2:
						$firstConcert.find('.concert_list').append(concert03_html);
						break;
				}
				$('.concert_list').bxSlider({
					slideWidth: 200,
					minSlides: 2,
					maxSlides: 5,
					moveSlides: 1,
					slideMargin: 20,
					pager:false,
					captions: true,
					nextText: '다음',
					prevText: '이전'
				});
			}
			e.preventDefault();
		});

		//이벤트
		$(document).on('click' ,'.ev_list > li > button', function(e){
			var $this = $(this);
			$this.parent().siblings().removeClass('selected');
			$this.parent().addClass('selected');
			e.preventDefault();
		});

		//이벤트 상세
		$(document).on('mouseenter' ,'.invite_gift ul a', function(){
			var $this = $(this);
			$this.parent().siblings().removeClass('on');
			$this.parent().addClass('on');
		});


		//공지사항
		var notiCounter = 1;
		var notiHeight = 20;
		var flag = true;
		var speed = 3000;
		var direction = 'next';
		var $target = $('.notice_wrap .list');
		var timer = setInterval(noticeSlide,speed);

		function noticeSlide(){
			$('.notice_wrap .control button').attr('disabled','disabled');
			if(direction == 'next'){
				$target.animate({top:-notiHeight},function(){
					$(this).find('li:eq(0)').insertAfter($(this).find('li:eq(3)'));
					$target.css('top','0');
					$('.notice_wrap .control button').removeAttr('disabled','disabled');

				});
			}else{
				$target.find('li:eq(3)').insertBefore($target.find('li:eq(0)'));
				$target.css('top','-20px');
				$target.animate({top:'0px'},function(){
					$target.css('top','0px');
					$('.notice_wrap .control button').removeAttr('disabled','disabled');
					direction = 'next';
				});
			}
		}

		$(document).on({
			mouseenter:function(){
				clearInterval(timer);
			},
			mouseleave:function(){
				if(flag){
					timer = setInterval(noticeSlide,speed);
				}
			}
		},'.notice_wrap');

		$(document).on('click' ,'.notice_wrap .control button', function(e){
			var $this = $(this);
			var $name = $this.get(0).className;
			switch ($name) {
				case 'stop':
					$this.addClass('play');
					$this.removeClass('stop');
					flag = false;
					clearInterval(timer);
					break;
				case 'play':
					$this.removeClass('play');
					$this.addClass('stop');
					clearInterval(timer);
					flag = true;
					break;
				case 'next':
					direction = 'next';
					clearInterval(timer);
					noticeSlide();
					break;
				case 'prev':
					direction = 'prev';
					clearInterval(timer);
					noticeSlide();
					break;
			}

			e.preventDefault();
		});

		//5대 지역 문화관광
		$(document).on('click' ,'.area_tourism .control button', function(e){
			var $this = $(this);
			var $target = $('.area_list');
			var $idx = $target.find('.selected').index();
			var $size = $target.find('li').size()-1;
			var $name = $this.get(0).className;
			var $color = ['#ffe63c'/*,'#3c98ff','#4641d4','#85a688','#ff7800'*/];

			if($idx >= $size){
				console.log(222);
			}else if($idx <= 0){
				console.log(444);
			}

			switch ($name) {
				case 'next':
					if($idx == $size){
						$idx = -1;
					}
					$target.find('li').removeClass('selected');
					$target.find('li:eq(' + ($idx+1) + ')').addClass('selected');
					$('.section02 .ly_right_bg').css('background',$color[$idx+1]);
					console.log($idx+1);
					break;
				case 'prev':
					$target.find('li').removeClass('selected');
					$target.find('li:eq(' + ($idx-1) + ')').addClass('selected');
					$('.section02 .ly_right_bg').css('background',$color[$idx-1]);
					console.log($idx-1);
					break;
			}
			e.preventDefault();
		});

		//프로모션 열고 닫기
		$(document).on('click' ,'.showHide_wrap button', function(e){
			var $this = $(this);
			if($this.hasClass('promo_open')){
				$this.removeClass('promo_open');
				$('.promotion').css('height','420px');
				$('.promotion .bx-wrapper, .promotion .control').css('display','block');
			}else{
				$this.addClass('promo_open');
				$('.promotion').css('height','0');
				$('.promotion .bx-wrapper, .promotion .control').css('display','none');
			}
			e.preventDefault();
		});

		//공연리스트 더보기
		moreList('show_list li:eq(0) .list', 5);
		$(document).on('click' ,'#show_list > ul > li > button', function(e){ //공연 상단텝
			var $this = $(this);
			var $target = $this.parent();
			var $idx = $target.index();
			var $wrap = $this.closest('.con_box');

			$wrap.css('height','500px');
			$('#show_list > ul > li:eq(' + $idx + ')').find('li').removeClass('active');
			moreList('show_list > ul > li:eq(' + $idx + ') .list', 5);
			e.preventDefault();
		});

		$(document).on('click' ,'#show_list .more', function(e){ // 공연더보기 버튼
			var $this = $(this);
			var $target = $this.parent();
			var $idx = $this.closest('.selected').index();
			var $wrap = $this.closest('.con_box');
			var $wrapHeight = $this.closest('.con_box').outerHeight();

			moreList('show_list > ul > li:eq(' + $idx + ') .list', 5);
			$wrap.css('height',$wrapHeight + 290);
			e.preventDefault();
		});

		function moreList(className, num){
			var $showList = $('#' + className).find('li').not('.active');
			var $showList_length = $showList.length;
			if(num >= $showList_length){
				num = $showList_length;
				$('#' + className).find('.more').hide();

			}else{
				$('#' + className).find('.more').show();
			}
			$('#' + className).find('li:not(".active"):lt(' + num + ')').addClass('active');

		}

	});
	</script> 
  <!-- 팝업 --> 
  <script language="Javascript">
  //function checkDayPopup(){
	  cookiedata = document.cookie;
  var dt = new Date();
  var year  = dt.getFullYear() ;
  var month = ""+ (dt.getMonth() + 1);
  var day   = ""+ dt.getDate();

  if(month.length == 1) month = "0"+month;
  if (day.length==1) day = "0"+day;
   var today = ""+year + month + day;
  //document.write("오늘날짜 : " + today + "<br>");
  var targetDay = "20180219";

if(today < targetDay){
	//alert("오늘이 작다 : " + today + "<br>");

	if ( cookiedata.indexOf("maindiv=done") < 0 ){
		document.all['divpop'].style.visibility = "visible";
		}
		else {
			document.all['divpop'].style.visibility = "hidden";
	}
}else{
	//alert(" 오늘이 크다 : " + today + "<br>");
	;

}
//}
		//팝업창
		function setCookie( name, value, expiredays ) {
			var todayDate = new Date();
				todayDate.setDate( todayDate.getDate() + expiredays );
				document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
			}

		function closeWin() {
			if ( document.notice_form.chkbox.checked ){
				setCookie( "maindiv", "done" , 1 );
			}
			document.all['divpop'].style.visibility = "hidden";
		}
	</script> 
  <!-- //팝업 --> 
 
<div class="container_body wide">
<div class="content_wrap" id="contents">
<div class="" style="width: 800px; margin:100px auto; top: 0px; left: 338px;" id="login_pop">
<div class="" style="height:50px;padding:15px 10px 10px 30px;background:#f1f1f1;border: 1px solid #e1e1e1;">
	<h2>문화N티켓 통합 로그인</h2>
</div>

<script src="/ticket/assets/js/jquery-cookie.js"></script>

<form id="loginForm2" action="/culturen/login_ok.action" method="post" onsubmit="doSubmit(this);return false;">

<input type="hidden" name="redirect_url" value="">

	<div class="" style="border: 1px solid #e1e1e1;">
		<div class="login_box">
			<h3 class="f_35 f_bold mgb30 mgt10" style="padding-top: 50px; padding-bottom: 20px;">로그인</h3>
			<fieldset>
				<legend>로그인</legend>
				<ul>
					<li class="">
						<label for="userId" class="hide">아이디</label>
						<input type="text" placeholder="아이디" name="userId" id="userId" class="comm_input fisrt_focus" title="아이디 입력">
					</li>
					<li>
						<label for="password" class="hide">비밀번호</label>
						<input type="password" placeholder="비밀번호" name="userPwd" id="password" class="comm_input" title="비밀번호 입력">
					</li>
				</ul>
				<button type="submit" id="login-btn" class="btn btn_red btn_login f_18 f_bold">로그인</button>
			</fieldset>
			<div class="btn_g">
				<div class="fl">
					<span class="check_b_wrap">
						<input type="checkbox" name="cookieId" id="cookieId">
						<span class="ico"></span>
						<label for="cookieId" class="txt">ID저장</label>
					</span>
				</div>
				<div class="mem_link fr">
					<ul>
						<li><a href="https://www.culture.go.kr/member/findId.do" target="_blank" title="새창">아이디 찾기</a></li>
						<li><a href="/culturen/findPw.action" target="_blank" title="새창">비밀번호 찾기</a></li>
						<li><a href="https://www.culture.go.kr/member/join.do" target="_blank" title="새창">회원가입</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="txt_list" style="padding-top: 30px; padding-bottom: 30px;">
			<ul>
				<li>문화포털의 통합회원이 되시면 하나의 ID로 문화포털과 문화N티켓 사이트의 다양한 서비스를 이용하실 수 있습니다.</li>
				<li>문화포털 회원이시면 별도의 회원가입 없이 사이트 이용이 가능합니다.</li>
			</ul>
		</div>
	</div>
</form>

</div>
</div>
</div>



</div>
<!--E: container -->
	
<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>

<button class="btn_top" style="display: none;"><span class="txt">TOP</span></button>


<!-- <div class="popup_outer pop_open" style="top: 0px; width: 100%; height: 520px; overflow: auto;"> -->
<!-- <div class="popup_outer pop_open" style="top: 100px; width: 100%; height: 520px; "> -->
<!-- <div class="" style="top: 100px; width: 100%; height: 520px; ">

			

<div class="" style="width: 650px; top: 0px; left: 338px;" id="login_pop">
	<div class="">
		<h2>문화N티켓 통합 로그인</h2>
	</div>

<script src="/ticket/assets/js/jquery-cookie.js"></script>



<form id="loginForm2" action="https://www.culture.go.kr/member/login_certification.do" method="post" onsubmit="doSubmit(this);return false;">

<input type="hidden" name="redirect_url" value="">

	<div class="popup_c pdt30 pdb30" style="padding-bottom:30px !important;">
		<div class="login_box">
			<h3 class="f_35 f_bold mgb30 mgt10">로그인</h3>
			<fieldset>
				<legend>로그인</legend>
				<ul>




					<li class=""><label for="userId" class="hide">아이디</label><input type="text" placeholder="아이디" name="usr_id" id="userId" class="comm_input fisrt_focus" title="아이디 입력"></li>
					<li><label for="password" class="hide">비밀번호</label><input type="password" placeholder="비밀번호" name="pwd" id="password" class="comm_input" title="비밀번호 입력"></li>
				</ul>
				<button type="submit" id="login-btn" class="btn btn_red btn_login f_18 f_bold">로그인</button>
			</fieldset>
			<div class="btn_g">
				<div class="fl">
					<span class="check_b_wrap">
						<input type="checkbox" name="cookieId" id="cookieId">
						<span class="ico"></span>
						<label for="cookieId" class="txt">ID저장</label>
					</span>
				</div>
				<div class="mem_link fr">
					<ul>
						<li><a href="https://www.culture.go.kr/member/findId.do" target="_blank" title="새창">아이디 찾기</a></li>
						<li><a href="https://www.culture.go.kr/member/findPw.do" target="_blank" title="새창">비밀번호 찾기</a></li>
						<li><a href="https://www.culture.go.kr/member/join.do" target="_blank" title="새창">회원가입</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="txt_list">
			<ul>
				<li>문화포털의 통합회원이 되시면 하나의 ID로 문화포털과 문화N티켓 사이트의 다양한 서비스를 이용하실 수 있습니다.</li>
				<li>문화포털 회원이시면 별도의 회원가입 없이 사이트 이용이 가능합니다.</li>
			</ul>
		</div>
	</div>
</form>
<form id="loginForm" action="/ticket/login/login.do" method="post">	
<input type="hidden" name="returnURL" value="http://www.culture.go.kr/ticket/main/main.do">
<input type="hidden" name="nonmemberLogin" id="nonmemberLogin" value="">
	// 관람후기, 기대평, 댓글 글쓰기 시 노출하지 않음
	
	<div class="popup_c bg_gray pdb60 pdt30" >
		<div class="login_box">
			<h3 class="f_24 f_bold mgb30 mgt20">비회원(예매확인)</h3>
			로그인, 예매/취소 내역 메뉴 선택 시
			<fieldset>
				<legend>비회원(예매확인)</legend>
				<ul>
					<li><label for="mbrId" class="hide">예매자명</label><input type="text" placeholder="휴대폰 뒤4자리" name="mbrId" id="mbrId" class="comm_input" title="휴대폰 뒤4자리 입력"></li>
					<li><label for="tempPassword" class="hide">대표번호</label><input type="password" placeholder="임시비밀번호 4자리" name="tempPassword" id="tempPassword" class="comm_input" title="임시비밀번호 4자리 입력"></li>
				</ul>
				<button type="submit" id="nonmember-login-btn" class="btn btn_dark btn_login f_18 f_bold">확인</button>
			</fieldset>
			// 로그인, 예매/취소 내역 메뉴 선택 시
		</div>
		<div class="ac f_13 f_gray">
			비회원으로 티켓을 예매하신 고객님은 휴대폰번호와 임시비밀번호로 예매 내역을 확인하실 수 있습니다.
		</div>
	</div>
	
	
	// 관람후기, 기대평, 댓글 글쓰기 시 노출하지 않음
</form>
	<button class="pop_close btn_ico"><span class="ico">닫기</span></button>
<button class="last_btn btn_ico"></button></div> -->

<script type="text/javascript">
	$(function(){
		var idSave = function () {
			if ( $("input[name='cookieId']").attr("checked"))
				$.cookie('culture_id',$("#uid").val());
			else
				$.cookie('culture_id',null);
			
			//alert('소중한 개인정보 보호를 위해 개인 pc에서만 사용해 주세요');
		};

		$("div input").each(function(){
			type =  $(this).attr('type');
			
			if(type == 'text' || type == 'password') {
				$(this).focus(function() {
					$(this).parent().addClass('focus');
					//$(this).attr('placeholder' , '');
				});
							
				$(this).blur(function() {
					$(this).parent().removeClass('focus');
					//$(this).attr('placeholder' , $(this).attr('title'));
				});
			}
		})

		if ( $.cookie('culture_id') != null ){
			
			$("#uid").val($.cookie('culture_id'));
			$('#icr-1').addClass('checked');
			$('input[name=cookieId]').attr('checked', true);
			
		}
		
		$("input[name='cookieId']").change(function(){
			if($(this).is(':checked'))
				idSave();
		});
		
	});
	
	function validate(){
		if( $('#nonmemberLogin').val() == 'Y' ){
			
			if($('#mbrId').val()  == '' || typeof $('#mbrId').val() == 'undefined') {
			    alert('휴대폰 뒤4자리를 입력해 주세요');
			    $('#mbrId').focus(); 
			   	return false;
			}
			
			if($('#tempPassword').val()  == '' || typeof $('#tempPassword').val() == 'undefined') { 
			    alert('임시비밀번호를 입력해 주세요');
			    $('#tempPassword').focus(); 
			   	return false;
			}
			
		}else{
			
			if($('#userId').val()  == '' || typeof $('#userId').val() == 'undefined') {
			    alert('아이디를 입력해 주세요');
			    $('#userId').focus(); 
			   	return false;
			}
			
			if($('#password').val()  == '' || typeof $('#password').val() == 'undefined') { 
			    alert('비밀번호를 입력해 주세요');
			    $('#password').focus(); 
			   	return false;
			}
		}
		
		
		return true;
	}
	
	function doSubmit(frm){
// 		if ( $("input[name='loginId']").val() == "" ){
			
// 			alert("아이디를 입력하세요.");
// 			$("input[name='loginId']").focus();
// 			return;
			
// 		}

// 		if ( $("input[name='loginPwd']").val() == "" ){
			
// 			alert("비밀번호를 입력하세요.");
// 			$("input[name='loginPwd']").focus();
// 			return;
			
// 		}
//  		var returnURL2 = $("#returnURL2").val();

		if ( $("#loginForm2 input[name='id']").val() == "" ){
			
			alert("아이디를 입력하세요.");
			$("#loginForm2 input[name='id']").focus();
			return;
			
		}

		if ( $("#loginForm2 input[name='pw']").val() == "" ){
			
			alert("비밀번호를 입력하세요.");
			$("#loginForm2 input[name='pw']").focus();
			return;
			
		}
		



		var tempReturnURL = 'http://www.culture.go.kr/ticket/sso/CreateRequest.jsp';
 		$('#loginForm2 [name="redirect_url"]').attr('value', tempReturnURL);


		
		frm.submit();
	}
</script>
		<script type="text/javascript">
        	$(function(){
        		$("#nonmember-login-btn").click(function(){
        			$("#nonmemberLogin").val('Y');
        		});
        		$("#login-btn").click(function(){
        			$("#nonmemberLogin").val('');
        		});
        		
   				$("#loginForm").submit(function(){
   					
   					if( validate() ) {
   					
	   					var formData = $("#loginForm").serialize();
	   					
	   					//alert("loginForm submit returnURL = " + $("#returnURL").val());
	   		 
	   					$.ajax({
	  			 			type : "POST",
	  			 			url : "/ticket/login/login_ajax.do",
	  			 			cache : false,
	  			 			data : formData,
	  			 			success : onSuccess,
	  			 			error : onError
	   					});
   					}
   					return false;
   				});
   			});
   			function onSuccess(json, status){
   				
   				//alert(JSON.stringify(json));
   				
   				var success = json.result.success;
   				var returnURL = json.result.returnURL;
   				if(success){
   					if(returnURL.indexOf("/ticket") == -1)
   						returnURL = "/ticket" + returnURL;
   						
   					location.href= returnURL;
   				}else{
   					var messages = json.result.messages;
   					if( messages != 'null' ){
	   					alert(messages);
   					}
   				}
   			}
   			function onError(data, status){alert("로그인 처리 중 오류가 발생했습니다.");}
        </script>
