<%@ page contentType="text/html; charset=UTF-8"%>


<jsp:include page="../layout/inc_top.jsp"/>






<!-- <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ko" lang="ko"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>
<!-- <title>회원가입 | 문화포털</title>
</head>
<body> -->
	<script type="text/javascript">
		
		$(document).ready(function(){
			
			$('input[type=image]').each(function () {
				if($(this).attr('memberType')) {
					$(this).click(function() {
				    	$('input[name=gubun]').val($(this).attr('memberType'));
				    	$( "form[name=memberTypeForm]").submit();
				    })
				}
			});
		});
	</script>
	<!-- <form action="https://www.culture.go.kr/member/join_next.do" name="memberTypeForm" method="post"> -->
	<form action="/member/join_next.do" name="memberTypeForm" method="post">
		<input type="hidden" name="gubun" value="">		
	</form>
	<div id="wrap" class="member">
		<div id="headerLogin">
			<h1>
				<a href="/index.do">
					<span>문화포털</span>
				</a>
			</h1>
		</div>
		<div id="contentLogin">
			<div class="memberWrap">
				<div class="joinHome">
				<span class="before"></span>
					<p class="text01">문화포털 <strong>회원가입</strong>을 환영합니다.</p>
					<p class="text02">회원 종류에 따라 가입 절차가 다르니 반드시 본인이 해당하는 회원 유형을 선택해주세요.</p>
					<ul class="joinType">
						<!-- 
						<li class="type01">
							<span>만 14세 미만</span>
							<strong>어린이 회원</strong>
							<input type="image" membertype="A" title="어린이 회원 회원가입하기" src="/culturen/resources/img/content/btnJoin.jpg" alt="회원가입">
						</li>
						-->
						<li class="type02"></li>
						<li class="type02">
							<span>만 14세 이상</span>
							<strong>일반 회원</strong>
							<a href="http://192.168.16.11:8080/culturen/join_next.action">
								<input type="image" membertype="B" title="일반 회원 회원가입하기" src="/culturen/resources/img/content/btnJoin.jpg" alt="회원가입">
							</a>
						</li>
						<li class="type02"></li>						
						<!-- 
						<li class="type03">
							<span>국내 거주</span>
							<strong>외국인 회원</strong>
							<input type="image" membertype="C" title="외국인 회원 회원가입하기" src="/culturen/resources/img/content/btnJoin.jpg" alt="회원가입">
						</li>
						-->
					</ul>
				<span class="after"></span>
				</div>
			</div>
		</div>
		
		<!-- <div id="footerLogin">
		<ul>
			<li><a href="/info/termsOfUse.do">이용약관</a></li>
			<li><a href="/info/privacyPolicy.do">개인정보처리방침</a></li>
			<li><a href="/info/aboutA.do">문화포털이용안내</a></li>
		</ul>
		<p class="copyright">Copyrightⓒ  2014 <strong>Korea Culture Information Service Agency</strong> All Rights Reserved.</p>
	</div>
	</div>

</body></html> -->




<br/><br/><br/><br/>

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>