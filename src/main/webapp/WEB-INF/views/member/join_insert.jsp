<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="../layout/inc_top.jsp"/>


<link href="/culturen/resources/css/contentMember.css" rel="stylesheet" type="text/css">
<!-- <link href="/culturen/resources/css/default.css" rel="stylesheet" type="text/css">
<link href="/culturen/resources/css/common.css" rel="stylesheet" type="text/css"> -->
<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>
<script src="/culturen/resources/js/jquery.customInput.js" type="text/javascript"></script>

<script src="/js/portal/view/login/login.js" type="text/javascript"></script>


<title>일반회원 회원가입 - 가입완료 | 문화포털</title>


<script type="text/javascript">
	goLogin = function(pid) {
		if(pid == 'W'){
			location.href='http://192.168.16.11:8080/culturen/login.action';
		}else{
			location.href='http://192.168.16.11:8080/culturen/login.action';
		}
	}
</script>

<!-- AceCounter 스크립트-->

<!-- This script is for AceCounter START -->
<script language="javascript">
var _ag = 0 ; // 로그인사용자 나이
var _id = ''; // 로그인사용자 아이디
var _mr = ''; // 로그인사용자 결혼여부 ('single' , 'married' )
var _gd = ''; // 로그인사용자 성별 ('man' , 'woman')
var _skey = '' ; // 내부검색어
var _jn = '' ; // 가입탈퇴 ( 'join','withdraw' )
var _jid = '' ; // 가입시입력한 ID
var _ud1 = '' ; // 사용자 정의변수 1 ( 1 ~ 10 정수값)
var _ud2 = '' ; // 사용자 정의변수 2 ( 1 ~ 10 정수값)
var _ud3 = '' ; // 사용자 정의변수 3 ( 1 ~ 10 정수값)
</script>
<!-- AceCounter END -->
<!-- AceCounter 스크립트-->


</head>
<body>
<div id="wrap" class="member">
	<div id="headerLogin">
		<h1><a href="/index.do"><span>문화포털</span></a></h1>
	</div>
	<div id="contentLogin">
		<!-- 회원 가입 : S -->
		<div class="memberWrap">
			<!-- 회원유형 : E -->
			<!-- 가입절차 : S -->
			<!-- 가입절차 아닐경우 class="hide" 추가 -->
			<div class="stepWrap">
			<span class="before"></span>
				<ol class="stepTit">
					<!-- 선택시 class="on" 추가 -->
					<li class="step01"><em>약관동의</em></li>
					<li class="step02"><em>실명인증</em></li>
					<li class="step03"><em>회원정보입력</em></li>
					<li class="step03_01"><em>그래픽인증</em></li>
					<li class="step04  on"><em>가입완료</em></li>
				</ol>
				<!-- 회원정보입력 : E -->
				<!-- 가입완료 : S -->
				<!-- 가입완료가 아닐경우 class="hide" 추가 -->
				<div class="step04Wrap">
					<p class="text01"> 가입완료</p>
					<p class="text02"> 회원 가입이 완료되었습니다.<br> 문화포털 회원이 되신 것을 환영합니다!</p>
					<!--<div class="joinBox">
						<p class="text03">회원가입으로<strong>1,000P</strong> 가 적립되었습니다.</p>
						<p class="text04">포인트 적립 현황은 로그인 후 <strong>마이페이지 &gt; 나의 문화포인트</strong>에서 확인하실 수 있습니다.</p>
					</div>-->
					<div class="btnBox">
						<button type="button" onclick="goLogin('P')"><img src="/culturen/resources/img/content/btnLogin01.jpg" alt="로그인"></button>
						
						
							<a href="/culturen"><img src="/culturen/resources/img/content/btnHome.jpg" alt="홈으로"></a>
						
					</div>
				</div>
				<!-- 가입완료 : E -->
			<span class="after"></span>
			</div>
			<!-- stepWrap : E -->
		</div>
		<!-- 회원 가입 : E -->
	</div>
	<div id="footerLogin">
		<ul>
			<li><a href="/info/termsOfUse.do">이용약관</a></li>
			<li><a href="/info/privacyPolicy.do">개인정보처리방침</a></li>
			<li><a href="/info/aboutA.do">문화포털이용안내</a></li>
		</ul>
		<p class="copyright">Copyrightⓒ  2014 <strong>Korea Culture Information Service Agency</strong> All Rights Reserved.</p>
	</div>
</div>



	<!-- *) 공통 분석스크립트  -->
	<!-- *) 공통 분석스크립트  -->
<!-- AceCounter Log Gathering Script  -->

<script type="text/javascript">
var _skey = '' ;        // 내부검색어

var _ag   = 0 ;         // 로그인사용자 나이
var _id   = '';                         // 로그인사용자 아이디
var _mr   = '';             // 로그인사용자 결혼여부 ('single' , 'married' )
var _gd   = '';         // 로그인사용자 성별 ('man' , 'woman')
var _jn = 'join' ;          //  가입탈퇴 ( 'join','withdraw' )
var _jid = 'son100v' ;                           // 가입시입력한 ID

if(typeof EL_GUL == 'undefined'){
var EL_GUL = 'gtr.kcisa.kr';var EL_GPT='80'; var _AIMG = new Image(); var _bn=navigator.appName; var _PR = location.protocol=="https:"?"https://"+EL_GUL:"http://"+EL_GUL+":"+EL_GPT;if( _bn.indexOf("Netscape") > -1 || _bn=="Mozilla"){ setTimeout("_AIMG.src = _PR+'/?cookie';",1); } else{ _AIMG.src = _PR+'/?cookie'; };
 document.writeln("");
 }
</script>

<!-- 한국문화정보센터 - 문화포털 웹로그 수집  TAG 시작 -->
<script type="text/javascript">
//<![CDATA[
//분석로그 수집 : 고정 
var _BT_SITE = "log.kcisa.kr";

// 로그인사용자에 대한 정보. 로그인되었을 경우 첨부
//var _BT_LOGINSEG = "userid" + "_/" + "성별( 1 :남성, 2:여성, 3:기타(법인포함) )" + "_/" + "연령";
// 로그인 되었을 경우 해당 값을 위 예시를 참고하여 작성하여 주시기 바랍니다.
var _BT_LOGINSEG = ""; 
//DEBUG 용도. on 으로 되어 있으면 script 실행결과가 alert 으로 나옴
//var _BT_DEBUG_ = "on";

document.write("<sc" + "ript type='text/java" + "scr" + "ipt' src='"
			+ ( document.location.protocol == "https:" ? "https://" : "http://" )
			+ _BT_SITE + "/tag/WebNibbler_culture_go_kr.js" + "'>");
document.write("</sc" + "ript>");
//]]>
</script><script type="text/javascript" src="https://log.kcisa.kr/tag/WebNibbler_culture_go_kr.js"></script><script src="https://cr.acecounter.com/Web/AceCounter_AW.js?gc=BH6A39943863534&amp;py=1&amp;gd=gtp15&amp;gp=8080&amp;up=NaPm_Ncisy&amp;rd=1522670214445" type="text/javascript"></script>


<noscript>
	&lt;img src="http://log.kcisa.kr/tag/tag.jsp?site_cd=www.culture.go.kr" width="0" height="0" border="0" alt="" /&gt;
</noscript>
<!-- 한국문화정보센터 - 문화포털 웹로그 수집  TAG 종료 -->

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>
