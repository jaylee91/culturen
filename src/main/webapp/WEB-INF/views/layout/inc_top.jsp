<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<title>문화 N 티켓</title>
		

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link href="/culturen/resources/css/import.css" rel="stylesheet">
<link href="/culturen/resources/css/layout.css" rel="stylesheet">

<link href="/ticket/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<!-- <script src="http://cr.acecounter.com/Web/AceCounter_AW.js?gc=AH4A42094072613&amp;py=0&amp;gd=gtb14&amp;gp=8080&amp;up=NaPm_Ncisy&amp;rd=1522631238917"></script> -->
<script src="/culturen/resources/js/AceCounter_AW422IVGRA.js"></script>

<!-- <script src="/ticket/assets/js/jquery.min.js"></script>기존 것 -->
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>

<script src="/culturen/resources/js/jquery-ui.js"></script>

<script src="/ticket/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/culturen/resources/js/popup.js"></script>
<script src="/culturen/resources/js/common.js"></script>
		
<!--[if lt IE 9]>
	<script src="/ticket/assets/js/html5shiv.js"></script>
	<script src="/ticket/assets/js/respond.min.js"></script>
<![endif]-->
		<link href="/culturen/resources/css/main.css" rel="stylesheet">
		<link href="/culturen/resources/css/jquery.bxslider.css" rel="stylesheet">
		<script src="/culturen/resources/js/jquery.bxslider.js"></script>
	</head>
	<body class="main">
		<div id="skipNavi">
			<ul>
				<li><a href="#contents">본문 바로가기</a></li>
				<li><a href="#gnb">주메뉴 바로가기</a></li>
			</ul>
		</div>

		<div id="wrap">
<!--S: Top -->
			


<div class="header_wrap">
	<header>
		<!-- <h1><a href="/ticket/"><img src="../../layout/images/common/logo.png" alt="문화N티켓"></a></h1> -->
		<h1><a href="http://192.168.16.11:8080/culturen/"><img src="/culturen/resources/img/common/logo.png" alt="문화N티켓"></a></h1>
		<div class="t_group">
			<ul>
			
			
				<c:choose>
				<c:when test="${empty sessionScope.customInfo.userId }">
					<li><a href="/culturen/login.action" class="" target="_blank" >로그인</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="/culturen/logout.action" class="" >로그아웃</a></li>
				</c:otherwise>
				</c:choose>
				
				<li><a href="http://192.168.16.11:8080/culturen/join.action" target="_blank" title="새창">회원가입</a></li>
			
				<li><a href="/ticket/mypage/cancle_list.action">예매/취소내역</a></li>
				<li><a href="/ticket/useInfo/guide.action">예매 가이드</a></li>
				<li><a href="/ticket/about/introduction.action">문화N티켓 소개</a></li>
				<li><a href="http://www.culture.go.kr/ticketadmin" target="_blank">참여단체 로그인</a></li>
			</ul>
		</div>
		<div id="gnb">
			<ul>
				<li class=""><a href="/culturen/concert_list.action">공연</a></li>
				<li class=""><a href="/culturen/exhibit_list.action">전시</a></li>
				<li class=""><a href="/culturen/storyList.action">문화이야기</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/ticket/culture/storyList.action">공연제작 이야기</a></li>
						<li><a href="/ticket/culture/actorList.action">문화인물 이야기</a></li>
						<li><a href="/ticket/culture/societyList.action">문화단체 이야기</a></li>
						<li><a href="/ticket/culture/tourList.action">문화관광 이야기</a></li>
					</ul>
				</li>
				<li class=""><a href="/ticket/event/ingList.action">이벤트</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/culturen/ingList.action">진행중 이벤트</a></li>
						<li><a href="/culturen/endList.action">지난 이벤트</a></li>
						<li><a href="/culturen/winList.action">당첨자 확인</a></li>
					</ul>
				</li>
				<li class=""><a href="/ticket/useInfo/guide.action">이용안내</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/culturen/guide.action">예매 가이드</a></li>
						<li><a href="/culturen/faq.action">FAQ</a></li>
						<li><a href="/culturen/ticketInfo.action">티켓판매 안내</a></li>
						<li><a href="/culturen/notice.action">공지사항</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="allmenu">
			<a href="#" class="all">전체메뉴<span>열기</span></a>
			<div class="all_list">
				<ul>
					<li class="dep01"><a href="/culturen/concert/concert_list.action">공연</a></li>
					<li class="dep01"><a href="/culturen/exhibit/exhibit_list.action">전시</a></li>
					<li class="dep01"><a href="/culturen/culture/storyList.action">문화이야기</a>
						<ul>
							<li><a href="/ticket/culture/storyList.action">공연제작 이야기</a></li>
							<li><a href="/ticket/culture/actorList.action">문화인물 이야기</a></li>
							<li><a href="/ticket/culture/societyList.action">문화단체 이야기</a></li>
							<li><a href="/ticket/culture/tourList.action">문화관광 이야기</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/event/ingList.action">이벤트</a>
						<ul>
							<li><a href="/ticket/event/ingList.action">진행중 이벤트</a></li>
							<li><a href="/ticket/event/endList.action">지난 이벤트</a></li>
							<li><a href="/ticket/event/winList.action">당첨자 확인</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/useInfo/guide.action">이용안내</a>
						<ul>
							<li><a href="/ticket/useInfo/guide.action">예매 가이드</a></li>
							<li><a href="/culturen/faq.action">FAQ</a></li>
							<li><a href="/ticket/useInfo/ticketInfo.action">티켓판매 안내</a></li>
							<li><a href="/culturen/notice.action">공지사항</a></li>
						</ul>
					</li>
					
					
					<c:choose>
					<c:when test="${!empty sessionScope.customInfo.userId }">
						<li class="dep01"><a href="/ticket/useInfo/guide.action">마이페이지</a>
							<ul>
								<li><a href="/culturen/join_update.action">개인정보수정</a></li>
								<li><a href="/ticket/useInfo/faq.action">Q&A</a></li>
								<!-- 
								<li><a href="/ticket/useInfo/ticketInfo.action">티켓판매 안내</a></li>
								<li><a href="/ticket/useInfo/notice.action">공지사항</a></li>
								-->
							</ul>
						</li>
					</c:when>
					</c:choose>
					
					<li class="dep01"><a href="http://192.168.16.11:8080/culturen/">문화N티켓 소개</a>
						<ul>
							
							<c:choose>
							
							
							<c:when test="${empty sessionScope.customInfo.userId }">
								<li><a href="http://192.168.16.11:8080/culturen/login.action" class="" target="_blank" >로그인</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="http://192.168.16.11:8080/culturen/logout.action" class="" >로그아웃</a></li>
							</c:otherwise>
							</c:choose>
							
							<li><a href="http://192.168.16.11:8080/culturen/join.action" class="f_red" target="_blank" title="새창">회원가입</a></li>
							
							<li><a href="/ticket/polc/policy.action">개인정보처리방침</a></li>
							<li><a href="/ticket/polc/term.action">이용약관</a></li>
							<li><a href="/ticket/polc/copy.action">저작권정책</a></li>
							<li class="end"><a href="/ticket/polc/collect.action">검색결과수집정책</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<form action="/ticket/search/search.action">
		<div class="t_search_wrap">
			<label for="keyword" class="hidden">통합검색</label>
			<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력하세요." class="comm_input" title="검색어 입력">
			<input type="image" class="btn_search" src="/culturen/resources/img/common/ico_search.png" alt="검색">
		</div>
		</form>
	</header>
	<div class="gnb_bg" style="height: 0px;"></div>
	<!-- 
	<a href="#" data-id="login_pop" class="lay_pop" id="login-pop-link" style="display:none">로그인</a>
	<a href="#" data-id="login_pop3" class="lay_pop" id="login-pop-link3" style="display:none">로그인</a>
	-->
</div>
<!-- //inc_top -->



