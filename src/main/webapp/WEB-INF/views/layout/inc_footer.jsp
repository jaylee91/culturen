<%@ page contentType="text/html; charset=UTF-8"%>
<div class="footer">
	<div class="footer_body">
		<div class="btm_t_wrap">
			<ul class="link">
				<li><a href="/ticket/polc/policy.do">개인정보처리방침</a></li>
				<li><a href="/ticket/polc/term.do">이용약관</a></li>
				<li><a href="/ticket/polc/copy.do">저작권정책</a></li>
				<li><a href="/ticket/polc/collect.do">검색결과수집정책</a></li>
			</ul>
			<ul class="sns">
					<!-- <li class="snsT"><a title="새창열림" href="https://www.facebook.com/culture.N.ticket/" target="_blank"><span>문화N티켓 페이스북 바로가기</span></a></li> -->
					<li class="snsF"><a title="새창열림" href="https://www.instagram.com/culture_n_ticket/" target="_blank"><span>문화N티켓 인스타그램 바로가기</span></a></li>
					<li class="snsB"><a title="새창열림" href="https://blog.naver.com/PostThumbnailList.nhn?blogId=kcis_&amp;from=postList&amp;categoryNo=71&amp;parentCategoryNo=71" target="_blank"><span>문화N티켓 블로그 바로가기</span></a></li>
			</ul>
			<div class="family_site">
				<a href="#none" class="family_btn">관련사이트</a>
				<ul>
					<li><a href="http://www.culture.go.kr/" target="_blank" title="새창">문화포털</a></li>
					<li><a href="http://www.culture.go.kr/data/" target="_blank" title="새창">문화데이터광장</a></li>
					<li><a href="http://www.culture.go.kr/wday/" target="_blank" title="새창">매달 마지막 수요일은 문화가 있는 날</a></li>
				</ul>
			</div>
		</div>

		<div class="copy_wrap">
			<div class="logo_g">
				<a href="http://www.mcst.go.kr/" target="_blank" title="새창"><img src="/culturen/resources/img/btm_logo01.gif" alt="문화체육관광부"></a>
				<a href="http://www.kcisa.kr/" target="_blank" title="새창"><img src="/culturen/resources/img/btm_logo02.gif" alt="한국문화정보원"></a>
			</div>

			<address>
				우)03925 서울시 마포구 월드컵북로 400 <br>
				대표전화:02-3153-2898, 이메일:ticket@kcisa.kr, Copyright 한국문화정보원<br>
				사업자 등록번호 101-82-10054 대표자 : 이현웅
			</address>

			<div class="certify_wrap">

				<a href="http://www.culture.go.kr/" target="_blank" title="새창"><img src="/culturen/resources/img/btm_r_02.gif" alt="문화포털"></a>
			</div>
		</div>

	</div>
</div>

<button class="btn_top" style="display: none;"><span class="txt">TOP</span></button>


<!-- AceCounter Log Gathering Script V.7.5.2017020801 -->
<script language="javascript">
	var _AceGID=(function(){var Inf=['gtb14.acecounter.com','8080','AH4A42094072613','AW','0','NaPm,Ncisy','ALL','0']; var _CI=(!_AceGID)?[]:_AceGID.val;var _N=0;var _T=new Image(0,0);if(_CI.join('.').indexOf(Inf[3])<0){ _T.src =( location.protocol=="https:"?"https://"+Inf[0]:"http://"+Inf[0]+":"+Inf[1]) +'/?cookie'; _CI.push(Inf);  _N=_CI.length; } return {o: _N,val:_CI}; })();
	var _AceCounter=(function(){var G=_AceGID;var _sc=document.createElement('script');var _sm=document.getElementsByTagName('script')[0];if(G.o!=0){var _A=G.val[G.o-1];var _G=(_A[0]).substr(0,_A[0].indexOf('.'));var _C=(_A[7]!='0')?(_A[2]):_A[3];var _U=(_A[5]).replace(/\,/g,'_');_sc.src=(location.protocol.indexOf('http')==0?location.protocol:'http:')+'//cr.acecounter.com/Web/AceCounter_'+_C+'.js?gc='+_A[2]+'&py='+_A[4]+'&gd='+_G+'&gp='+_A[1]+'&up='+_U+'&rd='+(new Date().getTime());_sm.parentNode.insertBefore(_sc,_sm);return _sc.src;}})();
</script>
<noscript>&lt;img src='http://gtb14.acecounter.com:8080/?uid=AH4A42094072613&amp;je=n&amp;' border='0' width='0' height='0' alt=''&gt;</noscript>	
<!-- AceCounter Log Gathering Script End -->
			<!--E: footer -->
		</div>
	
</body></html>