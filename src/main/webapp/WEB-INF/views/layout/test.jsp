<%@ page contentType="text/html; charset=UTF-8"%>




<html lang="ko"><head>
		<title>공연제작 이야기 &lt; 문화이야기 &lt; 문화 N 티켓</title>
		

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link href="/ticket/assets/css/import.css" rel="stylesheet">
<link href="/ticket/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="http://cr.acecounter.com/Web/AceCounter_AW.js?gc=AH4A42094072613&amp;py=0&amp;gd=gtb14&amp;gp=8080&amp;up=NaPm_Ncisy&amp;rd=1522656117601"></script><script src="/ticket/assets/js/jquery.min.js"></script>
<script src="/ticket/assets/js/jquery-ui.js"></script>
<script src="/ticket/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/ticket/assets/js/popup.js"></script>
<script src="/ticket/assets/js/common.js"></script>
		
<!--[if lt IE 9]>
	<script src="/ticket/assets/js/html5shiv.js"></script>
	<script src="/ticket/assets/js/respond.min.js"></script>
<![endif]-->
	</head>
	<body>
		<div id="skipNavi">
			<ul>
				<li><a href="#contents">본문 바로가기</a></li>
				<li><a href="#gnb">주메뉴 바로가기</a></li>
			</ul>
		</div>

		<div id="wrap">
			<!--S: Top -->
			


<div class="header_wrap">
	<header>
		<h1><a href="/ticket/"><img src="/ticket/assets/img/logo.png" alt="문화N티켓"></a></h1>
		<div class="t_group">
			<ul>
			
				<li><a href="#login_pop" class="lay_pop" data-id="login_pop2" id="login-pop-link2">로그인</a></li>
				<li><a href="https://www.culture.go.kr/member/join.do" target="_blank" title="새창">회원가입</a></li>
			
				<li><a href="/ticket/mypage/cancle_list.do">예매/취소내역</a></li>
				<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
				<li><a href="/ticket/about/introduction.do">문화N티켓 소개</a></li>
				<li><a href="http://www.culture.go.kr/ticketadmin" target="_blank">참여단체 로그인</a></li>
			</ul>
		</div>
		<div id="gnb">
			<ul>
				<li class=""><a href="/ticket/concert/concert_list.do">공연</a></li>
				<li class=""><a href="/ticket/exhibit/exhibit_list.do">전시</a></li>
				<li class="selected"><a href="/ticket/culture/storyList.do">문화이야기</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/ticket/culture/storyList.do">공연제작 이야기</a></li>
						<li><a href="/ticket/culture/actorList.do">문화인물 이야기</a></li>
						<li><a href="/ticket/culture/societyList.do">문화단체 이야기</a></li>
						<li><a href="/ticket/culture/tourList.do">문화관광 이야기</a></li>
					</ul>
				</li>
				<li class=""><a href="/ticket/event/ingList.do">이벤트</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/ticket/event/ingList.do">진행중 이벤트</a></li>
						<li><a href="/ticket/event/endList.do">지난 이벤트</a></li>
						<li><a href="/ticket/event/winList.do">당첨자 확인</a></li>
					</ul>
				</li>
				<li><a href="/ticket/useInfo/guide.do">이용안내</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
						<li><a href="/ticket/useInfo/faq.do">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
						<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="allmenu">
			<a href="#" class="all">전체메뉴<span>열기</span></a>
			<div class="all_list">
				<ul>
					<li class="dep01"><a href="/ticket/concert/concert_list.do">공연</a></li>
					<li class="dep01"><a href="/ticket/exhibit/exhibit_list.do">전시</a></li>
					<li class="dep01"><a href="/ticket/culture/storyList.do">문화이야기</a>
						<ul>
							<li><a href="/ticket/culture/storyList.do">공연제작 이야기</a></li>
							<li><a href="/ticket/culture/actorList.do">문화인물 이야기</a></li>
							<li><a href="/ticket/culture/societyList.do">문화단체 이야기</a></li>
							<li><a href="/ticket/culture/tourList.do">문화관광 이야기</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/event/ingList.do">이벤트</a>
						<ul>
							<li><a href="/ticket/event/ingList.do">진행중 이벤트</a></li>
							<li><a href="/ticket/event/endList.do">지난 이벤트</a></li>
							<li><a href="/ticket/event/winList.do">당첨자 확인</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/useInfo/guide.do">이용안내</a>
						<ul>
							<li><a href="/ticket/useInfo/guide.do">예매 가이드</a></li>
							<li><a href="/ticket/useInfo/faq.do">FAQ</a></li>
							<li><a href="/ticket/useInfo/ticketInfo.do">티켓판매 안내</a></li>
							<li><a href="/ticket/useInfo/notice.do">공지사항</a></li>
						</ul>
					</li>
					
					<li class="dep01"><a href="/ticket/about/introduction.do">문화N티켓 소개</a>
						<ul>
							
							<li><a href="#" class="f_red lay_pop" data-id="login_pop2">로그인</a></li>
							<li><a href="https://www.culture.go.kr/member/join.do" class="f_red" target="_blank" title="새창">회원가입</a></li>
							
							<li><a href="/ticket/polc/policy.do">개인정보처리방침</a></li>
							<li><a href="/ticket/polc/term.do">이용약관</a></li>
							<li><a href="/ticket/polc/copy.do">저작권정책</a></li>
							<li class="end"><a href="/ticket/polc/collect.do">검색결과수집정책</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<form action="/ticket/search/search.do">
		<div class="t_search_wrap">
			<label for="keyword" class="hidden">통합검색</label>
			<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력하세요." class="comm_input" title="검색어 입력">
			<input type="image" class="btn_search" src="/ticket/assets/img/ico_search.png" alt="검색">
		</div>
		</form>
	</header>
	<div class="gnb_bg" style="height: 0px;"></div>
	<a href="#" data-id="login_pop" class="lay_pop" id="login-pop-link" style="display:none">로그인</a>
	<a href="#" data-id="login_pop3" class="lay_pop" id="login-pop-link3" style="display:none">로그인</a>
</div>


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm03">
						<li class="selected"><a href="/ticket/culture/storyList.do">공연제작 이야기</a></li>
						<li><a href="/ticket/culture/actorList.do">문화인물 이야기</a></li>
						<li><a href="/ticket/culture/societyList.do">문화단체 이야기</a></li>
						<li><a href="/ticket/culture/tourList.do">문화관광 이야기</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->
				






			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>공연제작 이야기</h2>
						<p>하나의 공연이 무대에 올라가기 까지의 과정을 이야기 합니다.</p>
					</div>
					<form method="GET">
					<div class="board_search_wrap pdb0">
						<div class="fr">
							<label for="FIELD" class="hidden">검색구분</label>
							<select name="category" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
								<option value="0" selected="">전체</option>
								<option value="3">공연명</option>
								<option value="1">제목</option>
								<option value="2">내용</option>
							</select>
							<label for="KEY" class="hidden">검색어</label>
							<input type="text" placeholder="검색어를 입력하세요." name="keyword" value="" title="검색어 입력" class="comm_input scr_elem">
							<button class="btn btn_search scr_elem">검색</button>
						</div>
					</div>
					</form>
					
					<p class="mgb10 f_bold">총 1 건</p>
					<div class="thum_list">
						<ul>
							
							<li>
								<a href="storyView.do?seq=12&amp;">
									
									<div class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/7bb6d1cf566c49e0ac3eb3695d492f6b.png" style="height:145px;" alt="아동/가족 <콘서트 포 키즈 어린이를 위한 콘서트>"></div>
									<div class="con_box">
										<p class="subject">아동/가족 &lt;콘서트 포 키즈 어린이를 위한 콘서트&gt;</p>
										<p class="tit">[예시] 어린이를 위한 겨울 콘서트</p>
										<p class="txt">즐겁고 경쾌한 캐롤 음악과 동요를 중심으로 들을 수 있는 어린이 콘서트 '콘서트 포 키즈'는 기타, 타악기로 구성된 콘서트입니다. 또한 가수들이 배우로 함께 나와 즐겁고 재미있는 캐롤 음악을 함께 부르며, 새로운 스토리를 만들어가는 이야기로 어린이들이 흥미와 집중도를 높일 수 있도록 연출·제작 하였습니다. 이처럼 겨...</p>
									</div>
								</a>
							</li>
							
						</ul>
					</div>
					
					<div class="pageing">
						
							<a href="?pageNo=1" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1" class="last"><span>마지막 페이지 이동</span></a>
						
					</div>

				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<div class="footer">
	<div class="footer_body">
		<div class="btm_t_wrap">
			<ul class="link">
				<li><a href="/ticket/polc/policy.do">개인정보처리방침</a></li>
				<li><a href="/ticket/polc/term.do">이용약관</a></li>
				<li><a href="/ticket/polc/copy.do">저작권정책</a></li>
				<li><a href="/ticket/polc/collect.do">검색결과수집정책</a></li>
			</ul>
			<ul class="sns">
					<!-- <li class="snsT"><a title="새창열림" href="https://www.facebook.com/culture.N.ticket/" target="_blank"><span>문화N티켓 페이스북 바로가기</span></a></li> -->
					<li class="snsF"><a title="새창열림" href="https://www.instagram.com/culture_n_ticket/" target="_blank"><span>문화N티켓 인스타그램 바로가기</span></a></li>
					<li class="snsB"><a title="새창열림" href="https://blog.naver.com/PostThumbnailList.nhn?blogId=kcis_&amp;from=postList&amp;categoryNo=71&amp;parentCategoryNo=71" target="_blank"><span>문화N티켓 블로그 바로가기</span></a></li>
			</ul>
			<div class="family_site">
				<a href="#none" class="family_btn">관련사이트</a>
				<ul>
					<li><a href="http://www.culture.go.kr/" target="_blank" title="새창">문화포털</a></li>
					<li><a href="http://www.culture.go.kr/data/" target="_blank" title="새창">문화데이터광장</a></li>
					<li><a href="http://www.culture.go.kr/wday/" target="_blank" title="새창">매달 마지막 수요일은 문화가 있는 날</a></li>
				</ul>
			</div>
		</div>

		<div class="copy_wrap">
			<div class="logo_g">
				<a href="http://www.mcst.go.kr/" target="_blank" title="새창"><img src="/ticket/assets/img/btm_logo01.gif" alt="문화체육관광부"></a>
				<a href="http://www.kcisa.kr/" target="_blank" title="새창"><img src="/ticket/assets/img/btm_logo02.gif" alt="한국문화정보원"></a>
			</div>

			<address>
				우)03925 서울시 마포구 월드컵북로 400 <br>
				대표전화:02-3153-2898, 이메일:ticket@kcisa.kr, Copyright 한국문화정보원<br>
				사업자 등록번호 101-82-10054 대표자 : 이현웅
			</address>

			<div class="certify_wrap">

				<a href="http://www.culture.go.kr/" target="_blank" title="새창"><img src="/ticket/assets/img/btm_r_02.gif" alt="문화포털"></a>
			</div>
		</div>

	</div>
</div>

<button class="btn_top"><span class="txt">TOP</span></button>


<!-- AceCounter Log Gathering Script V.7.5.2017020801 -->
<script language="javascript">
	var _AceGID=(function(){var Inf=['gtb14.acecounter.com','8080','AH4A42094072613','AW','0','NaPm,Ncisy','ALL','0']; var _CI=(!_AceGID)?[]:_AceGID.val;var _N=0;var _T=new Image(0,0);if(_CI.join('.').indexOf(Inf[3])<0){ _T.src =( location.protocol=="https:"?"https://"+Inf[0]:"http://"+Inf[0]+":"+Inf[1]) +'/?cookie'; _CI.push(Inf);  _N=_CI.length; } return {o: _N,val:_CI}; })();
	var _AceCounter=(function(){var G=_AceGID;var _sc=document.createElement('script');var _sm=document.getElementsByTagName('script')[0];if(G.o!=0){var _A=G.val[G.o-1];var _G=(_A[0]).substr(0,_A[0].indexOf('.'));var _C=(_A[7]!='0')?(_A[2]):_A[3];var _U=(_A[5]).replace(/\,/g,'_');_sc.src=(location.protocol.indexOf('http')==0?location.protocol:'http:')+'//cr.acecounter.com/Web/AceCounter_'+_C+'.js?gc='+_A[2]+'&py='+_A[4]+'&gd='+_G+'&gp='+_A[1]+'&up='+_U+'&rd='+(new Date().getTime());_sm.parentNode.insertBefore(_sc,_sm);return _sc.src;}})();
</script>
<noscript>&lt;img src='http://gtb14.acecounter.com:8080/?uid=AH4A42094072613&amp;je=n&amp;' border='0' width='0' height='0' alt=''&gt;</noscript>	
<!-- AceCounter Log Gathering Script End -->
			<!--E: footer -->
		</div>
	
</body></html>