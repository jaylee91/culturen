<%@page import="com.culturen.dao.MemberDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<!-- JSTL 지시어 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<title>아이디 중복 확인 | 문화포털</title>
	<link href="/culturen/resources/css/popup.css" rel="stylesheet" type="text/css">
	<script src="/culturen/resources/js/jquery.js" type="text/javascript"></script>
	
</head>
<body>

	<script type="text/javascript">

		function idCheck(obj)
		{
		    var userId = $("input[name='userId']").val();
		    var seq = 0 , seq1=0;
		    var alpha = 'abcdefghijklmnopqrstuvwxyz';
		    var digit = '1234567890 ';
		    var alphaCheck = false;
		    var numberCheck = false;
	
		     if ( userid.length < 6 || userid.length > 12 ) {
		         alert("아이디는 6 ~ 12 자리까지 입니다!");
		         $("input[name='userId']").val("");
		         $("input[name='userId']").focus();
		         return false;
		     }else{
		    	if (userid.indexOf(' ') > -1) {
			  	        alert("공백을 입력할 수 없습니다.");
			  	        $("input[name='userId']").val("");
				        $("input[name='userId']").focus();
			  	        return false;
				}
		    	
		     	var count=0
		    	  	for (i=0;i<userId.length;i++){
		    	    	ls_one_char = userId.charAt(i);
		    	 
		    	    	if(ls_one_char.search(/[a-z|A-Z|0-9]/) == -1) {
		    	   			count++
		    	   		}
		    	  	}
		    	  	if(count!=0) {
		    	  		alert("특수문자는 사용하실 수 없습니다.") 
		    	    	$("input[name='userId']").val("");
		         		$("input[name='userId']").focus();
		 	   	    return false;
		    	  	}
		    	  	
		 	   	 for(var i=0; i<userId.length; i++){
		 	 		   if(alpha.indexOf(userId.charAt(i)) != -1){
		 	 		    	alphaCheck = true;
		 	 		   }
		 	 		   if(digit.indexOf(userId.charAt(i)) != -1){
		 	 		    	numberCheck = true;
		 	 		   }
		 	  	}//for
		   		
		 	  	/*
		 		if(alphaCheck != true || numberCheck != true){
		 		    alert("영문,숫자 1자 이상으로 조합해주세요.");
		 		    $("input[name='user_id']").val("");
			        $("input[name='user_id']").focus();
		 		    return false;
		 		}//if
		  		 */
		  		
		  		splitedDigit1 = userId.split("") ; 
		     	var initStr = splitedDigit1[0] ;
		     	
		     	for(i=0; i< splitedDigit1.length-2; i++){ 
		     	  if( (splitedDigit1[i-1] == splitedDigit1[i] && splitedDigit1[i] == splitedDigit1[i+1] && splitedDigit1[i+1] == splitedDigit1[i+2] )){ 
		     		  alert ("4자이상 동일한 숫자나 문자를 입력하시면 안됩니다.");
		     		  $("input[name='userId']").val("");
			          $("input[name='userId']").focus();
		     		  return false;
		     	  }  
		         }// end for
		         
		     }
	
		     //return true;
		     obj.submit();
		 }
		
		function setId(userId){
			var d = document.domain ;

			while ( true )
			{
				// Test if we can access a parent property.
				try
				{
					var test = window.top.opener.document.domain ;
					break ;
				}
				catch( e )
				{}

				// Remove a domain part: www.mytest.example.com => mytest.example.com => example.com ...
				d = d.replace( /.*?(?:\.|$)/, '' ) ;

				if ( d.length == 0 )
					break ;		// It was not able to detect the domain.

				try
				{
					document.domain = d ;
				}
				catch (e)
				{
					break ;
				}
			}
			var id = userId;
			
			$("#userId",opener.document).val(id);
			$("#hid_user_id",opener.document).val(id);
			$("#idChk",opener.document).val("Y");
			
			
		}
	
	</script>
	<h1><span>아이디 중복 확인</span></h1>
	<div class="content">
		<div class="checkID">
			<!-- s : 사용 할 수 있는 ID -->
			
			<c:if test="${!empty id }">
				<p class="text02">
					<span>${userId }</span> 는 이미 사용중인 아이디입니다.
				</p>
			</c:if>
			
			<c:if test="${empty id }">
				<p class="text02">
					<span>${userId }</span> 는 사용할 수 있는 아이디 입니다.
					<span class="button white">
						<button type="button" onclick="setId('${userId }');self.close();">사용하기</button>
					</span>
					<p class="note">다른 아이디를 사용하시고자 할 경우에는 다른 아이디를 입력하시고 중복확인을 클릭하세요.</p>
				</p>
			</c:if>
			
			<form action="/culturen/checkID.action" method="post" onsubmit="idCheck(this);return false;">
				<label>
					<span>다른 아이디 입력</span>
					<input type="text" class="inputText checkID" name="userId" title="검색할 ID 입력" maxlength="12">	
				</label>
				<span class="button gray"><button type="submit">중복확인</button></span>
			</form>
		</div>
	</div>


<%-- 
<c:if test="${empty id}">
 	${userId }는(은) 사용할 수 있는 아이디 입니다.
</c:if>

<c:if test="${!empty id }">
	${userId }는(은) 사용하실 수 없는 아이디 입니다.
</c:if>
 --%>

 </body>
</html>