<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../layout/inc_top.jsp"/>

<!--E: Top -->
<link rel="stylesheet" type="text/css" href="/ticket/assets/css/required.css">	
<link rel="stylesheet" href="/maps/documentation/javascript/demos/demos.css">
<script src="http://cr.acecounter.com/Web/AceCounter_AW.js?gc=AH4A42094072613&amp;py=0&amp;gd=gtb14&amp;gp=8080&amp;up=NaPm_Ncisy&amp;rd=1522716911035"></script><script src="http://175.125.91.99:3000/examples/seatmap-selector.js"></script>
<script src="/culturen/resources/js/seatmap_factory.js"></script>

<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				
				





<script src="/ticket/assets/js/concert.js"></script>

<input type="hidden" id="contextPath" value="/ticket">



		<div class="container">
			<div class="container_body">
				<div class="content_wrap" id="contents">

					<div class="concert_detail">
						<h2 class="tit">[${dto.realmName }] ${dto.title } <span class="area"> ${dto.area }</span></h2>
						<div class="detail_body">
							<div class="lt_left">
								
								<div class="poster"><img src="${dto.imgUrl }"></div>
							</div>
							<div class="lt_right">
								<div class="detail_info">
									<div class="dafault_list">
										<ul>
											<c:if test="${!empty dto.subTitle }">
											<li><span class="tit">부제</span><span class="hidden">:</span>${dto.subTitle }</li>
											</c:if>
											<li><span class="tit">장소</span><span class="hidden">:</span><a href="javascript:goTab(2);" class="arrow_link">${dto.place }</a></li>
											<li><span class="tit">기간</span><span class="hidden">:</span>${dto.startDate } ~ ${dto.endDate }</li>
											<li><span class="tit">입장연령</span><span class="hidden">:</span></li>
											<li><span class="tit">관람시간</span><span class="hidden">:</span></li>
											<li><span class="tit">공연단체</span><span class="hidden">:</span>	<a href="javascript:goTab(3);" class="arrow_link">연극집단 반</a>	</li>
<!-- 											<li><span class="tit">주최/주관</span><span class="hidden">:</span></li> -->
										</ul>
									</div>
									<div class="info_list_wrap">
										<div class="info_list tit_fixed">
											<h3>가격</h3>
											<ul>
												
												<li class="wide"><span class="tit">정상가격</span>
												<span class="hidden">:</span>
												<span class="price">${dto.price }</span>
												</li>
												
												
											</ul>
											
										</div>
										<div class="info_list">
											<h3>할인</h3>
											<ul>
												
													<li><span class="tit">일반</span><span class="hidden">:</span><span class="f_red">30,000원</span></li>
												
													<li><span class="tit">문화패스(만24세 이하 &amp; 대학생)</span><span class="hidden">:</span><span class="f_red">15,000원</span></li>
												
												
											</ul>
											
											<div class="sail_info">
												<button class="ico_btn ico_btn_gray btn_sail_info f_normal"><span class="txt ico_r ico_arrow_dw">할인정보 더보기</span></button>
												<div class="layer_pop" style="width:340px;">
													<div class="popup_h">
														<h2>할인정보 안내</h2>
													</div>
													<div class="popup_c">
														<ul class="sail_info_txt mCustomScrollbar _mCS_1 mCS_no_scrollbar"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: 0px;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
															
																<li>예술인패스 &amp; 공연예술계 종사자 할인 - <span class="f_red">15,000원 </span></li>
															
																<li>국가유공자 &amp; 장애인할인(동반1인) - <span class="f_red">15,000원 </span></li>
															
														</div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></ul>
													</div>
													<button class="pop_close btn_ico"><span class="ico">닫기</span></button>
												</div>
											</div>
											

										</div>
									</div>
								</div>
								<div class="detail_btm">
									<div class="btm_l">

										<a href="#" onclick="shareFacebook(); return false;"><img src="/culturen/resources/img/ico_facebook.png" alt="facebook으로 퍼가기"></a>
										<a href="#" onclick="shareTwitter(); return false;"><img src="/culturen/resources/img/ico_twiter.png" alt="twitter로 퍼가기"></a>
										<button class="ico_btn btn_like mgl20"><span class="txt ico ico_heart">좋아요</span></button>
										
										<input type="hidden" id="likeCnt" value="0">
										<input type="hidden" id="likeProductCd" value="PR000116">
										<button class="ico_btn btn_cart" onclick="javascript: myInterest();"><span class="txt ico ico_interest">관심공연담기</span></button>
									</div>
									
									<div class="btm_r">
									
									
									
									
									
  
    
    	
    									<button class="btn btn_large btn_red ticketStepBtn1" data-id="ticketing" id="btn-ticketing"><span class="txt">예매하기 </span></button>
										<input type="hidden" id="companyCd" name="companyCd" value="TA000160">
										<input type="hidden" id="productCd" name="productCd" value="PR000116">
										<input type="hidden" id="facilityCd" name="facilityCd" value="FA004815">
		
		
    
    

									</div>
									
								</div>
							</div>
						</div>
					</div>

					<!--탭정보 start -->
					<div class="detail_v_tab">
						<ul>
							<li style="text-align: center;"><a href="#" class="on" id="7">상세정보</a></li>
							<!--관람후기 -->
							<li style="text-align: center;"><a href="#" id="0" class="">관람후기</a></li>
							<!--기대평 -->
							<li style="text-align: center;"><a href="#" id="1">기대평</a></li>
							<li style="text-align: center;"><a href="#" id="2">장소 정보</a></li>
							<li style="text-align: center;"><a href="#" id="3">단체 정보</a></li>
							<!--출연 정보 -->
							<li style="text-align: center;"><a href="#" id="4">출연 정보</a></li>
							<li style="text-align: center;"><a href="#" id="5">취소/환불 안내</a></li>
						</ul>
					</div>
					<!--탭정보 END -->

					<!--상세정보 -->
					<div class="concert" id="concertInfo" style="display: block;">

						<div class="detail_con">
							
							
							${dto.contents1}
							
							
							<!--StartFragment--><p style="line-height: 2;"><span style="font-weight: bold;">&lt;공지사항&gt;</span><br></p><p style="line-height: 2;">1) 티켓수령은 공연 당일 공연시작 1시간 전부터 해당 공연장 매표소에서 가능합니다.</p><p style="line-height: 2;">2) 할인을 적용 받으셨을 경우, 공연 당일 해당 증빙자료 미지참 시 정가 기준의 차액을 지불해주셔야 티켓 수령이 가능합니다.</p><p style="line-height: 2;">3) 고등학생 이상관람가입니다.</p><p style="line-height: 2;">예매 전에 필히 확인을 요하며, 관람 연령 미달로 인한 변경/취소/환불은 불가합니다.<br></p><p style="line-height: 2;">4) 주차장이 매우 협소하므로 대중 교통 이용을 권장하며, 주차 문제와 교통난으로 인한 변경/취소/환불은 불가합니다.</p><p style="line-height: 2;">5) 공연시작 후에는 입장이 제한되오니 공연시작 전까지 반드시 입장해주시기 바라며, 이로 인한 변경/취소/환불은 불가합니다.<br></p><p style="line-height: 2;">6) 공연소요시간은 변동될 수 있습니다.<br></p><p style="line-height: 2;">7) 공연 중 사전에 협의되지 않은 사진 촬영/동영상 촬영/녹음은 금지되어 있습니다.<br></p><p style="line-height: 2;">8) 휠체어석 이용을 원하시는 경우 고객지원센터(02-3668-0007)로 문의해주시기 바랍니다.<br></p><p style="line-height: 2;"><br></p><p style="line-height: 2;"><span style="font-weight: bold;">&lt;할인 정보&gt;</span><br></p><p style="line-height: 2;">-조기예매할인 4월15일까지 -50%-15,000원</p><p style="line-height: 2;">-국가유공자 &amp; 장애인할인 (동반1인) -50%-15,000원-관람당일 복지카드,유공자증 지참-미지참시 차액지불</p><p style="line-height: 2;">-문화패스 할인 (만24세 이하 &amp; 대학생) -50%-15,000원-관람당일 증빙자료 지참﻿-미지참시 차액지불</p><p style="line-height: 2;">-예술인패스 &amp; 공연예술계 종사자 할인 -본인 -50%-15,000원 -관람당일 증빙자료 지참-미지참시 차액지불</p><p style="line-height: 2;">-단체할인 20명이상 -기획사 문의</p><div style="line-height: 2;"><br></div>
							
						</div>

						<table class="board_view_tp02">
							<caption>공연에 관한 상세정보를 보실 수 있습니다.</caption>
							<colgroup>
								<col style="width:15%">
								<col style="width:*">
								<col style="width:15%">
								<col style="width:*">
							</colgroup>
							 <tbody>
								  <tr>
									<th scope="row">단체</th>
									<td>연극집단 반</td>
									<th scope="row">고객문의</th>
									<td>${dto.phone }</td>
								  </tr>
								  <tr>
									<th scope="row">장소</th>
									<td>${dto.place }</td>
									<th scope="row">입장연령</th>
									<td>만 15세이상</td>
								  </tr>
								  <tr>
									<th scope="row">관람시간</th>
									<td colspan="3">100분</td>
								  </tr>
								  <tr>
									<th scope="row">유효기간/이용조건</th>
									<td colspan="3">${dto.startDate } ~ ${dto.endDate } 예매한 공연 날짜, 회차에 한해 이용 가능</td>
								  </tr>
								  <tr>
									<th scope="row">예매취소조건</th>
									<td colspan="3">
										취소일자에 따라서 아래와 같이 취소수수료가 부과됩니다. 예매일 기준보다 관람일 기준이 우선 적용됩니다.<br>
										단, 예매 당일 밤 12시 이전 취소 시에는 취소수수료가 없습니다.
										<table class="s_table mgt15 mgb15">
											<caption>예매취소조건에 관한 상세정보를 보실 수 있습니다.</caption>
											<colgroup>
												<col style="width:300px">
												<col style="width:*">
											</colgroup>
											 <tbody>
												  <tr>
													<th scope="row">미부과기간</th>
													<td>없음</td>
												  </tr>
												  <tr>
													<th scope="row">관람일 9일 전~7일 전</th>
													<td>티켓 금액의 10%</td>
												  </tr>
												  <tr>
													<th scope="row">관람일 6일 전~3일 전</th>
													<td>티켓 금액의 20%</td>
												  </tr>
												  <tr>
													<th scope="row">관람일 2일 전~1일 전</th>
													<td>티켓 금액의 30%</td>
												  </tr>
											 </tbody>
										</table>
									</td>
								  </tr>
								  <tr>
									<th scope="row">취소환불방법</th>
									<td colspan="3">
										'예매/취소 내역'에서 직접 취소할 수 있습니다.<br>
										공연의 특성에 따라 취소 정책이 달라질 수 있습니다.<br>
										각 공연이 공지하는 취소 정책이 우선 적용되므로, 예매 시 상품 안내를 참고하시기 바랍니다.
									</td>
								  </tr>
							 </tbody>
						</table>
					</div>


					<!--관람 후기 -->
					<div class="concert_info" id="concertInfo" style="display: none;">


						

						<!--S: 덧글 -->
						<div class="write_btn_g">
							<p class="fl f_14 pdt20">총 <span class="f_red f_bold">0개</span>의 관람 후기가 있습니다.</p>
						</div>

						<div class="comment_wrap">
							<ul>
								<form id="reviewComment" method="POST">
								<li class="comment_write">
									<div class="lt_left">
										<div class="star_sel">
											<div class="dafault_bg">
												<span class="bar"></span>
												<span>
													<label for="writer" class="tit">별점1점</label>
													<input type="radio" name="star" value="1" title="별점1점">
												</span>
												<span>
													<label for="writer" class="tit">별점2점</label>
													<input type="radio" name="star" value="2" title="별점2점">
												</span>
												<span>
													<label for="writer" class="tit">별점3점</label>
													<input type="radio" name="star" value="3" title="별점3점">
												</span>
												<span>
													<label for="writer" class="tit">별점4점</label>
													<input type="radio" name="star" value="4" title="별점4점">
												</span>
												<span>
													<label for="writer" class="tit">별점5점</label>
													<input type="radio" name="star" value="5" title="별점5점">
												</span>
											</div>
										</div>
										<div class="txt">
											<textarea name="content" id="content" rows="" cols="" class="comm_input wide pda10" placeholder="로그인 후 글쓰기가 가능합니다." style="height:150px;" title="관람후기를 작성해주세요."></textarea>
											<div class="write_btn_g">
												<p class="fl">* 욕설, 비방 등 불건전한 내용이라 판단되는 글은 <a href="javascript:goplaform_rule()" class="f_red un_l">문화N티켓 게시판 운영규정</a>에 의거, 사전 통보 없이 삭제될 수 있습니다</p>
												
												
												<button class="btn btn_middle btn_dark fr" type="button" onclick="Comment('reviewComment');"><span class="txt" id="modeName1">등록</span></button>
												
											</div>

										</div>
									</div>
								</li>
											<input type="hidden" id="type" name="type" value="i">
											<input type="hidden" id="productId" name="productId" value="">
											<input type="hidden" id="id" name="id" value="">
											<input type="hidden" id="parentId" name="parentId" value="">
											<input type="hidden" id="productCd" name="productCd" value="PR000116">
								</form>

								
								
									<li style="text-align: center;">아직 관람후기가 없습니다.</li>
								
							</ul>
						</div>

						<div class="pageing">
							
						</div>
						<!--E: 덧글 -->

					</div>

					<!--기대평 -->
					<div class="concert_info" id="concertInfo" style="display: none;">
						

						<!--S: 덧글 -->
						<div class="write_btn_g">
							<p class="fl f_14 pdt20">총 <span class="f_red f_bold">0개</span>의 기대평이 있습니다.</p>
						</div>

						<div class="comment_wrap write_evaluation">
							<ul>

								<form id="expectationComment" method="POST">
								<li class="comment_write">
									<div class="lt_left">
										<div class="txt">
											<!--div class="writer">
												<label for="writer" class="tit">작성자</label>
												<input type="text" id="writer" placeholder="작성자를 입력하세요." title="작성자 입력" class="comm_input" />
											</div-->
											<textarea id="content" name="content" rows="" cols="" class="comm_input wide pda10" placeholder="로그인 후 글쓰기가 가능합니다." style="height:150px;" title="기대평을 작성해주세요."></textarea>
											<div class="write_btn_g">
												<p class="fl">* 욕설, 비방 등 불건전한 내용이라 판단되는 글은 <a href="javascript:goplaform_rule()" class="f_red un_l">문화N티켓 게시판 운영규정</a>에 의거, 사전 통보 없이 삭제될 수 있습니다</p>
												
												
												<button class="btn btn_middle btn_dark fr" type="button" onclick="Comment('expectationComment');"><span class="txt" id="modeName1">등록</span></button>
												
											</div>
										</div>
									</div>
								</li>
											<input type="hidden" id="type" name="type" value="i">
											<input type="hidden" id="id" name="id" value="">
											<input type="hidden" id="productCd" name="productCd" value="PR000116">
								</form>

								
								
									<li style="text-align: center;">아직 기대평이 없습니다.</li>
								
							</ul>
						</div>

						<div class="pageing">
							
						</div>

						<!--E: 덧글 -->

					</div>

					<!--장소정보 -->
					<div class="concert_info" id="concertInfo" style="display: none;">
						<div class="place_box">
							
							<div class="pic place"><img src="http://www.culture.go.kr/ticket/uploads/facility/20180321/24781ba607974688b564984e6138c685.jpg"></div>
							<ul>
								<li><span class="tit">장소명</span><span class="hidden">:</span>${dto.place }</li>
								<li><span class="tit">주소</span><span class="hidden">:</span>${dto.placeAddr }</li>
								<li><span class="tit">연락처</span><span class="hidden">:</span>${dto.phone }</li>
								<li>
									<span class="tit">홈페이지</span>
									<span class="hidden">:</span>
									<a href="${dto.placeUrl }" target="_blank">${dto.placeUrl }</a>
								</li>
							</ul>
						</div>

						<!--S: 지도 -->
						<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2AnOM27Qm3GD9bu5dHoYyD7_zsR8UoiI&callback=initMap"></script>
						<style>#map {height: 400px; width: 100%; }</style>
						<script>
						function initMap() {
					        //var uluru = {lat: -25.363, lng: 131.044};
					        var gpsY=${dto.gpsY};
					        var gpsX=${dto.gpsX};
					        
					        var uluru = {lat: gpsY, lng: gpsX};
							var map = new google.maps.Map(document.getElementById('map'), {
								zoom: 17,
								center: uluru
							});
							var marker = new google.maps.Marker({
								position: uluru,
								map: map
							});
						}
						</script>
						<div id="map"></div>
					<!-- <div id="map" class="map_box" style="width: 100%; position: relative; overflow: hidden; background: url(&quot;http://i1.daumcdn.net/dmaps/apis/loading_n.png&quot;);">
							<img src="../assets/img/map.jpg" alt="" />
						<div style="position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; cursor: url(&quot;http://i1.daumcdn.net/dmaps/apis/cursor/openhand.cur.ico&quot;) 7 5, url(&quot;http://i1.daumcdn.net/dmaps/apis/cursor/openhand.cur.ico&quot;), default;"><div style="position: absolute;"><div style="position: absolute; z-index: 0;"></div><div style="position: absolute; z-index: 1; left: 0px; top: 0px;"><img src="http://map1.daumcdn.net/map_2d/1803rob/L3/2005/897.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: -48px; top: 230px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map2.daumcdn.net/map_2d/1803rob/L3/2005/898.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 208px; top: 230px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map3.daumcdn.net/map_2d/1803rob/L3/2005/899.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 464px; top: 230px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map0.daumcdn.net/map_2d/1803rob/L3/2005/900.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 720px; top: 230px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map1.daumcdn.net/map_2d/1803rob/L3/2005/901.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 976px; top: 230px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map2.daumcdn.net/map_2d/1803rob/L3/2005/902.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 1232px; top: 230px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map1.daumcdn.net/map_2d/1803rob/L3/2006/897.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: -48px; top: -26px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map2.daumcdn.net/map_2d/1803rob/L3/2006/898.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 208px; top: -26px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map3.daumcdn.net/map_2d/1803rob/L3/2006/899.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 464px; top: -26px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map0.daumcdn.net/map_2d/1803rob/L3/2006/900.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 720px; top: -26px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map1.daumcdn.net/map_2d/1803rob/L3/2006/901.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 976px; top: -26px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map2.daumcdn.net/map_2d/1803rob/L3/2006/902.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 1232px; top: -26px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map1.daumcdn.net/map_2d/1803rob/L3/2007/897.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: -48px; top: -282px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map2.daumcdn.net/map_2d/1803rob/L3/2007/898.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 208px; top: -282px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map3.daumcdn.net/map_2d/1803rob/L3/2007/899.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 464px; top: -282px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map0.daumcdn.net/map_2d/1803rob/L3/2007/900.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 720px; top: -282px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map1.daumcdn.net/map_2d/1803rob/L3/2007/901.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 976px; top: -282px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"><img src="http://map2.daumcdn.net/map_2d/1803rob/L3/2007/902.png" alt="" draggable="false" style="position: absolute; user-select: none; -webkit-user-drag: none; min-width: 0px; min-height: 0px; max-width: none; max-height: none; left: 1232px; top: -282px; opacity: 1; transition-property: opacity; transition-duration: 0.2s; transition-timing-function: ease; width: 256px; height: 256px;"></div><div style="position: absolute; z-index: 1;"></div><div style="position: absolute; z-index: 1; width: 0px; height: 0px;"></div><div style="position: absolute; z-index: 1;"><svg version="1.1" style="stroke: none; stroke-dashoffset: 0.5; stroke-linejoin: round; fill: none; transform: translateZ(0px); position: absolute; left: 0px; top: 0px; width: 0px; height: 0px;" viewBox="0 0 0 0"><defs></defs></svg></div><div style="position: absolute; z-index: 1; width: 100%; height: 0px;"><div style="position: absolute; margin: -39px 0px 0px -13px; z-index: 0; left: 599px; top: 149px; display: none;"><img draggable="false" src="http://t1.daumcdn.net/localimg/localimages/07/mapjsapi/default_marker.png" alt="" style="min-width: 0px; min-height: 0px; max-width: 99999px; max-height: none; border: 0px; display: block; position: absolute; user-select: none; -webkit-user-drag: none; clip: rect(0px, 40px, 42px, 0px); top: 0px; left: 0px; width: 40px; height: 42px;"><img src="http://i1.daumcdn.net/dmaps/apis/transparent.gif" alt="" draggable="false" usemap="#daum.maps.Marker.Area:1" style="min-width: 0px; min-height: 0px; max-width: 99999px; max-height: none; border: 0px; display: block; position: absolute; user-select: none; -webkit-user-drag: none; width: 40px; height: 42px;"><map id="daum.maps.Marker.Area:1" name="daum.maps.Marker.Area:1"><area href="javascript:void(0)" alt="" shape="poly" coords="13,42,9,27,7,23,0,16,0,10,4,4,9,0,17,0,22,4,26,10,26,16,19,23,17,27" title="" style="-webkit-tap-highlight-color: transparent;"></map></div></div></div></div><div style="position: absolute; cursor: default; z-index: 1; white-space: nowrap; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: tahoma, sans-serif; color: rgb(85, 85, 85); left: 7px; bottom: 5px;"><a target="_blank" href="http://map.daum.net/" title="Daum 지도로 보시려면 클릭하세요." style="float: left; width: 38px; height: 17px; cursor: pointer;"><img src="http://t1.daumcdn.net/localimg/localimages/07/mapjsapi/m_bi.png" alt="Daum 지도로 이동" style="width: 37px; height: 18px; border: none;"></a><div style="color: rgb(0, 0, 0); text-align: center; font-size: 10px; margin: 0px 2px; float: left; width: 52px; display: none;"><div style="color: rgb(0, 0, 0);">50m</div><div style="position: relative; overflow: hidden; height: 8px; margin-top: -4px;"><img src="http://t1.daumcdn.net/localimg/localimages/07/mapjsapi/scalebar.png" alt="" style="position: absolute; width: 164px; height: 40px; max-width: none; top: 0px; left: -82px;"></div></div><div style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 11px; line-height: normal; font-family: tahoma, sans-serif; float: left; margin: 3px 2px 0px;">© Kakao<span></span></div></div><div style="cursor: auto; position: absolute; z-index: 2; left: 0px; top: 0px;"></div></div> -->
						<!--E: 지도 -->

						<!--S: 공연장의 다른 공연 -->
						

					</div>

					<!--단체정보 -->
					<div class="concert_info" id="concertInfo" style="display: none;">

						<div class="place_box">
							<div class="pic logo"><img src="http://www.culture.go.kr/ticket/uploads/TA000160/company.jpg" alt="연극집단 반"></div>
							<ul>
								<li><span class="tit">단체명</span><span class="hidden">:</span>연극집단 반</li>
								<li><span class="tit">대표자</span><span class="hidden">:</span>박찬국</li>
								<li><span class="tit">주소</span><span class="hidden">:</span>서울 성북구 보문로30길 46 (동선동2가) 지층 </li>
								<li><span class="tit">연락처</span><span class="hidden">:</span>
									
										070-3339-0188
									
									
								</li>
								<li>
									<span class="tit">홈페이지</span>
									<span class="hidden">:</span>
									
									
									



								</li>
							</ul>
							<a href="/ticket/culture/societyView.do?seq=TA000160" class="ico_btn ico_btn_gray f_normal d_link"><span class="txt ico_r ico_arrow_right">공연단체 이야기 바로가기</span></a>
						</div>

						<!--S: 공연단체의 다른 공연 -->
						

						<!--E: 공연장의 다른 공연 -->

					</div>

					<!--출연정보 -->
					<div class="concert_info" id="concertInfo" style="display: none;">

						<div class="actor_list">
							<ul>
								
								
									<li style="text-align: center;">출연 정보가 없습니다.</li>
								
							</ul>
						</div>
					</div>

					<!--취소/환불 안내 -->
					<div class="concert_info" id="concertInfo" style="display: none;">
						<div class="cancle_info">
						<div class="h2_box">
								<h2>예매 마감 안내</h2>
							</div>
							<ul>
								<li>
									<!-- <h3>예매 마감 안내</h3> -->
									<p>관람일 전일 (평일/주말/공휴일/토요일) 17:00시 예매가 마감됩니다.</p>
								</li>
							</ul>
							<div class="h2_box">
								<h2>예매 취소 안내</h2>
							</div>
							<ul>
								<li>
									<h3>취소 마감 시간</h3>
									<p>관람일 전일 (평일/주말/공휴일/토요일) 17:00시 취소가 마감됩니다.</p>
								</li>
								<li>
									<h3>취소 수수료 안내</h3>
									<table class="board_view_tp02 mgt30 mgb20">
										<caption>취소 수수료에 대한 정보를 받으실 수 있습니다..</caption>
										<colgroup>
											<col style="width:20%">
											<col style="width:*">
										</colgroup>
										 <tbody>
											  <tr>
												<th scope="row">예매 후 ~ 관람일 10일 전</th>
												<td>없음</td>
											  </tr>
											  <tr>
												<th scope="row">관람일 9일 전~7일 전</th>
												<td>티켓 금액의 10%</td>
											  </tr>
											  <tr>
												<th scope="row">관람일 6일 전~3일 전</th>
												<td>티켓 금액의 20%</td>
											  </tr>
											  <tr>
												<th scope="row">관람일 2일 전~1일 전</th>
												<td>티켓 금액의 30%</td>
											  </tr>
										 </tbody>
									</table>

									<ul>
										<li>- 예매 당일 밤 12시 이전 취소 시 취소수수료가 없습니다.</li>
										<li>- 예매 후 9일 이내라도 취소시점이 관람일로부터 9일 이내라면 그에 해당하는 취소수수료가 부과됩니다.</li>
										<li>- 상품의 특성에 따라 취소수수료 정책이 달라질 수 있습니다.(각 상품 예매 시 취소수수료 확인) </li>
									</ul>
								</li>
							</ul>

							<div class="h2_box">
								<h2>환불 방법</h2>
							</div>
							<ul>
								<li>
									<h3>가상계좌로 결제하신 경우</h3>
									<p>
										예매 취소 시에 환불 계좌번호를 남기고, 그 계좌를 통해 취소 수수료를 제외한 금액을 환불 받습니다.<br>
									   단, 예매 후 1개월이 경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 500원을 제외한 금액이 입금됩니다. <br>
									   취소 후 고객님의 계좌로 입금까지 대략 5~7일 정도가 소요됩니다. (주말 제외)
									</p>
								</li>
								<li>
									<h3>실시간 계좌이체로 결제하신 경우</h3>
									<p>
										예매 취소 시 입금하신 계좌정보로 취소 수수료를 제외한 금액을 환불 받습니다.<br>
									   단, 예매 후 1개월이 경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 500원을 제외한 금액이 입금됩니다.<br>
									   취소 후 고객님의 계좌로 입금까지 대략 5~7일 정도가 소요됩니다. (주말 제외)

									</p>
								</li>
								<li>
									<h3>카드로 결제하신 경우</h3>
									<p>
										일반적으로 당사의 취소 처리가 완료되고 3~5일 후 카드사의 취소가 확인됩니다. (체크카드 동일)<br>
									   예매 취소 시점과 해당 카드사의 환불 처리기준에 따라 취소금액의 환급방법과 환급일은 다소 차이가 있을 수 있으며, 예매 취소 시점에 따라 취소 수수료를 제외한 금액을 취소합니다.
									</p>
								</li>
								<li>
									<h3>휴대폰으로 결제하신 경우</h3>
									<p>
										취소 신청 후 바로 취소 처리가 되며 취소 수수료를 제외한 티켓 금액 이용료가 취소 가능합니다.<br>
									   단, 당월 결제에 대한 취소가 아닌 1개월이 경과된 시점에서 취소할 경우 환불 시 취소 수수료 외에 이체 수수료 500원을 제외한 금액이 입금됩니다.
								   </p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<!-- <script type="text/javascript" src="http://apis.daum.net/maps/maps3.js?apikey=37e5ff3b19c5ea22eb4ffcbd0f9a7ba3eb2397c6&amp;libraries=services"></script>
		<script charset="UTF-8" src="http://s1.daumcdn.net/svc/attach/U03/cssjs/mapapi/3.5.22/1511766315938/open.js"></script>
		<script charset="UTF-8" src="http://s1.daumcdn.net/svc/attach/U03/cssjs/mapapi/libs/1.0.4/1502944427572/services.js"></script> -->
		<script type="text/javascript">
			var fnShare = function () {
				var caption = '[문화N티켓] 이혈- 21세기 살인자';
				var desc = '[문화N티켓] 이혈- 21세기 살인자, 이혈-21세기 살인자.';
				//var link = window.location.href;
				var link = $(location).attr('href');
				//var picture = window.location.origin + '/images/portal/layout/logo.png';
				var picture = 'http://www.culture.go.kr/data/assets/images/http://www.culture.go.kr/ticket/uploads/TA000160/product/ae6eab333d5c463583cc8f5783616ed3.jpg|2018_이혈_포스터_문화N티켓 사이즈.jpg';

				caption = $.trim(caption.replace(/\"/g, "＂").replace(/'/g, "＇"));
				desc = $.trim(desc.replace(/\"/g, "＂").replace(/'/g, "＇"));
				link = $.trim(link);
				picture = $.trim(picture);

				return {
					caption	: caption,
					desc : desc,
					link : link,
					picture : picture
				};
			};

			$(function(){
				$(".concert_info").each(function(){
					$(this).hide();
				});

				$('.detail_v_tab ul li a').click(function(e){
					e.preventDefault();
					removeOnClass();
					allHideDiv();
					$(this).addClass("on");
					showDiv($(this).attr("id"));
				});

				$('.ticketStepBtn1').click(function(e){
					var filter = "win16|win32|win64|mac|macintel";
					if ( navigator.platform ) {
						if ( filter.indexOf( navigator.platform.toLowerCase() ) < 0 ) {
							//mobile
							alert('모바일에서는 결제가 되지 않습니다.');
							return;
						}else {
								//pc
							popUp2('ticketing', $('#companyCd').val(), $('#productCd').val(), $('#facilityCd').val());
						}
					}

				});

				//for test
				//$('.ticketStepBtn').click();
				
			});

			function removeOnClass(){
				$('.detail_v_tab ul li a').each(function(){
					$(this).removeClass("on");
				});
			}

			function allHideDiv(){
				$('.concert').hide();
				$(".concert_info").each(function(){
					$(this).hide();
				});
			}

			function showDiv(e){
				if(e == 7){
					$('.concert').show();
				}else{
					$('.concert_info').eq(e).show();
				}
			}

			function goTab(showID) {
				$('.detail_v_tab ul li a').each(function(){
					if ($(this).attr("id") == showID) {
						//e.preventDefault();
						removeOnClass();
						allHideDiv();
						$(this).addClass("on");
						showDiv($(this).attr("id"));
					}
				});

// 				e.preventDefault();
// 				removeOnClass();
// 				allHideDiv();
// 				goAddClass(showID);
// 				showDiv(showID);
// 				//r
			}

			function goAddClass(showID) {
// 				$('.detail_v_tab ul li a').each(function(){
// 					if ($(this).attr("id") == showID) {
// 						$(this).addClass("on");
// 					}
// 				});
			}

			function goplaform_rule() {
				popUp('plaform_rule');
			}

			function popUp2(obj, companyCd, productCd, facilityCd ){
				var $id = obj || $(this).attr('data-id');
				var $url = '../popup/';
				if($id == 'ticketing'){
					$url = '../ticket/';
					$id = 'step01';
				}
				var popWrap = $('<div class="popup_outer">').load($url + $id +'.do?companyCd='+companyCd+'&productCd='+productCd+'&facilityCd='+facilityCd + '&randomId='+(new Date()).getTime(), function(){
					$('body').append(popWrap).find('.popup_outer').find('.popup').popUp();
				});

				$(this).addClass('pop_open');
			}


		</script>

			</div>
			<!--E: container -->
	
			<!--S: footer -->
			

<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>