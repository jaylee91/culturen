<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../layout/inc_top.jsp"/>

<!--E: Top -->
					
			
<!-- 바디 부분  -->			
<!--S: container -->
<div class="container">

<div class="container_body">
	<div class="content_wrap" id="contents">
		<form name="searchForm" method="get">
		<div class="board_search_wrap sort_wrap">
			<div class="fl">
				<span class="tit">장르</span>
				<a href="/culturen/concert_list.action" class="btn btn_red "><span class="txt">전체</span></a>
				<a href="/culturen/concert_list.action?genre=10" class="btn  "><span class="txt">연극</span></a>
				<a href="/culturen/concert_list.action?genre=20" class="btn  "><span class="txt">뮤지컬</span></a>
				<a href="/culturen/concert_list.action?genre=30" class="btn  "><span class="txt">콘서트</span></a>
				<a href="/culturen/concert_list.action?genre=40" class="btn  "><span class="txt">클래식/무용</span></a>
				<a href="/culturen/concert_list.action?genre=50" class="btn  "><span class="txt">아동/가족</span></a>
			</div>
			<div class="fr">
				<label for="area" class="tit">지역</label>
				<select id="area" class="comm_input input_m mgr40" style="width: 100px;" name="area">
					<option value="">전체</option>
					<option value="01">서울</option><option value="02">경기</option><option value="03">강원</option><option value="04">경상</option><option value="05">전라</option><option value="06">충청</option><option value="07">제주</option>
				</select>
				<script type="text/javascript">
				$().ready(function(){
					$('#area option').each(function(){
						if($(this).val() == ""){
							$(this).prop("selected", true);
						}
					});
				});
				</script>
				<label for="turn" class="tit">순서</label>
				<select id="orderby" name="orderby" title="순서구분" style="width: 100px;" class="comm_input input_m">
					<option value="regDate" selected="">최신순</option>
					<option value="endDate">종료임박순</option>
					<option value="title">제목</option>
					<option value="review">후기</option>
				</select>
			</div>
			<input type="hidden" name="genre" value="${genre }">
		</div>
		</form>

		<div class="concert_list">
			<ul>
			<!-- 반복문으로 데이터 뿌려주는 부분 -->
			<c:if test="${totalCount!=0 }">
			<c:forEach var="dto" items="${lists }">
				<li>
					<a href="/culturen/concert_view.action?seq=${dto.seq }">
						<div class="pic">
							<img src="${dto.thumbnail }">
							<ul>
								<li class="sale">할인율</li>
								<li class="ticket">예매</li>
							</ul>
						</div>
						<p class="txt">${dto.title }</p>
					</a>
				</li>
			</c:forEach>
			</c:if>
			
			<c:if test="${totalCount==0 }">
				진행중인 공연이 없습니다.
			</c:if>
			</ul>
		</div>

		<div class="ac">
			<button class="list_more"><span class="txt">더보기</span></button>
		</div>

	</div>



	<script type="text/javascript">
		var pageNo = 1;
		$(function(){

			$('#area').change(function(){
				this.form.submit();
			});
			$('#orderby').change(function(){
				this.form.submit();
			});
			$(".list_more").click(function(){
				getItems();
				return false;
			});
		});

		function getItems( ){
			var total = ${totalCount};
			var url = "/culturen/concert_list_ajax.action?pageNo="+ (++pageNo) + "&genre=" + $('input[name="genre"]').val()+ "&area=" + $('#area').val() ;
			$.ajax({
				type: "GET",
				url: url,
				dataType: "html",
				success: function (data) {
					$(".concert_list > ul").append($.trim(data));

					var datarows = $('.concert_list > ul > li').length;
					if(datarows >= total)
						$(".list_more").hide();
				},
				error: function (err) {
					alert(err);
					console.log(err);
				}
			});
		}
	</script>
</div>

			</div>
			<!--E: container -->
	
	
	
<!-- 푸터 시작 -->	
<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>			
