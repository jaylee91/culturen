<%@ page contentType="text/html; charset=UTF-8"%>


<jsp:include page="../layout/inc_top.jsp"/>


<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				

			<!--S: lnb -->
			<div class="lnb abs_menu">
				<div class="abs_menu_body">
					<ul class="sm03">
						<li class="selected"><a href="/culturen/storyList.action">공연제작 이야기</a></li>
						<li><a href="/culturen/actorList.action">문화인물 이야기</a></li>
						<li><a href="/culturen/societyList.action">문화단체 이야기</a></li>
						<li><a href="/culturen/tourList.action">문화관광 이야기</a></li>
					</ul>
				</div>
			</div>
			<!--E: lnb -->


			<div class="container_body">
				<div class="content_wrap" id="contents">
					<div class="title_wrap">
						<h2>공연제작 이야기</h2>
						<p>하나의 공연이 무대에 올라가기 까지의 과정을 이야기 합니다.</p>
					</div>
					<form method="GET">
					<div class="board_search_wrap pdb0">
						<div class="fr">
							<label for="FIELD" class="hidden">검색구분</label>
							<select name="category" id="FIELD" title="검색구분" style="width: 100px;" class="comm_input scr_elem">
								<option value="0" selected="">전체</option>
								<option value="3">공연명</option>
								<option value="1">제목</option>
								<option value="2">내용</option>
							</select>
							<label for="KEY" class="hidden">검색어</label>
							<input type="text" placeholder="검색어를 입력하세요." name="keyword" value="" title="검색어 입력" class="comm_input scr_elem">
							<button class="btn btn_search scr_elem">검색</button>
						</div>
					</div>
					</form>
					
					<p class="mgb10 f_bold">총 1 건</p>
					<div class="thum_list">
						<ul>
							
							<li>
								<a href="storyView.do?seq=12&amp;">
									
									<div class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/7bb6d1cf566c49e0ac3eb3695d492f6b.png" style="height:145px;" alt="아동/가족 <콘서트 포 키즈 어린이를 위한 콘서트>"></div>
									<div class="con_box">
										<p class="subject">아동/가족 &lt;콘서트 포 키즈 어린이를 위한 콘서트&gt;</p>
										<p class="tit">[예시] 어린이를 위한 겨울 콘서트</p>
										<p class="txt">즐겁고 경쾌한 캐롤 음악과 동요를 중심으로 들을 수 있는 어린이 콘서트 '콘서트 포 키즈'는 기타, 타악기로 구성된 콘서트입니다. 또한 가수들이 배우로 함께 나와 즐겁고 재미있는 캐롤 음악을 함께 부르며, 새로운 스토리를 만들어가는 이야기로 어린이들이 흥미와 집중도를 높일 수 있도록 연출·제작 하였습니다. 이처럼 겨...</p>
									</div>
								</a>
							</li>
							
						</ul>
					</div>
					
					<div class="pageing">
						
							<a href="?pageNo=1" class="first"><span>첫 페이지 이동</span></a><span><strong title="현재 페이지">1</strong></span><a href="?pageNo=1" class="last"><span>마지막 페이지 이동</span></a>
						
					</div>

				</div>
			</div>
			</div>
			<!--E: container -->
	
			<!--S: footer -->
			




<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>	