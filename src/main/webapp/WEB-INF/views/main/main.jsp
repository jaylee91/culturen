<%@ page contentType="text/html; charset=UTF-8"%>

<jsp:include page="../layout/inc_top.jsp"/>

<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<title>문화 N 티켓</title>
		

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<link href="/culturen/resources/css/import.css" rel="stylesheet">
<link href="/culturen/resources/css/layout.css" rel="stylesheet">

<link href="/ticket/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="http://cr.acecounter.com/Web/AceCounter_AW.js?gc=AH4A42094072613&amp;py=0&amp;gd=gtb14&amp;gp=8080&amp;up=NaPm_Ncisy&amp;rd=1522631238917"></script>
<script src="/culturen/resources/js/AceCounter_AW422IVGRA.js"></script>

<script src="/ticket/assets/js/jquery.min.js"></script>기존 것
<script src="/culturen/resources/js/jquery-1.11.3.min.js"></script>

<script src="/culturen/resources/js/jquery-ui.js"></script>

<script src="/ticket/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/culturen/resources/js/popup.js"></script>
<script src="/culturen/resources/js/common.js"></script>
		
[if lt IE 9]>
	<script src="/ticket/assets/js/html5shiv.js"></script>
	<script src="/ticket/assets/js/respond.min.js"></script>
<![endif]
		<link href="/culturen/resources/css/main.css" rel="stylesheet">
		<link href="/culturen/resources/css/jquery.bxslider.css" rel="stylesheet">
		<script src="/culturen/resources/js/jquery.bxslider.js"></script>
	</head>
	<body class="main">
		<div id="skipNavi">
			<ul>
				<li><a href="#contents">본문 바로가기</a></li>
				<li><a href="#gnb">주메뉴 바로가기</a></li>
			</ul>
		</div>

		<div id="wrap">
S: Top
			


<div class="header_wrap">
	<header>
		<h1><a href="/ticket/"><img src="../../layout/images/common/logo.png" alt="문화N티켓"></a></h1>
		<h1><a href="/ticket/"><img src="/culturen/resources/img/common/logo.png" alt="문화N티켓"></a></h1>
		<div class="t_group">
			<ul>
			
				<li><a href="#login_pop" class="lay_pop" data-id="login_pop2" id="login-pop-link2">로그인</a></li>
				<li><a href="https://www.culture.go.kr/member/join.action" target="_blank" title="새창">회원가입</a></li>
			
				<li><a href="/ticket/mypage/cancle_list.action">예매/취소내역</a></li>
				<li><a href="/ticket/useInfo/guide.action">예매 가이드</a></li>
				<li><a href="/ticket/about/introduction.action">문화N티켓 소개</a></li>
				<li><a href="http://www.culture.go.kr/ticketadmin" target="_blank">참여단체 로그인</a></li>
			</ul>
		</div>
		<div id="gnb">
			<ul>
				<li class=""><a href="/culturen/concert_list.action">공연</a></li>
				<li class=""><a href="/culturen/exhibit_list.action">전시</a></li>
				<li class=""><a href="/culturen/storyList.action">문화이야기</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/culturen/storyList.action">공연제작 이야기</a></li>
						<li><a href="/culturen/actorList.action">문화인물 이야기</a></li>
						<li><a href="/culturen/societyList.action">문화단체 이야기</a></li>
						<li><a href="/culturen/tourList.action">문화관광 이야기</a></li>
					</ul>
				</li>
				<li class=""><a href="/ticket/event/ingList.action">이벤트</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/ticket/event/ingList.action">진행중 이벤트</a></li>
						<li><a href="/ticket/event/endList.action">지난 이벤트</a></li>
						<li><a href="/ticket/event/winList.action">당첨자 확인</a></li>
					</ul>
				</li>
				<li class=""><a href="/ticket/useInfo/guide.action">이용안내</a>
					<ul style="height: 0px; padding-top: 0px;">
						<li><a href="/ticket/useInfo/guide.action">예매 가이드</a></li>
						<li><a href="/ticket/useInfo/faq.action">FAQ</a></li>
						<li><a href="/ticket/useInfo/ticketInfo.action">티켓판매 안내</a></li>
						<li><a href="/ticket/useInfo/notice.action">공지사항</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="allmenu">
			<a href="#" class="all">전체메뉴<span>열기</span></a>
			<div class="all_list">
				<ul>
					<li class="dep01"><a href="/ticket/concert/concert_list.action">공연</a></li>
					<li class="dep01"><a href="/ticket/exhibit/exhibit_list.action">전시</a></li>
					<li class="dep01"><a href="/ticket/culture/storyList.action">문화이야기</a>
						<ul>
							<li><a href="/ticket/culture/storyList.action">공연제작 이야기</a></li>
							<li><a href="/ticket/culture/actorList.action">문화인물 이야기</a></li>
							<li><a href="/ticket/culture/societyList.action">문화단체 이야기</a></li>
							<li><a href="/ticket/culture/tourList.action">문화관광 이야기</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/event/ingList.action">이벤트</a>
						<ul>
							<li><a href="/ticket/event/ingList.action">진행중 이벤트</a></li>
							<li><a href="/ticket/event/endList.action">지난 이벤트</a></li>
							<li><a href="/ticket/event/winList.action">당첨자 확인</a></li>
						</ul>
					</li>
					<li class="dep01"><a href="/ticket/useInfo/guide.action">이용안내</a>
						<ul>
							<li><a href="/ticket/useInfo/guide.action">예매 가이드</a></li>
							<li><a href="/ticket/useInfo/faq.action">FAQ</a></li>
							<li><a href="/ticket/useInfo/ticketInfo.action">티켓판매 안내</a></li>
							<li><a href="/ticket/useInfo/notice.action">공지사항</a></li>
						</ul>
					</li>
					
					<li class="dep01"><a href="/ticket/about/introduction.action">문화N티켓 소개</a>
						<ul>
							
							<li><a href="#" class="f_red lay_pop" data-id="login_pop2">로그인</a></li>
							<li><a href="https://www.culture.go.kr/member/join.action" class="f_red" target="_blank" title="새창">회원가입</a></li>
							
							<li><a href="/ticket/polc/policy.action">개인정보처리방침</a></li>
							<li><a href="/ticket/polc/term.action">이용약관</a></li>
							<li><a href="/ticket/polc/copy.action">저작권정책</a></li>
							<li class="end"><a href="/ticket/polc/collect.action">검색결과수집정책</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<form action="/ticket/search/search.action">
		<div class="t_search_wrap">
			<label for="keyword" class="hidden">통합검색</label>
			<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력하세요." class="comm_input" title="검색어 입력">
			<input type="image" class="btn_search" src="/culturen/resources/img/common/ico_search.png" alt="검색">
		</div>
		</form>
	</header>
	<div class="gnb_bg" style="height: 0px;"></div>
	<a href="#" data-id="login_pop" class="lay_pop" id="login-pop-link" style="display:none">로그인</a>
	<a href="#" data-id="login_pop3" class="lay_pop" id="login-pop-link3" style="display:none">로그인</a>
</div> -->
<!-- //inc_top -->





<script>
	$(function(){
		var loggedIn = '';
		if( loggedIn == 'false' ){
			$("#returnURL").val('');
			//alert("returnURL = " + $("#returnURL").val());
			$("#login-pop-link2").click();
		}
	});
</script>
                
			<!--E: Top -->
			
			<!--S: container -->
			<div class="container">
				
 
  <link href="/culturen/resources/css/jquery.bxslider.css" rel="stylesheet"> 
  <script src="/culturen/resources/js/jquery.bxslider.js"></script> 
  <script>
	$(document).ready(function(){

		// 프로모션
		var promotionSlide = $('.promotion_item').bxSlider({
			mode: 'fade',
			auto: true,
			autoControls: true,
			pause: 5000,
			speed:1000,
			nextText: '다음',
			prevText: '이전',
			autoControlsCombine:true,
			nextSelector: '.promo_next',
			prevSelector: '.promo_prev',
			autoControlsSelector:'.promo_stop',
			startText:'재생',
			stopText:'정지',
			onSliderLoad:function(){
				console.log('21212');
			}
		});

		$(document).on('click','.bx-next, .bx-prev, .bx-pager-link',function() {
			if(!$('.promo_stop').hasClass('on')){
				promotionSlide.stopAuto();
				promotionSlide.startAuto();
			}
		});

		$(document).on('click','.bx-stop',function() {
			var $this = $(this);
			if($('.promo_stop').hasClass('on')){
				$('.promo_stop').removeClass('on');
			}else{
				$('.promo_stop').addClass('on');
			}

			console.log("1111")
		});

		var concertSlide = $('.concert_list').bxSlider({
			slideWidth: 200,
			minSlides: 2,
			maxSlides: 5,
			moveSlides: 1,
			slideMargin: 20,
			pager:false,
			captions: true,
			nextText: '다음',
			prevText: '이전'
		});

		var concert01_html = $('.concert01').html();
		var concert02_html = $('.concert02').html();
		var concert03_html = $('.concert03').html();
		$('.concert_list').append(concert01_html);
		concertSlide.reloadSlider();


		//새로운공연, 후기많은공연 탭메뉴
		$(document).on('click' ,'.tab_tp01 > ul > li > button', function(e){
			var $this = $(this);
			var $idx = $(this).parent().index();
			$this.parent().siblings().removeClass('selected');
			$this.parent().addClass('selected');
			if($this.next().get(0).className != 'list'){
				$('.concert_list_outer').empty();
				var $firstConcert = $('.concert_list_wrap ul li:eq(' + $idx + ') .concert_list_outer').append('<ul class="concert_list">');
					switch ($idx) {
					case 0:
						$firstConcert.find('.concert_list').append(concert01_html);
						break;
					case 1:
						$firstConcert.find('.concert_list').append(concert02_html);
						break;
					case 2:
						$firstConcert.find('.concert_list').append(concert03_html);
						break;
				}
				$('.concert_list').bxSlider({
					slideWidth: 200,
					minSlides: 2,
					maxSlides: 5,
					moveSlides: 1,
					slideMargin: 20,
					pager:false,
					captions: true,
					nextText: '다음',
					prevText: '이전'
				});
			}
			e.preventDefault();
		});

		//이벤트
		$(document).on('click' ,'.ev_list > li > button', function(e){
			var $this = $(this);
			$this.parent().siblings().removeClass('selected');
			$this.parent().addClass('selected');
			e.preventDefault();
		});

		//이벤트 상세
		$(document).on('mouseenter' ,'.invite_gift ul a', function(){
			var $this = $(this);
			$this.parent().siblings().removeClass('on');
			$this.parent().addClass('on');
		});


		//공지사항
		var notiCounter = 1;
		var notiHeight = 20;
		var flag = true;
		var speed = 3000;
		var direction = 'next';
		var $target = $('.notice_wrap .list');
		var timer = setInterval(noticeSlide,speed);

		function noticeSlide(){
			$('.notice_wrap .control button').attr('disabled','disabled');
			if(direction == 'next'){
				$target.animate({top:-notiHeight},function(){
					$(this).find('li:eq(0)').insertAfter($(this).find('li:eq(3)'));
					$target.css('top','0');
					$('.notice_wrap .control button').removeAttr('disabled','disabled');

				});
			}else{
				$target.find('li:eq(3)').insertBefore($target.find('li:eq(0)'));
				$target.css('top','-20px');
				$target.animate({top:'0px'},function(){
					$target.css('top','0px');
					$('.notice_wrap .control button').removeAttr('disabled','disabled');
					direction = 'next';
				});
			}
		}

		$(document).on({
			mouseenter:function(){
				clearInterval(timer);
			},
			mouseleave:function(){
				if(flag){
					timer = setInterval(noticeSlide,speed);
				}
			}
		},'.notice_wrap');

		$(document).on('click' ,'.notice_wrap .control button', function(e){
			var $this = $(this);
			var $name = $this.get(0).className;
			switch ($name) {
				case 'stop':
					$this.addClass('play');
					$this.removeClass('stop');
					flag = false;
					clearInterval(timer);
					break;
				case 'play':
					$this.removeClass('play');
					$this.addClass('stop');
					clearInterval(timer);
					flag = true;
					break;
				case 'next':
					direction = 'next';
					clearInterval(timer);
					noticeSlide();
					break;
				case 'prev':
					direction = 'prev';
					clearInterval(timer);
					noticeSlide();
					break;
			}

			e.preventDefault();
		});

		//5대 지역 문화관광
		$(document).on('click' ,'.area_tourism .control button', function(e){
			var $this = $(this);
			var $target = $('.area_list');
			var $idx = $target.find('.selected').index();
			var $size = $target.find('li').size()-1;
			var $name = $this.get(0).className;
			var $color = ['#ffe63c'/*,'#3c98ff','#4641d4','#85a688','#ff7800'*/];

			if($idx >= $size){
				console.log(222);
			}else if($idx <= 0){
				console.log(444);
			}

			switch ($name) {
				case 'next':
					if($idx == $size){
						$idx = -1;
					}
					$target.find('li').removeClass('selected');
					$target.find('li:eq(' + ($idx+1) + ')').addClass('selected');
					$('.section02 .ly_right_bg').css('background',$color[$idx+1]);
					console.log($idx+1);
					break;
				case 'prev':
					$target.find('li').removeClass('selected');
					$target.find('li:eq(' + ($idx-1) + ')').addClass('selected');
					$('.section02 .ly_right_bg').css('background',$color[$idx-1]);
					console.log($idx-1);
					break;
			}
			e.preventDefault();
		});

		//프로모션 열고 닫기
		$(document).on('click' ,'.showHide_wrap button', function(e){
			var $this = $(this);
			if($this.hasClass('promo_open')){
				$this.removeClass('promo_open');
				$('.promotion').css('height','420px');
				$('.promotion .bx-wrapper, .promotion .control').css('display','block');
			}else{
				$this.addClass('promo_open');
				$('.promotion').css('height','0');
				$('.promotion .bx-wrapper, .promotion .control').css('display','none');
			}
			e.preventDefault();
		});

		//공연리스트 더보기
		moreList('show_list li:eq(0) .list', 5);
		$(document).on('click' ,'#show_list > ul > li > button', function(e){ //공연 상단텝
			var $this = $(this);
			var $target = $this.parent();
			var $idx = $target.index();
			var $wrap = $this.closest('.con_box');

			$wrap.css('height','500px');
			$('#show_list > ul > li:eq(' + $idx + ')').find('li').removeClass('active');
			moreList('show_list > ul > li:eq(' + $idx + ') .list', 5);
			e.preventDefault();
		});

		$(document).on('click' ,'#show_list .more', function(e){ // 공연더보기 버튼
			var $this = $(this);
			var $target = $this.parent();
			var $idx = $this.closest('.selected').index();
			var $wrap = $this.closest('.con_box');
			var $wrapHeight = $this.closest('.con_box').outerHeight();

			moreList('show_list > ul > li:eq(' + $idx + ') .list', 5);
			$wrap.css('height',$wrapHeight + 290);
			e.preventDefault();
		});

		function moreList(className, num){
			var $showList = $('#' + className).find('li').not('.active');
			var $showList_length = $showList.length;
			if(num >= $showList_length){
				num = $showList_length;
				$('#' + className).find('.more').hide();

			}else{
				$('#' + className).find('.more').show();
			}
			$('#' + className).find('li:not(".active"):lt(' + num + ')').addClass('active');

		}

	});
	</script> 
  <!-- 팝업 --> 
  <script language="Javascript">
  //function checkDayPopup(){
	  cookiedata = document.cookie;
  var dt = new Date();
  var year  = dt.getFullYear() ;
  var month = ""+ (dt.getMonth() + 1);
  var day   = ""+ dt.getDate();

  if(month.length == 1) month = "0"+month;
  if (day.length==1) day = "0"+day;
   var today = ""+year + month + day;
  //document.write("오늘날짜 : " + today + "<br>");
  var targetDay = "20180219";

if(today < targetDay){
	//alert("오늘이 작다 : " + today + "<br>");

	if ( cookiedata.indexOf("maindiv=done") < 0 ){
		document.all['divpop'].style.visibility = "visible";
		}
		else {
			document.all['divpop'].style.visibility = "hidden";
	}
}else{
	//alert(" 오늘이 크다 : " + today + "<br>");
	;

}
//}
		//팝업창
		function setCookie( name, value, expiredays ) {
			var todayDate = new Date();
				todayDate.setDate( todayDate.getDate() + expiredays );
				document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
			}

		function closeWin() {
			if ( document.notice_form.chkbox.checked ){
				setCookie( "maindiv", "done" , 1 );
			}
			document.all['divpop'].style.visibility = "hidden";
		}
	</script> 
  <!-- //팝업 --> 
 
 
<!-- //inc_top --> 
 
 
 
 
 
<!-- 바디 부분  --> 
  <div class="promotion"> 
   <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 420px;"><ul class="promotion_item" style="width: auto; position: relative;"> 
    <li style="float: none; list-style: none; position: absolute; width: 1261px; z-index: 0; display: none;"> 
     <div class="item_wrap" style="background:#292b38 url('/culturen/resources/img/main/promotion_01.jpg') no-repeat center top;"> 
      <div class="item"> 
       <div class="item_con txt_black"> 
        <p class="s_tit">국악앙상블</p> 
        <p class="tit">몽실, 떠오르다</p> 
        <p class="txt">서울문화재단 최초 예술지원 선정작품</p> 
       </div> 
       <a href="http://www.culture.go.kr/ticket/concert/concert_view.action?productCd=PR000108&amp;companyCd=TA000159&amp;facilityCd=0999" class="btn_reservation">예매하기</a> 
       <!-- <a href="http://www.culture.go.kr/ticket/event/open/eventView.action" class="btn_reservation">참여하기</a>  --> 
       <!-- div class="btn_reservation">2월 중 티켓 OPEN</div--> 
       <!-- a href="#" class="btn_reservation">예매하기</a--> 
       <div class="sub_txt"> 
        <p class="tit">국악앙상블 몽실, 떠오르다</p> 
        <p class="txt">서울문화재단 최초 예술지원 선정작품</p> 
       </div> 
      </div> 
      <div class="btm_txt_bg"></div> 
     </div> </li> 
    <li style="float: none; list-style: none; position: absolute; width: 1261px; z-index: 0; display: none;"> 
     <div class="item_wrap" style="background:#e7e3eb url('/culturen/resources/img/main/promotion_02.jpg') no-repeat center top;"> 
      <div class="item"> 
       <div class="item_con txt_black"> 
        <p class="s_tit"> 창작무용공연</p> 
        <p class="tit">상상력</p> 
        <p class="txt">국립무용단 간판스타 장현수 무용가가 <br> 표현하는 그리스 로마 신화</p> 
       </div> 
       <a href="http://www.culture.go.kr/ticket/concert/concert_view.action?productCd=PR000093&amp;companyCd=TA000028&amp;facilityCd=1320" class="btn_reservation">예매하기</a> 
       <!-- <a href="http://www.culture.go.kr/ticket/event/open/eventView.action" class="btn_reservation">참여하기</a>  --> 
       <!-- div class="btn_reservation">2월 중 티켓 OPEN</div--> 
       <!--<a href="#" class="btn_reservation">예매하기</a>--> 
       <div class="sub_txt"> 
        <p class="tit">창작무용공연 상상력</p> 
        <p class="txt">국립무용단 간판스타 장현수 무용가가 표현하는 그리스 로마 신화</p> 
       </div> 
      </div> 
      <div class="btm_txt_bg"></div> 
     </div> </li> 
    <li style="float: none; list-style: none; position: absolute; width: 1261px; z-index: 0; display: none;"> 
     <div class="item_wrap" style="background:#7c0000 url('/culturen/resources/img/main/promotion_03.jpg') no-repeat center top;"> 
      <div class="item"> 
       <div class="item_con"> 
        <p class="s_tit">2018 경북연극제 참가자 </p> 
        <p class="tit">템프파일</p> 
        <p class="txt">죽음뒤에 숨겨진 진실을 알리고자 하는 자와 <br> 진실을 은페할 수 밖에 없는 두인간의 치열한 싸움</p> 
       </div> 
       <a href="http://www.culture.go.kr/ticket/concert/concert_view.action?productCd=PR000103&amp;companyCd=TA000158&amp;facilityCd=2569" class="btn_reservation">예매하기</a> 
       <!-- <a href="http://www.culture.go.kr/ticket/event/open/eventView.action" class="btn_reservation">참여하기</a> --> 
       <div class="sub_txt"> 
        <p class="tit">2018 경북연극제 참가자 템프파일 </p> 
        <p class="txt">죽음뒤에 숨겨진 진실을 알리고자 하는 자와 진실을 은페할 수 밖에 없는 두인간의 치열한 싸움</p> 
       </div> 
      </div> 
      <div class="btm_txt_bg"></div> 
     </div> </li> 
    <li style="float: none; list-style: none; position: absolute; width: 1261px; z-index: 50; display: list-item;"> 
     <div class="item_wrap" style="background:#f3eae3 url('/culturen/resources/img/main/promotion_04.jpg') no-repeat center top;"> 
      <div class="item"> 
       <div class="item_con txt_black"> 
        <p class="s_tit">옴니버스 연극</p> 
        <p class="tit02">연극 순간에서 <br><span style="font-size:24px">:일곱개의 시선</span></p> 
        <p class="txt">우리와 가까우면서도 깊이 있는, 다양한 연령대와 남녀노소 모두 공감 할 수 있는 이야기</p> 
       </div> 
       <a href="http://www.culture.go.kr/ticket/concert/concert_view.action?productCd=PR000085&amp;companyCd=TA000125&amp;facilityCd=1541" class="btn_reservation">예매하기</a> 
       <!-- <a href="http://www.culture.go.kr/ticket/event/open/eventView.action" class="btn_reservation">참여하기</a>  --> 
       <!--div class="btn_reservation">2월 중 티켓 OPEN</div--> 
       <div class="sub_txt"> 
        <p class="tit">연극 순간에서: 일곱개의 시선</p> 
        <p class="txt">우리와 가까우면서도 깊이 있는, 다양한 연령대와 남녀노소 모두 공감 할 수 있는 이야기</p> 
       </div> 
      </div> 
      <div class="btm_txt_bg"></div> 
     </div> </li> 
   </ul></div><div class="bx-controls bx-has-pager"><div class="bx-pager bx-default-pager"><div class="bx-pager-item"><a href="" data-slide-index="0" class="bx-pager-link">1</a></div><div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link">2</a></div><div class="bx-pager-item"><a href="" data-slide-index="2" class="bx-pager-link">3</a></div><div class="bx-pager-item"><a href="" data-slide-index="3" class="bx-pager-link active">4</a></div></div></div></div> 
   <ul class="control"> 
    <li><span class="promo_prev"><a class="bx-prev" href="">이전</a></span></li> 
    <li><span class="promo_stop"><div class="bx-controls-auto"><div class="bx-controls-auto-item"><a class="bx-stop" href="">정지</a></div></div></span></li> 
    <li><span class="promo_next"><a class="bx-next" href="">다음</a></span></li> 
   </ul> 
   <div class="showHide_wrap"> 
    <p><button>닫힘</button></p> 
   </div> 
  </div> 
  <!--// 프로모션 --> 
  <!-- 새로운공연, 후기많은공연 --> 
  <div class="section01"> 
   <div class="tab_tp01 concert_list_wrap"> 
    <ul> 
     <li class="selected"><button>새로운 공연</button> 
      <div class="concert_list_outer"> 
       <div class="bx-wrapper" style="max-width: 1080px; margin: 0px auto;"><div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 270px;"><ul class="concert_list" style="width: 715%; position: relative; transition-duration: 0s; transform: translate3d(-1100px, 0px, 0px);"><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000118&amp;facilityCd=2452&amp;companyCd=TA000135"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/aadc416930c84da78befa663cd873782.jpg" title="공동전시 <강원서예의 전통과 계승>" alt="공동전시 <강원서예의 전통과 계승>"> </a> <div class="bx-caption"><span>공동전시 &lt;강원서예의 전통과 계승&gt;</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/concert/concert_view.action?productCd=PR000116&amp;facilityCd=FA004815&amp;companyCd=TA000160"> <img src="http://www.culture.go.kr/ticket/uploads/TA000160/product/ae6eab333d5c463583cc8f5783616ed3.jpg" title="이혈- 21세기 살인자" alt="이혈- 21세기 살인자"> </a> <div class="bx-caption"><span>이혈- 21세기 살인자</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000109&amp;facilityCd=1172&amp;companyCd=TA000142"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/bac89b08a7384f0ea2f6e84de193534b.jpg" title="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기" alt="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기"> </a> <div class="bx-caption"><span>문화채널마포 수요예술포럼 문화비젼 2030 톺아보기</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/concert/concert_view.action?productCd=PR000108&amp;facilityCd=0999&amp;companyCd=TA000159"> <img src="http://www.culture.go.kr/ticket/uploads/TA000159/product/bb528978ef384e53880742a23ec17cae.jpg" title="국악앙상블 몽실의 첫번째 프로젝트" alt="국악앙상블 몽실의 첫번째 프로젝트"> </a> <div class="bx-caption"><span>국악앙상블 몽실의 첫번째 프로젝트</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000106&amp;facilityCd=FA004814&amp;companyCd=TA000157"> <img src="http://www.culture.go.kr/ticket/uploads/TA000157/product/30cdf5f17d9c432d99984e4a87814e1f.jpg" title="불혹, 미혹하다" alt="불혹, 미혹하다"> </a> <div class="bx-caption"><span>불혹, 미혹하다</span></div></li> 
    <li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000118&amp;facilityCd=2452&amp;companyCd=TA000135"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/aadc416930c84da78befa663cd873782.jpg" title="공동전시 <강원서예의 전통과 계승>" alt="공동전시 <강원서예의 전통과 계승>"> </a> <div class="bx-caption"><span>공동전시 &lt;강원서예의 전통과 계승&gt;</span></div></li> 
    <li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;"> <a href="/ticket/concert/concert_view.action?productCd=PR000116&amp;facilityCd=FA004815&amp;companyCd=TA000160"> <img src="http://www.culture.go.kr/ticket/uploads/TA000160/product/ae6eab333d5c463583cc8f5783616ed3.jpg" title="이혈- 21세기 살인자" alt="이혈- 21세기 살인자"> </a> <div class="bx-caption"><span>이혈- 21세기 살인자</span></div></li> 
    <li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000109&amp;facilityCd=1172&amp;companyCd=TA000142"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/bac89b08a7384f0ea2f6e84de193534b.jpg" title="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기" alt="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기"> </a> <div class="bx-caption"><span>문화채널마포 수요예술포럼 문화비젼 2030 톺아보기</span></div></li> 
    <li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;"> <a href="/ticket/concert/concert_view.action?productCd=PR000108&amp;facilityCd=0999&amp;companyCd=TA000159"> <img src="http://www.culture.go.kr/ticket/uploads/TA000159/product/bb528978ef384e53880742a23ec17cae.jpg" title="국악앙상블 몽실의 첫번째 프로젝트" alt="국악앙상블 몽실의 첫번째 프로젝트"> </a> <div class="bx-caption"><span>국악앙상블 몽실의 첫번째 프로젝트</span></div></li> 
    <li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000106&amp;facilityCd=FA004814&amp;companyCd=TA000157"> <img src="http://www.culture.go.kr/ticket/uploads/TA000157/product/30cdf5f17d9c432d99984e4a87814e1f.jpg" title="불혹, 미혹하다" alt="불혹, 미혹하다"> </a> <div class="bx-caption"><span>불혹, 미혹하다</span></div></li> 
   <li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000118&amp;facilityCd=2452&amp;companyCd=TA000135"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/aadc416930c84da78befa663cd873782.jpg" title="공동전시 <강원서예의 전통과 계승>" alt="공동전시 <강원서예의 전통과 계승>"> </a> <div class="bx-caption"><span>공동전시 &lt;강원서예의 전통과 계승&gt;</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/concert/concert_view.action?productCd=PR000116&amp;facilityCd=FA004815&amp;companyCd=TA000160"> <img src="http://www.culture.go.kr/ticket/uploads/TA000160/product/ae6eab333d5c463583cc8f5783616ed3.jpg" title="이혈- 21세기 살인자" alt="이혈- 21세기 살인자"> </a> <div class="bx-caption"><span>이혈- 21세기 살인자</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000109&amp;facilityCd=1172&amp;companyCd=TA000142"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/bac89b08a7384f0ea2f6e84de193534b.jpg" title="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기" alt="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기"> </a> <div class="bx-caption"><span>문화채널마포 수요예술포럼 문화비젼 2030 톺아보기</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/concert/concert_view.action?productCd=PR000108&amp;facilityCd=0999&amp;companyCd=TA000159"> <img src="http://www.culture.go.kr/ticket/uploads/TA000159/product/bb528978ef384e53880742a23ec17cae.jpg" title="국악앙상블 몽실의 첫번째 프로젝트" alt="국악앙상블 몽실의 첫번째 프로젝트"> </a> <div class="bx-caption"><span>국악앙상블 몽실의 첫번째 프로젝트</span></div></li><li style="float: left; list-style: none; position: relative; width: 200px; margin-right: 20px;" class="bx-clone"> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000106&amp;facilityCd=FA004814&amp;companyCd=TA000157"> <img src="http://www.culture.go.kr/ticket/uploads/TA000157/product/30cdf5f17d9c432d99984e4a87814e1f.jpg" title="불혹, 미혹하다" alt="불혹, 미혹하다"> </a> <div class="bx-caption"><span>불혹, 미혹하다</span></div></li></ul></div><div class="bx-controls bx-has-controls-direction"><div class="bx-controls-direction"><a class="bx-prev" href="">이전</a><a class="bx-next" href="">다음</a></div></div></div> 
      </div> </li> 
     <li><button>후기 많은 공연</button> 
      <div class="concert_list_outer"> 
       <!--ul class="concert_list"></ul--> 
      </div> </li> 
     <li><button>관심 많은 공연</button> 
      <div class="concert_list_outer"> 
       <!--ul class="concert_list"></ul--> 
      </div> </li> 
    </ul> 
   </div> 
   <!-- 새로운공연, 후기많은공연등 리스트를 불러들이는 부분 --> 
   <ul class="concert01 dp_n"> 
    <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000118&amp;facilityCd=2452&amp;companyCd=TA000135"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/aadc416930c84da78befa663cd873782.jpg" title="공동전시 <강원서예의 전통과 계승>" alt="공동전시 <강원서예의 전통과 계승>"> </a> </li> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000116&amp;facilityCd=FA004815&amp;companyCd=TA000160"> <img src="http://www.culture.go.kr/ticket/uploads/TA000160/product/ae6eab333d5c463583cc8f5783616ed3.jpg" title="이혈- 21세기 살인자" alt="이혈- 21세기 살인자"> </a> </li> 
    <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000109&amp;facilityCd=1172&amp;companyCd=TA000142"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/bac89b08a7384f0ea2f6e84de193534b.jpg" title="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기" alt="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기"> </a> </li> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000108&amp;facilityCd=0999&amp;companyCd=TA000159"> <img src="http://www.culture.go.kr/ticket/uploads/TA000159/product/bb528978ef384e53880742a23ec17cae.jpg" title="국악앙상블 몽실의 첫번째 프로젝트" alt="국악앙상블 몽실의 첫번째 프로젝트"> </a> </li> 
    <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000106&amp;facilityCd=FA004814&amp;companyCd=TA000157"> <img src="http://www.culture.go.kr/ticket/uploads/TA000157/product/30cdf5f17d9c432d99984e4a87814e1f.jpg" title="불혹, 미혹하다" alt="불혹, 미혹하다"> </a> </li> 
   </ul> 
   <ul class="concert02 dp_n"> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000028&amp;facilityCd=0005&amp;companyCd=TA000003"> <img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/7bb6d1cf566c49e0ac3eb3695d492f6b.png" title="콘서트 포 키즈 어린이를 위한 콘서트" alt="콘서트 포 키즈 어린이를 위한 콘서트"> </a> </li> 
    <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000036&amp;facilityCd=2056&amp;companyCd=TA000014"> <img src="/ticket/uploads//TA000014/product/b8c9e67f74024988af478ce9ea02cc4c.gif" title="(이벤트 전시) 찰리와 초콜릿 공장 원화 작가, 퀀틴 ...." alt="(이벤트 전시) 찰리와 초콜릿 공장 원화 작가, 퀀틴 ...."> </a> </li> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000040&amp;facilityCd=3475&amp;companyCd=TA000015"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/5e393c2ccd5c44688ca1ea79a5749baa.jpg" title="(이벤트 공연) 관객과의 전쟁" alt="(이벤트 공연) 관객과의 전쟁"> </a> </li> 
    <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000049&amp;facilityCd=3665&amp;companyCd=TA000024"> <img src="http://www.culture.go.kr/ticket/uploads/TA000024/product/2cb3e127cb6e4efda88b4e29fed0845f.jpg" title="성남의얼굴展 <성남을걷다>" alt="성남의얼굴展 <성남을걷다>"> </a> </li> 
    <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000052&amp;facilityCd=1352&amp;companyCd=TA000027"> <img src="http://www.culture.go.kr/ticket/uploads/TA000027/product/cf6fdf7dafd84b57acf65a793ad3479b.jpg" title="로맨틱 춘천 페스티벌 2018" alt="로맨틱 춘천 페스티벌 2018"> </a> </li> 
   </ul> 
   <ul class="concert03 dp_n"> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000043&amp;facilityCd=1899&amp;companyCd=TA000018"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/e97f7462ef484e8f94eb2106c89a1595.jpg" title="(이벤트 공연) 산울림 고전극장_오셀로의 식탁" alt="(이벤트 공연) 산울림 고전극장_오셀로의 식탁"> </a> </li> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000040&amp;facilityCd=3475&amp;companyCd=TA000015"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/5e393c2ccd5c44688ca1ea79a5749baa.jpg" title="(이벤트 공연) 관객과의 전쟁" alt="(이벤트 공연) 관객과의 전쟁"> </a> </li> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000047&amp;facilityCd=1899&amp;companyCd=TA000018"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/fcbeb893b25843079197d322b7c7c042.jpg" title="(이벤트 공연) 산울림 고전극장_줄리엣과 줄리엣" alt="(이벤트 공연) 산울림 고전극장_줄리엣과 줄리엣"> </a> </li> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000055&amp;facilityCd=2158&amp;companyCd=TA000014"> <img src="http://www.culture.go.kr/ticket/uploads/TA000014/product/aa856647ece24f8aa4f1ea761769dddf.jpg" title="(이벤트공연) 지금은 웬수가 된 너이지만 [ 그 리 워 라 ]" alt="(이벤트공연) 지금은 웬수가 된 너이지만 [ 그 리 워 라 ]"> </a> </li> 
    <li> <a href="/ticket/concert/concert_view.action?productCd=PR000057&amp;facilityCd=2158&amp;companyCd=TA000019"> <img src="http://www.culture.go.kr/ticket/uploads/TA000019/product/97ad6905eb7648acbef08d7f8a2ccca7.jpg" title="(이벤트 공연) 제34회 라이브 클럽 데이" alt="(이벤트 공연) 제34회 라이브 클럽 데이"> </a> </li> 
   </ul> 
  </div> 
  <!--// 새로운공연, 후기많은공연 --> 
  <!-- 이벤트, 5대지역관광 --> 
  <div class="section02"> 
   <div class="con_box"> 
    <div class="con_left"> 
     <div class="con_wrap"> 
      <h2>이벤트</h2> 
      <ul class="ev_list"> 
       <li class="invite_gift selected"><button>지난이벤트</button> 
        <ul> 
         <li class=""> <a href="#con_wrap"><p class="photo"><img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/fa396616e0a44279b6ca37fc0ea98214.jpg" title="문화N티켓 오픈기념 이벤트" alt="문화N티켓 오픈기념 이벤트"><span class="ico_sale">초대<strong>263</strong>분</span></p></a> <a href="#con_wrap" class="link"><span class="tit">문화N티켓 오픈기념 이벤트</span> <span class="date">기간 : 2018-01-06~2018-01-22</span></a> </li> 
         <li class="on"> <a href="#con_wrap"><p class="photo"><img src="http://www.culture.go.kr/ticket/uploads/TA000014/product/aa856647ece24f8aa4f1ea761769dddf.jpg" title="[오픈이벤트] 롱디(LONG:D) 앨벌판매 콘서트 " alt="[오픈이벤트] 롱디(LONG:D) 앨벌판매 콘서트 "><span class="ico_sale">초대<strong>25</strong>분</span></p></a> <a href="#con_wrap" class="link"><span class="tit">[오픈이벤트] 롱디(LONG:D) 앨벌판매 콘서트 </span> <span class="date">기간 : 2018-01-06~2018-01-22</span></a> </li> 
         <li> <a href="#con_wrap"><p class="photo"><img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/e97f7462ef484e8f94eb2106c89a1595.jpg" title="[오픈이벤트] 산울림 고전극장" alt="[오픈이벤트] 산울림 고전극장"><span class="ico_sale">초대<strong>18</strong>분</span></p></a> <a href="#con_wrap" class="link"><span class="tit">[오픈이벤트] 산울림 고전극장</span> <span class="date">기간 : 2018-01-06~2018-01-22</span></a> </li> 
         <li> <a href="#con_wrap"><p class="photo"><img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/5e393c2ccd5c44688ca1ea79a5749baa.jpg" title="[오픈이벤트] 관객과의 전쟁" alt="[오픈이벤트] 관객과의 전쟁"><span class="ico_sale">초대<strong>25</strong>분</span></p></a> <a href="#con_wrap" class="link"><span class="tit">[오픈이벤트] 관객과의 전쟁</span> <span class="date">기간 : 2018-01-06~2018-01-22</span></a> </li> 
        </ul> <a href="/ticket/event/endList.action" class="more">더보기</a> </li> 
      </ul> 
     </div> 
    </div> 
    <div class="con_right area_tourism"> 
     <h2><span>문화관광</span> 이야기</h2> 
     <ul class="area_list"> 
      <li class="selected"> 
	      <a href="/ticket/culture/tourList.action"> 
	      <img src="/culturen/resources/img/main/area_img_01.jpg" title="홍대" alt="홍대"> </a> 
	      <a href="/ticket/culture/tourList.action" class="more">더보기</a> 
      </li> 
      <li> <a href="/ticket/culture/tourList.action"> <img src="/ticket/assets/img/main/area_img_02.jpg" title="인천" alt="인천"> </a> <a href="/ticket/culture/tourList.action" class="more">더보기</a> </li> 
      <li> <a href="/ticket/culture/tourList.action"> <img src="/ticket/assets/img/main/area_img_01.jpg" title="명동" alt="명동"> </a> <a href="/ticket/culture/tourList.action" class="more">더보기</a> </li> 
      <li> <a href="/ticket/culture/tourList.action"> <img src="/ticket/assets/img/main/area_img_02.jpg" title="인천" alt="인천"> </a> <a href="/ticket/culture/tourList.action" class="more">더보기</a> </li> 
      <li> <a href="/ticket/culture/tourList.action"> <img src="/ticket/assets/img/main/area_img_01.jpg" title="명동" alt="명동"> </a> <a href="/ticket/culture/tourList.action" class="more">더보기</a> </li> 
     </ul> 
     <ul class="control"> 
      <li><button class="prev">이전 지역</button></li> 
      <li><button class="next">다음 지역</button></li> 
     </ul> 
    </div> 
   </div> 
   <div class="ly_right_bg"></div> 
  </div> 
  <!--// 이벤트, 5대지역관광 --> 
  <!-- 오늘의 공연단체, 오늘의 배우 --> 
  <div class="section03"> 
   <div class="con_box"> 
    <div class="con_left"> 
     <div class="con_wrap"> 
      <div class="con_b"> 
       <div class="con_b_header"> 
        <h2>오늘의 <span>공연단체</span></h2> 
        <div class="group_name"> 
         <span class="logo"><img src="http://www.culture.go.kr/ticket/uploads/TA000160/company.jpg" alt="연극집단 반"></span> 
         <span>연극집단 반</span> 
        </div> 
       </div> 
       <ul> 
        <li> <a href="/ticket/culture/societyView.action?seq=TA000160"> <p class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000160/company.jpg" alt="연극집단 반"></p> <p>연극집단 반</p> </a> </li> 
        <li> <a href="/ticket/culture/societyView.action?seq=TA000159"> <p class="pic"><img src="http://www.culture.go.kr/ticket/uploads/TA000159/company.jpg" alt="몽실"></p> <p>몽실</p> </a> </li> 
       </ul> 
       <a href="/ticket/culture/societyList.action" class="more">더보기</a> 
      </div> 
      <div class="con_b"> 
       <div class="con_b_header actor_name"> 
        <h2>오늘의 <span>배우</span></h2> 
        <p><span>뮤지컬 배우</span>권민경</p> 
        <div class="photo">
         <img src="http://www.culture.go.kr/ticket/uploads/actors/20180221/2a783bd7e6fc471c81c52e0b6c265035.jpg" alt="권민경">
        </div> 
       </div> 
       <ul> 
        <li> <a href="/ticket/culture/actorView.action?seq=AC000090"> <p class="pic"><img src="/culturen/resources/img/poster_noimg.gif" alt="이규용"></p> <p>이규용<br>뮤지컬 배우</p> </a> </li> 
        <li> <a href="/ticket/culture/actorView.action?seq=AC000089"> <p class="pic"><img src="/culturen/resources/img/poster_noimg.gif" alt="배윤희"></p> <p>배윤희<br>뮤지컬 배우</p> </a> </li> 
       </ul> 
       <a href="/ticket/culture/actorList.action" class="more">더보기</a> 
      </div> 
     </div> 
    </div> 
    <div class="con_right"> 
     <h2><span>공연제작</span> 이야기</h2> 
     <div class="con_kind"> 
      <span>아동/가족</span>콘서트 포 키즈 어린이를 위한 콘서트 
     </div> 
     <a href="/ticket/culture/storyView.action?seq=12"> <p class="subject">[예시] 어린이를 위한 겨울 콘서트</p> <p class="txt"> 즐겁고 경쾌한 캐롤 음악과 동요를 중심으로 들을 수 있는 어린이 콘서트 '콘서트 포 키즈'는 기타, 타악기로 구성된 콘서트입니다. 또한 가수들이 배우로 함께 나와 즐겁고 재미있는 캐롤 음악을 함께 부르며, 새로운 스토리를 만들어가는 이야기로 어린이들이 흥미와 집중도를 높일 수 있도록 연출·제작 하였습니다. 이처럼 겨울이야기로 가득할 ... </p> 
      <!-- 							<ul> --> 
      <!-- 							</ul> --> </a> 
     <a href="/ticket/culture/storyList.action" class="more">더보기</a> 
    </div> 
   </div> 
   <div class="ly_right_bg"></div> 
  </div> 
  <!--// 오늘의 공연단체, 오늘의 배우 --> 
 
 <%-- 
  <!-- 공연리스트 --> 
  <div class="section04"> 
   <div class="con_box"> 
    <div class="tab_tp01" id="show_list"> 
     <ul> 
      <li class="selected"><button>전체</button> 
       <div class="list"> 
        <ul> 
         <li class="active"> <a href="/ticket/concert/concert_view.action?productCd=PR000100&amp;companyCd=TA000153&amp;facilityCd=2887"> <img src="http://www.culture.go.kr/ticket/uploads/TA000153/product/226a01fa76344d83a6776e84234b4bfe.jpg" alt="모럴패밀리"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li class="active"> <a href="/ticket/concert/concert_view.action?productCd=PR000066&amp;companyCd=TA000130&amp;facilityCd=2670"> <img src="http://www.culture.go.kr/ticket/uploads/TA000130/product/40fe9058e573439aa341d2fd740f96f0.jpg" alt="트러블메이트"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li class="active"> <a href="/ticket/concert/concert_view.action?productCd=PR000061&amp;companyCd=TA000100&amp;facilityCd=1930"> <img src="http://www.culture.go.kr/ticket/uploads/TA000100/product/d05bacadb86848b0b9fad65dfbaf4632.png" alt="조각 : 사라진 기억"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li class="active"> <a href="/ticket/concert/concert_view.action?productCd=PR000076&amp;companyCd=TA000135&amp;facilityCd=2452"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/938cbcc9644f4e079244f8126333895c.jpg" alt="2018 평창 동계올림픽 기념 특집전 " 한국문화="" 속="" 곰""=""> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li class="active"> <a href="/ticket/concert/concert_view.action?productCd=PR000105&amp;companyCd=TA000156&amp;facilityCd=2277"> <img src="http://www.culture.go.kr/ticket/uploads/TA000156/product/5d691b43d05c4f7a98efa12811bf2283.jpg" alt="2018 희망 콘서트"> 
           <div class="lb_info"> 
            <span class="sale">20%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000108&amp;companyCd=TA000159&amp;facilityCd=0999"> <img src="http://www.culture.go.kr/ticket/uploads/TA000159/product/bb528978ef384e53880742a23ec17cae.jpg" alt="국악앙상블 몽실의 첫번째 프로젝트"> 
           <div class="lb_info"> 
            <span class="age">20대</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000053&amp;companyCd=TA000030&amp;facilityCd=2659"> <img src="http://www.culture.go.kr/ticket/uploads/TA000030/product/baaf32a7013c421dbdd629fec9409a66.jpg" alt="지음[知音, 짓다] - 시간의 흔적, 미래로 펼치다"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000077&amp;companyCd=TA000143&amp;facilityCd=1319"> <img src="http://www.culture.go.kr/ticket/uploads/TA000143/product/1adfcafb3874415a8290a62db7e24478.png" alt="플로우"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000048&amp;companyCd=TA000011&amp;facilityCd=0150"> <img src="http://www.culture.go.kr/ticket/uploads/TA000011/product/ac45390c02ac4453aa36681f29240101.gif" alt="뮤지컬 〈마흔 즈음에〉 - 김광석을 노래하다_ 부산"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000044&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/efaede3af97d4dd3a9912db6a46d587d.jpg" alt="(이벤트 공연) 산울림 고전극장_소네트"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000062&amp;companyCd=TA000100&amp;facilityCd=2670"> <img src="http://www.culture.go.kr/ticket/uploads/TA000100/product/9269a14bb284464483d3ace9abf7fb22.jpg" alt="안나라수마나라"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000073&amp;companyCd=TA000133&amp;facilityCd=1588"> <img src="http://www.culture.go.kr/ticket/uploads/TA000133/product/da1cbed2434c49078c69be837386e0fe.jpg" alt="조은하 피아노연주회"> 
           <div class="lb_info"> 
            <span class="age">40대</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000055&amp;companyCd=TA000014&amp;facilityCd=2158"> <img src="http://www.culture.go.kr/ticket/uploads/TA000014/product/aa856647ece24f8aa4f1ea761769dddf.jpg" alt="(이벤트공연) 지금은 웬수가 된 너이지만 [ 그 리 워 라 ]"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000103&amp;companyCd=TA000158&amp;facilityCd=2569"> <img src="http://www.culture.go.kr/ticket/uploads/TA000158/product/74866d1427b348a68be9ae30a8fab5a0.jpg" alt="연극 <템프 파일>"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000116&amp;companyCd=TA000160&amp;facilityCd=FA004815"> <img src="http://www.culture.go.kr/ticket/uploads/TA000160/product/ae6eab333d5c463583cc8f5783616ed3.jpg" alt="이혈- 21세기 살인자"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000036&amp;companyCd=TA000014&amp;facilityCd=2056"> <img src="/ticket/uploads//TA000014/product/b8c9e67f74024988af478ce9ea02cc4c.gif" alt="(이벤트 전시) 찰리와 초콜릿 공장 원화 작가, 퀀틴 ...."> 
           <div class="lb_info"> 
            <span class="age">20대</span> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000028&amp;companyCd=TA000003&amp;facilityCd=0005"> <img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/7bb6d1cf566c49e0ac3eb3695d492f6b.png" alt="콘서트 포 키즈 어린이를 위한 콘서트"> 
           <div class="lb_info"> 
            <span class="age">20대</span> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000047&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/fcbeb893b25843079197d322b7c7c042.jpg" alt="(이벤트 공연) 산울림 고전극장_줄리엣과 줄리엣"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000057&amp;companyCd=TA000019&amp;facilityCd=2158"> <img src="http://www.culture.go.kr/ticket/uploads/TA000019/product/97ad6905eb7648acbef08d7f8a2ccca7.jpg" alt="(이벤트 공연) 제34회 라이브 클럽 데이"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000045&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/41364e3fe89c41ec92c8f691482dbe80.jpg" alt="(이벤트 공연) 산울림 고전극장_5필리어"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000072&amp;companyCd=TA000117&amp;facilityCd=0987"> <img src="http://www.culture.go.kr/ticket/uploads/TA000117/product/4f484aec96f446f7a939b9a320207b27.jpg" alt="알렉산더 지라드"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000050&amp;companyCd=TA000025&amp;facilityCd=0175"> <img src="http://www.culture.go.kr/ticket/uploads/TA000025/product/2c35b1efac354d7b84e3f023b3630969.jpg" alt="밀양"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000085&amp;companyCd=TA000125&amp;facilityCd=1541"> <img src="http://www.culture.go.kr/ticket/uploads/TA000125/product/66b1d774c5f346f69ba6d0bdd90dd335.jpg" alt="연극 <순간에서 : 일곱 개의 시선>"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000087&amp;companyCd=TA000147&amp;facilityCd=1714"> <img src="http://www.culture.go.kr/ticket/uploads/TA000147/product/ef7356abad75468ca16f60facbcffd12.jpg" alt="마르떼 경남교육뮤지컬단 두번째 정기공연 뮤지컬 " 연기"...."=""> 
           <div class="lb_info"> 
            <span class="age">30대</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000118&amp;companyCd=TA000135&amp;facilityCd=2452"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/aadc416930c84da78befa663cd873782.jpg" alt="공동전시 <강원서예의 전통과 계승>"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000046&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/b66cc7f2b0074865a53e031a894ea4c7.jpg" alt="(이벤트 공연) 산울림 고전극장_멈추고, 생각하고, 햄릿"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000040&amp;companyCd=TA000015&amp;facilityCd=3475"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/5e393c2ccd5c44688ca1ea79a5749baa.jpg" alt="(이벤트 공연) 관객과의 전쟁"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000079&amp;companyCd=TA000145&amp;facilityCd=2619"> <img src="http://www.culture.go.kr/ticket/uploads/TA000145/product/1c4812f4711b4e82b741da167402e0c2.jpg" alt="말 그리고 얼굴"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000093&amp;companyCd=TA000028&amp;facilityCd=1320"> <img src="http://www.culture.go.kr/ticket/uploads/TA000028/product/c8d6fc0568f940a6900d35b137f7f5a5.jpg" alt="상상력"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000070&amp;companyCd=TA000130&amp;facilityCd=1627"> <img src="http://www.culture.go.kr/ticket/uploads/TA000130/product/f1d64bce4fce41c7946a39beed37e605.gif" alt="감성멜로연극 <하루>"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000042&amp;companyCd=TA000015&amp;facilityCd=3475"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/7c313181594e40e195514917de00edd7.jpg" alt="(이벤트 공연) 쇼미더퍼니"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000065&amp;companyCd=TA000041&amp;facilityCd=1709"> <img src="http://www.culture.go.kr/ticket/uploads/TA000041/product/c0c512b34d5f40b385d58df9cde0e7f7.gif" alt="쇼뮤지컬 꽃보다 슈퍼스타"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000075&amp;companyCd=TA000142&amp;facilityCd=1172"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/67c95d26b4ad494fb356812e6192eb12.jpg" alt="문화채널마포 수요예술포럼"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000049&amp;companyCd=TA000024&amp;facilityCd=3665"> <img src="http://www.culture.go.kr/ticket/uploads/TA000024/product/2cb3e127cb6e4efda88b4e29fed0845f.jpg" alt="성남의얼굴展 <성남을걷다>"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000043&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/e97f7462ef484e8f94eb2106c89a1595.jpg" alt="(이벤트 공연) 산울림 고전극장_오셀로의 식탁"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000051&amp;companyCd=TA000026&amp;facilityCd=2554"> <img src="http://www.culture.go.kr/ticket/uploads/TA000026/product/6d4dce387de74099a2298a15efb34ca5.jpg" alt="2017 서울 포커스 [25.7]"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000041&amp;companyCd=TA000015&amp;facilityCd=3475"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/bce4cb481b4946dcbe7e8e5e432ea998.jpg" alt="(이벤트 공연) 홍콩쇼"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000063&amp;companyCd=TA000100&amp;facilityCd=1930"> <img src="http://www.culture.go.kr/ticket/uploads/TA000100/product/81cc0d3576254fc0b393b2f8df9125bb.jpg" alt="술래잡기"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000106&amp;companyCd=TA000157&amp;facilityCd=FA004814"> <img src="http://www.culture.go.kr/ticket/uploads/TA000157/product/30cdf5f17d9c432d99984e4a87814e1f.jpg" alt="불혹, 미혹하다"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000109&amp;companyCd=TA000142&amp;facilityCd=1172"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/bac89b08a7384f0ea2f6e84de193534b.jpg" alt="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000054&amp;companyCd=TA000031&amp;facilityCd=2212"> <img src="http://www.culture.go.kr/ticket/uploads/TA000031/product/53401af19e9246f398747598acdbc66a.jpg" alt="기획전 <해양 명품 100선, 바다를 품다>&nbsp;"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000052&amp;companyCd=TA000027&amp;facilityCd=1352"> <img src="http://www.culture.go.kr/ticket/uploads/TA000027/product/cf6fdf7dafd84b57acf65a793ad3479b.jpg" alt="로맨틱 춘천 페스티벌 2018"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000067&amp;companyCd=TA000130&amp;facilityCd=2670"> <img src="http://www.culture.go.kr/ticket/uploads/TA000130/product/b814b730caea4ca2a3dc1a2e64f8b672.jpg" alt="다함께다방구"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000096&amp;companyCd=TA000147&amp;facilityCd=FA004813"> <img src="http://www.culture.go.kr/ticket/uploads/TA000147/product/619dd391b9c64d9fba84dbc05119452e.jpg" alt="마르떼 무용단 제1회 정기공연 " hans="" &="" dans"...."=""> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000091&amp;companyCd=TA000134&amp;facilityCd=4622"> <img src="http://www.culture.go.kr/ticket/uploads/TA000134/product/bb291c7170654959b5c8c3a4298bad4f.jpg" alt="서울국제핸드메이드페어 2018"> 
           <div class="lb_info"> 
            <span class="sale">40%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
      <li><button>뮤지컬</button> 
       <div class="list"> 
        <ul> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000087&amp;companyCd=TA000147&amp;facilityCd=1714"> <img src="http://www.culture.go.kr/ticket/uploads/TA000147/product/ef7356abad75468ca16f60facbcffd12.jpg" alt="마르떼 경남교육뮤지컬단 두번째 정기공연 뮤지컬 " 연기"...."=""> 
           <div class="lb_info"> 
            <span class="age">30대</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000065&amp;companyCd=TA000041&amp;facilityCd=1709"> <img src="http://www.culture.go.kr/ticket/uploads/TA000041/product/c0c512b34d5f40b385d58df9cde0e7f7.gif" alt="쇼뮤지컬 꽃보다 슈퍼스타"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000048&amp;companyCd=TA000011&amp;facilityCd=0150"> <img src="http://www.culture.go.kr/ticket/uploads/TA000011/product/ac45390c02ac4453aa36681f29240101.gif" alt="뮤지컬 〈마흔 즈음에〉 - 김광석을 노래하다_ 부산"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
      <li><button>콘서트</button> 
       <div class="list"> 
        <ul> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000108&amp;companyCd=TA000159&amp;facilityCd=0999"> <img src="http://www.culture.go.kr/ticket/uploads/TA000159/product/bb528978ef384e53880742a23ec17cae.jpg" alt="국악앙상블 몽실의 첫번째 프로젝트"> 
           <div class="lb_info"> 
            <span class="age">20대</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000105&amp;companyCd=TA000156&amp;facilityCd=2277"> <img src="http://www.culture.go.kr/ticket/uploads/TA000156/product/5d691b43d05c4f7a98efa12811bf2283.jpg" alt="2018 희망 콘서트"> 
           <div class="lb_info"> 
            <span class="sale">20%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000073&amp;companyCd=TA000133&amp;facilityCd=1588"> <img src="http://www.culture.go.kr/ticket/uploads/TA000133/product/da1cbed2434c49078c69be837386e0fe.jpg" alt="조은하 피아노연주회"> 
           <div class="lb_info"> 
            <span class="age">40대</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000057&amp;companyCd=TA000019&amp;facilityCd=2158"> <img src="http://www.culture.go.kr/ticket/uploads/TA000019/product/97ad6905eb7648acbef08d7f8a2ccca7.jpg" alt="(이벤트 공연) 제34회 라이브 클럽 데이"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000055&amp;companyCd=TA000014&amp;facilityCd=2158"> <img src="http://www.culture.go.kr/ticket/uploads/TA000014/product/aa856647ece24f8aa4f1ea761769dddf.jpg" alt="(이벤트공연) 지금은 웬수가 된 너이지만 [ 그 리 워 라 ]"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
      <li><button>연극</button> 
       <div class="list"> 
        <ul> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000116&amp;companyCd=TA000160&amp;facilityCd=FA004815"> <img src="http://www.culture.go.kr/ticket/uploads/TA000160/product/ae6eab333d5c463583cc8f5783616ed3.jpg" alt="이혈- 21세기 살인자"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000103&amp;companyCd=TA000158&amp;facilityCd=2569"> <img src="http://www.culture.go.kr/ticket/uploads/TA000158/product/74866d1427b348a68be9ae30a8fab5a0.jpg" alt="연극 <템프 파일>"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000100&amp;companyCd=TA000153&amp;facilityCd=2887"> <img src="http://www.culture.go.kr/ticket/uploads/TA000153/product/226a01fa76344d83a6776e84234b4bfe.jpg" alt="모럴패밀리"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000085&amp;companyCd=TA000125&amp;facilityCd=1541"> <img src="http://www.culture.go.kr/ticket/uploads/TA000125/product/66b1d774c5f346f69ba6d0bdd90dd335.jpg" alt="연극 <순간에서 : 일곱 개의 시선>"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000079&amp;companyCd=TA000145&amp;facilityCd=2619"> <img src="http://www.culture.go.kr/ticket/uploads/TA000145/product/1c4812f4711b4e82b741da167402e0c2.jpg" alt="말 그리고 얼굴"> 
           <div class="lb_info"> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000077&amp;companyCd=TA000143&amp;facilityCd=1319"> <img src="http://www.culture.go.kr/ticket/uploads/TA000143/product/1adfcafb3874415a8290a62db7e24478.png" alt="플로우"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000070&amp;companyCd=TA000130&amp;facilityCd=1627"> <img src="http://www.culture.go.kr/ticket/uploads/TA000130/product/f1d64bce4fce41c7946a39beed37e605.gif" alt="감성멜로연극 <하루>"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000067&amp;companyCd=TA000130&amp;facilityCd=2670"> <img src="http://www.culture.go.kr/ticket/uploads/TA000130/product/b814b730caea4ca2a3dc1a2e64f8b672.jpg" alt="다함께다방구"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000066&amp;companyCd=TA000130&amp;facilityCd=2670"> <img src="http://www.culture.go.kr/ticket/uploads/TA000130/product/40fe9058e573439aa341d2fd740f96f0.jpg" alt="트러블메이트"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000063&amp;companyCd=TA000100&amp;facilityCd=1930"> <img src="http://www.culture.go.kr/ticket/uploads/TA000100/product/81cc0d3576254fc0b393b2f8df9125bb.jpg" alt="술래잡기"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000062&amp;companyCd=TA000100&amp;facilityCd=2670"> <img src="http://www.culture.go.kr/ticket/uploads/TA000100/product/9269a14bb284464483d3ace9abf7fb22.jpg" alt="안나라수마나라"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000061&amp;companyCd=TA000100&amp;facilityCd=1930"> <img src="http://www.culture.go.kr/ticket/uploads/TA000100/product/d05bacadb86848b0b9fad65dfbaf4632.png" alt="조각 : 사라진 기억"> 
           <div class="lb_info"> 
            <span class="sale">66%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000047&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/fcbeb893b25843079197d322b7c7c042.jpg" alt="(이벤트 공연) 산울림 고전극장_줄리엣과 줄리엣"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000046&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/b66cc7f2b0074865a53e031a894ea4c7.jpg" alt="(이벤트 공연) 산울림 고전극장_멈추고, 생각하고, 햄릿"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000045&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/41364e3fe89c41ec92c8f691482dbe80.jpg" alt="(이벤트 공연) 산울림 고전극장_5필리어"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000044&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/efaede3af97d4dd3a9912db6a46d587d.jpg" alt="(이벤트 공연) 산울림 고전극장_소네트"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000043&amp;companyCd=TA000018&amp;facilityCd=1899"> <img src="http://www.culture.go.kr/ticket/uploads/TA000018/product/e97f7462ef484e8f94eb2106c89a1595.jpg" alt="(이벤트 공연) 산울림 고전극장_오셀로의 식탁"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000042&amp;companyCd=TA000015&amp;facilityCd=3475"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/7c313181594e40e195514917de00edd7.jpg" alt="(이벤트 공연) 쇼미더퍼니"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000041&amp;companyCd=TA000015&amp;facilityCd=3475"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/bce4cb481b4946dcbe7e8e5e432ea998.jpg" alt="(이벤트 공연) 홍콩쇼"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000040&amp;companyCd=TA000015&amp;facilityCd=3475"> <img src="http://www.culture.go.kr/ticket/uploads/TA000015/product/5e393c2ccd5c44688ca1ea79a5749baa.jpg" alt="(이벤트 공연) 관객과의 전쟁"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
      <li><button>클래식/무용</button> 
       <div class="list"> 
        <ul> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000096&amp;companyCd=TA000147&amp;facilityCd=FA004813"> <img src="http://www.culture.go.kr/ticket/uploads/TA000147/product/619dd391b9c64d9fba84dbc05119452e.jpg" alt="마르떼 무용단 제1회 정기공연 " hans="" &="" dans"...."=""> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000093&amp;companyCd=TA000028&amp;facilityCd=1320"> <img src="http://www.culture.go.kr/ticket/uploads/TA000028/product/c8d6fc0568f940a6900d35b137f7f5a5.jpg" alt="상상력"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
      <li><button>아동/가족</button> 
       <div class="list"> 
        <ul> 
         <li> <a href="/ticket/concert/concert_view.action?productCd=PR000028&amp;companyCd=TA000003&amp;facilityCd=0005"> <img src="http://www.culture.go.kr/ticket/uploads/TA000003/product/7bb6d1cf566c49e0ac3eb3695d492f6b.png" alt="콘서트 포 키즈 어린이를 위한 콘서트"> 
           <div class="lb_info"> 
            <span class="age">20대</span> 
            <span class="sale">50%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
      <li><button>전시</button> 
       <div class="list"> 
        <ul> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000118&amp;companyCd=TA000135&amp;facilityCd=2452"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/aadc416930c84da78befa663cd873782.jpg" alt="공동전시 <강원서예의 전통과 계승>"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000106&amp;companyCd=TA000157&amp;facilityCd=FA004814"> <img src="http://www.culture.go.kr/ticket/uploads/TA000157/product/30cdf5f17d9c432d99984e4a87814e1f.jpg" alt="불혹, 미혹하다"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000091&amp;companyCd=TA000134&amp;facilityCd=4622"> <img src="http://www.culture.go.kr/ticket/uploads/TA000134/product/bb291c7170654959b5c8c3a4298bad4f.jpg" alt="서울국제핸드메이드페어 2018"> 
           <div class="lb_info"> 
            <span class="sale">40%</span> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000076&amp;companyCd=TA000135&amp;facilityCd=2452"> <img src="http://www.culture.go.kr/ticket/uploads/TA000135/product/938cbcc9644f4e079244f8126333895c.jpg" alt="2018 평창 동계올림픽 기념 특집전 " 한국문화="" 속="" 곰""=""> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000072&amp;companyCd=TA000117&amp;facilityCd=0987"> <img src="http://www.culture.go.kr/ticket/uploads/TA000117/product/4f484aec96f446f7a939b9a320207b27.jpg" alt="알렉산더 지라드"> 
           <div class="lb_info"> 
            <span class="ticket">예매</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000054&amp;companyCd=TA000031&amp;facilityCd=2212"> <img src="http://www.culture.go.kr/ticket/uploads/TA000031/product/53401af19e9246f398747598acdbc66a.jpg" alt="기획전 <해양 명품 100선, 바다를 품다>&nbsp;"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000053&amp;companyCd=TA000030&amp;facilityCd=2659"> <img src="http://www.culture.go.kr/ticket/uploads/TA000030/product/baaf32a7013c421dbdd629fec9409a66.jpg" alt="지음[知音, 짓다] - 시간의 흔적, 미래로 펼치다"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000051&amp;companyCd=TA000026&amp;facilityCd=2554"> <img src="http://www.culture.go.kr/ticket/uploads/TA000026/product/6d4dce387de74099a2298a15efb34ca5.jpg" alt="2017 서울 포커스 [25.7]"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000050&amp;companyCd=TA000025&amp;facilityCd=0175"> <img src="http://www.culture.go.kr/ticket/uploads/TA000025/product/2c35b1efac354d7b84e3f023b3630969.jpg" alt="밀양"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000049&amp;companyCd=TA000024&amp;facilityCd=3665"> <img src="http://www.culture.go.kr/ticket/uploads/TA000024/product/2cb3e127cb6e4efda88b4e29fed0845f.jpg" alt="성남의얼굴展 <성남을걷다>"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000036&amp;companyCd=TA000014&amp;facilityCd=2056"> <img src="/ticket/uploads//TA000014/product/b8c9e67f74024988af478ce9ea02cc4c.gif" alt="(이벤트 전시) 찰리와 초콜릿 공장 원화 작가, 퀀틴 ...."> 
           <div class="lb_info"> 
            <span class="age">20대</span> 
            <span class="free">무료</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
      <li><button>행사/축제</button> 
       <div class="list"> 
        <ul> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000109&amp;companyCd=TA000142&amp;facilityCd=1172"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/bac89b08a7384f0ea2f6e84de193534b.jpg" alt="문화채널마포 수요예술포럼 문화비젼 2030 톺아보기"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000075&amp;companyCd=TA000142&amp;facilityCd=1172"> <img src="http://www.culture.go.kr/ticket/uploads/TA000142/product/67c95d26b4ad494fb356812e6192eb12.jpg" alt="문화채널마포 수요예술포럼"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
         <li> <a href="/ticket/exhibit/exhibit_view.action?productCd=PR000052&amp;companyCd=TA000027&amp;facilityCd=1352"> <img src="http://www.culture.go.kr/ticket/uploads/TA000027/product/cf6fdf7dafd84b57acf65a793ad3479b.jpg" alt="로맨틱 춘천 페스티벌 2018"> 
           <div class="lb_info"> 
            <span class="free">무료</span> 
           </div> </a> </li> 
        </ul> 
        <button class="more">더보기</button> 
       </div> </li> 
     </ul> 
    </div> 
   </div> 
  </div> 
  <!--// 공연리스트 --> 
  --%>
  
  <!-- 공지 및 유딜 --> 
  <div class="section05"> 
   <div class="con_box"> 
    <div class="notice_wrap"> 
     <div class="body"> 
      <ul class="list" style="top: 0px;"> 
        
        
        
       <li class="3"> <a href="/ticket/useInfo/noticeView.action?seq=27"> [공지] 2018년 설연휴(02.14~02.16) 고객센터 운영시간 안내 <span>2018-02-14</span> </a> </li><li class="4"> <a href="/ticket/useInfo/noticeView.action?seq=26"> [공지] 문화N티켓 모바일 서비스 제한 및 오류 안내 <span>2018-02-13</span> </a> </li><li class="1"> <a href="/ticket/useInfo/noticeView.action?seq=29"> [공지] 문화N티켓 서비스 업데이트 및 서버 점검 작업 예정 안내(2018년 3월 19일(월)) <span>2018-03-19</span> </a> </li><li class="2"> <a href="/ticket/useInfo/noticeView.action?seq=28"> [공지] 문화N티켓 서비스 업데이트 및 서버 점검 작업 예정 안내(2018년 2월 20일(화)) <span>2018-02-20</span> </a> </li> 
       <li class="5"> <a href="/ticket/useInfo/noticeView.action?seq=25"> [공지] 문화 예술공연단체의 '문화N티켓'사이트 참여신청을 받습니다. <span>2017-12-21</span> </a> </li> 
      </ul> 
     </div> 
     <ul class="control"> 
      <li><button class="prev">이전</button></li> 
      <li><button class="stop">정지</button></li> 
      <li><button class="next">다음</button></li> 
     </ul> 
     <a href="/ticket/useInfo/notice.action" class="more">더보기</a> 
    </div> 
    <div class="btm_util"> 
     <div class="q_link"> 
      <ul> 
       <li><a href="http://www.culture.go.kr/wday/" target="_blank" title="새창">문화가 있는 날</a></li> 
       <li><a href="http://www.culture.go.kr/perform/relayList.action" target="_blank" title="새창">릴레이티켓</a></li> 
       <li><a href="/ticket/useInfo/guide.action?tab=4">취소/환불 안내</a></li> 
       <li><a href="/ticket/useInfo/ticketRequest.action">공연단체 참여신청</a></li> 
       <li><a href="http://www.culture.go.kr/ticketadmin/" target="_blank">티켓관리시스템</a></li> 
       <li><a href="/ticket/useInfo/faq.action">자주 묻는 질문</a></li> 
      </ul> 
     </div> 
     <div class="center"> 
      <p class="txt">공연 예매 / 공연 등록 문의</p> 
      <p class="phone">02.3153.2898</p> 
      <p class="time">평일 오전 10시 ~ 오후 6시</p> 
     </div> 
    </div> 
   </div> 
  </div> 
  <!-- 공지 및 유딜 --> 
 

			</div>
			<!--E: container -->


<!--S: footer -->
<jsp:include page="../layout/inc_footer.jsp"/>



<!-- 푸터 시작 -->	
<!--S: footer -->
			

<!-- <div class="footer">
	<div class="footer_body">
		<div class="btm_t_wrap">
			<ul class="link">
				<li><a href="/ticket/polc/policy.action">개인정보처리방침</a></li>
				<li><a href="/ticket/polc/term.action">이용약관</a></li>
				<li><a href="/ticket/polc/copy.action">저작권정책</a></li>
				<li><a href="/ticket/polc/collect.action">검색결과수집정책</a></li>
			</ul>
			<ul class="sns">
					<li class="snsT"><a title="새창열림" href="https://www.facebook.com/culture.N.ticket/" target="_blank"><span>문화N티켓 페이스북 바로가기</span></a></li>
					<li class="snsF"><a title="새창열림" href="https://www.instagram.com/culture_n_ticket/" target="_blank"><span>문화N티켓 인스타그램 바로가기</span></a></li>
					<li class="snsB"><a title="새창열림" href="https://blog.naver.com/PostThumbnailList.nhn?blogId=kcis_&amp;from=postList&amp;categoryNo=71&amp;parentCategoryNo=71" target="_blank"><span>문화N티켓 블로그 바로가기</span></a></li>
			</ul>
			<div class="family_site">
				<a href="#none" class="family_btn">관련사이트</a>
				<ul>
					<li><a href="http://www.culture.go.kr/" target="_blank" title="새창">문화포털</a></li>
					<li><a href="http://www.culture.go.kr/data/" target="_blank" title="새창">문화데이터광장</a></li>
					<li><a href="http://www.culture.go.kr/wday/" target="_blank" title="새창">매달 마지막 수요일은 문화가 있는 날</a></li>
				</ul>
			</div>
		</div>

		<div class="copy_wrap">
			<div class="logo_g">
				<a href="http://www.mcst.go.kr/" target="_blank" title="새창"><img src="/culturen/resources/img/btm_logo01.gif" alt="문화체육관광부"></a>
				<a href="http://www.kcisa.kr/" target="_blank" title="새창"><img src="/culturen/resources/img/btm_logo02.gif" alt="한국문화정보원"></a>
			</div>

			<address>
				우)03925 서울시 마포구 월드컵북로 400 <br>
				대표전화:02-3153-2898, 이메일:ticket@kcisa.kr, Copyright 한국문화정보원<br>
				사업자 등록번호 101-82-10054 대표자 : 이현웅
			</address>

			<div class="certify_wrap">

				<a href="http://www.culture.go.kr/" target="_blank" title="새창"><img src="/culturen/resources/img/btm_r_02.gif" alt="문화포털"></a>
			</div>
		</div>

	</div>
</div>

<button class="btn_top" style="display: none;"><span class="txt">TOP</span></button>


AceCounter Log Gathering Script V.7.5.2017020801
<script language="javascript">
	var _AceGID=(function(){var Inf=['gtb14.acecounter.com','8080','AH4A42094072613','AW','0','NaPm,Ncisy','ALL','0']; var _CI=(!_AceGID)?[]:_AceGID.val;var _N=0;var _T=new Image(0,0);if(_CI.join('.').indexOf(Inf[3])<0){ _T.src =( location.protocol=="https:"?"https://"+Inf[0]:"http://"+Inf[0]+":"+Inf[1]) +'/?cookie'; _CI.push(Inf);  _N=_CI.length; } return {o: _N,val:_CI}; })();
	var _AceCounter=(function(){var G=_AceGID;var _sc=document.createElement('script');var _sm=document.getElementsByTagName('script')[0];if(G.o!=0){var _A=G.val[G.o-1];var _G=(_A[0]).substr(0,_A[0].indexOf('.'));var _C=(_A[7]!='0')?(_A[2]):_A[3];var _U=(_A[5]).replace(/\,/g,'_');_sc.src=(location.protocol.indexOf('http')==0?location.protocol:'http:')+'//cr.acecounter.com/Web/AceCounter_'+_C+'.js?gc='+_A[2]+'&py='+_A[4]+'&gd='+_G+'&gp='+_A[1]+'&up='+_U+'&rd='+(new Date().getTime());_sm.parentNode.insertBefore(_sc,_sm);return _sc.src;}})();
</script>
<noscript>&lt;img src='http://gtb14.acecounter.com:8080/?uid=AH4A42094072613&amp;je=n&amp;' border='0' width='0' height='0' alt=''&gt;</noscript>	
AceCounter Log Gathering Script End
			E: footer
		</div>
	
</body></html> -->